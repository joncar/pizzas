var callbackOnRefresh = function(data){console.log(data);}
$(document).on('ready',function(){
   //Conectar con el server
    ws = io.connect('http://74.208.12.230:3000',{'force new connection': true});
    ws.on('connect',function(){
        ws.on('onopen', function() {
           datos = {type:'web',id:idSocket};
           ws.emit('connectID',datos);
           //ws.emit('buscarRepartidor',{'idPedido':'518'});
           console.log('Conexion con el socket establecida');
        });
    });        

    ws.on('disconnect',function(){
        console.log('desconectado');
    });

    ws.on('refresh',function(data){
        $(document).trigger('refresh',[data]);                            
    });
    
    ws.on('lose',function(data){
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Pedido perdido',
                // (string | mandatory) the text inside the notification
                text: 'Se ha perdido un pedido por falta de repartidores disponibles. Consultalo en: <a href="'+url+'/pedidos/'+data.pedidoid+'">#'+data.pedidoid+'</a>',
                // (string | optional) the image to display on the left
                image: '',
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: true,
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'my-sticky-class'
            });
    });
});
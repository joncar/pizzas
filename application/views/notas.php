
<div class='row'>
    <div class='panel panel-default'>
        <div class='panel-heading'>
            <h1 class='panel-title'>Datos principales</h1>
        </div>
        <div class='panel-body'>
            <div class="form-group">                
                <label><?php echo $input_fields['instrumentos_id']->display_as; ?> <?php echo ($input_fields['instrumentos_id']->required)? "<span class='required'>*</span> " : ""; ?></label>
                <?php echo  $input_fields['instrumentos_id']->input ?>
            </div>
            <div class="form-group">
                <label><?php echo $input_fields['nombre']->display_as; ?> <?php echo ($input_fields['nombre']->required)? "<span class='required'>*</span> " : ""; ?></label>
                <?php echo  $input_fields['nombre']->input ?>
            </div>
            <div class="form-group">
                <label><?php echo $input_fields['imagen']->display_as; ?> <?php echo ($input_fields['imagen']->required)? "<span class='required'>*</span> " : ""; ?></label><br/>
                <?php echo  $input_fields['imagen']->input ?>
            </div>
            <div class="form-group">
                <label><?php echo $input_fields['audio']->display_as; ?> <?php echo ($input_fields['audio']->required)? "<span class='required'>*</span> " : ""; ?></label><br/>
                <?php echo  $input_fields['audio']->input ?>
            </div>
            <div class="form-group">
                <label><?php echo $input_fields['publicado']->display_as; ?> <?php echo ($input_fields['publicado']->required)? "<span class='required'>*</span> " : ""; ?></label><br/>
                <?php echo  $input_fields['publicado']->input ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">
                Crear regiones
            </h1>
        </div>
        <div class="panel-body">
            <div align='left' class='col-xs-6 row'>
                <div>
                    <b>Configuración de la región</b>
                </div>                
                <div class='row'>
                    <div class='form-group col-xs-6'>
                        <label>Ancho recuadro</label>
                        <input type='number' placeholder="px" class='form-control' value='100' id="widthinput">
                    </div>
                    <div class='col-xs-6 form-group'>
                        <label>Largo recuadro</label>
                        <input type='number' class='form-control' placeholder="px" value='100' id="heightinput"> 
                    </div>               
                </div>
                <div class='row'>
                    <div class='form-group col-xs-6'>
                        <label>Posición X</label>
                        <input type='text' readonly="" style="background:lightgray" class='form-control' value='0px' id="leftinput">
                    </div>
                    <div class='col-xs-6 form-group'>
                        <label>Posición Y</label>
                        <input type='text' readonly="" style="background:lightgray" class='form-control' value='0px' id="topinput"> 
                    </div>               
                </div>                
            </div> 
        </div>
    </div>
</div>
<div class="row">
    <div class='col-xs-8'>
        <div class='panel panel-default'>
            <div class='panel-heading'>
                <h1 class='panel-title'>Previsualizar</h1>
            </div>
            <div class='panel-body'>
                <div class=col-xs-12" style="position:relative; width:600px; height:700px; background:url(<?= base_url('images/partituras/1157-Moonlight_Sonata_Jazz_Lead_Sheet-page-001.jpg') ?>) no-repeat; background-size:600px 700px;" id="previsualizarpanel">
                    <div id="visor" style="background:rgba(0,255,255,.4); z-index:10000; width:100px; height:100px; border:2px dashed black; cursor:move;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class='col-xs-4'>
        <div class='panel panel-default'>
            <div class='panel-heading'>
                <h1 class='panel-title'>Partituras</h1>
            </div>
            <div class='panel-body' id='notaslist'>
                Ninguna
            </div>
            <div class='row'>
                <div class='form-group col-xs-6'>
                    <label>Desde segs</label>
                    <input type='number' class='form-control' id="desde">
                </div>
                <div class='col-xs-6 form-group'>
                    <label>Hasta segs</label>
                    <input type='number' class='form-control' id="hasta"> 
                </div>                        
            </div>
            <div class="row" align="center" style="margin-top:20px;">
                    <a href='javascript:addRegion()' type="button" class="btn btn-success"><i class="fa fa-plus"></i> Añadir</a>
            </div>
            <?php foreach($hidden_fields as $hidden_field){
                                echo $hidden_field->input;
                        } ?>
        </div>
    </div>
</div> 
<script src="<?= base_url() ?>/js/jquery.maskedinput.min.js"></script>
<script>
    regiones = 0;
    regioneslist = [];
    $(document).ready(function(){        
        $("#visor").draggable({
            containment: "#previsualizarpanel",
            scroll: false,
            snap: ".regionselected",
            drag:function(){
                $("#leftinput").val($("#visor").css('left'));
                $("#topinput").val($("#visor").css('top'));
            }
        });
        
        $("#widthinput").change(function(){
            $("#visor").css('width',$(this).val()+'px');
        });
        $("#heightinput").change(function(){
            $("#visor").css('height',$(this).val()+'px');
        });
        
        $(document).on('aftercrop',function(){            
            $("#previsualizarpanel").css({'background-size':'600px 700px','background':'url(<?= base_url() ?>images/partituras/'+$("#field-imagen").val()+') no-repeat'});
        });
        $("#previsualizarpanel").css({'background-size':'600px 700px','background':'url(<?= base_url() ?>images/partituras/'+$("#field-imagen").val()+') no-repeat'});
        
        if($("#field-regiones").val()!=''){
            regioneslist = JSON.parse($("#field-regiones").val());
            drawInit();
            fillRegionList();
        }
    });
    
    function drawInit(){
        for(i in regioneslist){
            $("#previsualizarpanel").append('<div class="regionselected" id="v'+regioneslist[i].id+'" style="background:rgba(255,0,0,.1); width:'+regioneslist[i].width+'px; height:'+regioneslist[i].height+'px; position:absolute; top:'+regioneslist[i].top+'; left:'+regioneslist[i].left+'; border:1px dashed red; cursor:move;"></div>');
        }
    }
    
    function addRegion(){
        if($("#desde").val()=='' || $("#hasta").val()==''){
            alert('Debes elegir el periodo de tiempo');
        }
        else{
            regiones++;
            regioneslist.push({
                id:regiones,
                desde:$("#desde").val(),
                hasta:$("#hasta").val(),
                width:$("#widthinput").val(),
                height:$("#heightinput").val(),
                top:$("#topinput").val(),
                left:$("#leftinput").val()
            });

            $("#previsualizarpanel").append('<div class="regionselected" id="v'+regiones+'" style="background:rgba(255,0,0,.1); width:'+$("#widthinput").val()+'px; height:'+$("#heightinput").val()+'px; position:absolute; top:'+$("#topinput").val()+'; left:'+$("#leftinput").val()+'; border:1px dashed red; cursor:move;"></div>');
            left = $("#leftinput").val();
            left = left.replace('px','');
            left = parseInt(left)+parseInt($("#widthinput").val())+'px';
            $("#leftinput").val(left);
            $("#visor").css('left',left);
            $("#desde").val($("#hasta").val());
            fillRegionList();
        }
    }
    
    function fillRegionList(){
        if(regioneslist.length>0){
            s = '<ol>';
            for(i in regioneslist){
                s+= '<li><b>desde: </b>'+regioneslist[i].desde+' <b>hasta: </b> '+regioneslist[i].hasta+' <a style="color:red" href="javascript:remove('+regioneslist[i].id+')"><i class="fa fa-eraser"></i></a></li>';
            }
            s+= '</ol>';
            $("#notaslist").html(s);
            $("#field-regiones").val(JSON.stringify(regioneslist));
        }
        else{
            $("#notaslist").html('Ninguna');
        }
    }
    
    function remove(item){
        aux = [];        
        for(i in regioneslist){
            if(regioneslist[i].id!=item)
            {                
                aux.push(regioneslist[i]);
            }
            else{
                $("#previsualizarpanel #v"+regioneslist[i].id).remove();
            }
        }
        regioneslist = aux;
        fillRegionList();
    }
</script>
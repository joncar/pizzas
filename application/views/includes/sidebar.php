<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
            <li>
                <a href="<?= base_url('panel') ?>">
                    <i class="menu-icon fa fa-desktop"></i>
                    <span class="menu-text">Dashboard</span>                                    
                </a>                                
            </li>
            <!--- Fin App ---->     
 
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">Usuarios</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <!--- Seguridad --->
                    <li>
                        <a href="<?= base_url('repartidor/repartidores') ?>">
                            <i class="menu-icon fa fa-exclamation-triangle"></i>
                            <span class="menu-text">Repartidores</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('sucursal/sucursales') ?>">
                            <i class="menu-icon fa fa-exclamation-triangle"></i>
                            <span class="menu-text">Sucursales</span>                                    
                        </a>                                
                    </li>
                </ul>
            </li>
            <!--- Fin Admin ---->
            <!--- Admin --->
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-motorcycle"></i>
                        <span class="menu-text">Transporte</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <!--- Seguridad --->
                    <li>
                        <a href="<?= base_url('equipo/seguros') ?>">
                            <i class="menu-icon fa fa-exclamation-triangle"></i>
                            <span class="menu-text">Seguros</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('equipo/equipos') ?>">
                            <i class="menu-icon fa fa-exclamation-triangle"></i>
                            <span class="menu-text">Transportes</span>                                    
                        </a>                                
                    </li>
                </ul>
            </li>             
            <li>
                <a href="<?= base_url('admin/pedidos') ?>">
                    <i class="menu-icon fa fa-truck"></i>
                    <span class="menu-text">Servicios</span>
                </a>                        
            </li>
            <!--- Fin Admin ---->
            <!--- Admin --->
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-wrench"></i>
                        <span class="menu-text">Configuración</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <!--- Seguridad --->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Seguridad</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('seguridad/grupos') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Grupos</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/funciones') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/user') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Usuarios</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/core') ?>">
                                    <i class="menu-icon fa fa-clock-o"></i>
                                    <span class="menu-text">Core</span>
                                </a>                        
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?= base_url('repartidor/ajustes') ?>">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Ajustes Generales</span>
                        </a>                        
                    </li>
                </ul>
            </li>
            <!--- Fin Admin ---->
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>

<div id="page-wrapper" style="min-height: 722px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-dashboard"></i> Administración</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row well">
            <form method="post" action="<?= base_url('panel/aplicarFiltros') ?>">
                <a class="btn btn-default" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-calendar"></i>
                </a>
                <div class="collapse" id="collapseExample">
                    <div class="row well">
                        <div class="form-group col-xs-6">
                            <label class="control-label col-xs-3">Desde: </label>
                            <div class="col-xs-9">
                                <input type="text" name="fechadesde" placeholder="dd-mm-yy" class="form-control datetime-input" id="desde" value="<?= !empty($_SESSION['filtros']) && !empty($_SESSION['filtros']['fechadesde'])?$_SESSION['filtros']['fechadesde']:'' ?>">
                            </div>
                        </div>
                        <div class="form-group col-xs-6">
                            <label class="control-label col-xs-3">Hasta: </label>
                            <div class="col-xs-9">
                                <input type="text" name="fechahasta" placeholder="dd-mm-yy" class="form-control datetime-input" id="hasta" value="<?= !empty($_SESSION['filtros']) && !empty($_SESSION['filtros']['fechahasta'])?$_SESSION['filtros']['fechahasta']:'' ?>">
                            </div>
                        </div>
                        <div class="col-xs-12" align="center">
                            <button type="submit" class="btn btn-success">Consultar</button>
                            <a href="<?= base_url('panel/aplicarFiltros/1') ?>" class="btn btn-default">Resetear filtros</a>
                        </div>
                    </div>
                </div>                
            </form>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-2 col-lg-offset-1 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-money fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge">$<span id="cobrado">0</span></h2>
                                <div>Envíos cobrados</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="motorizados">0</h2>
                                <div>Repartidores Online</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('repartidor/conectadosList') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-motorcycle fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="pedidos">0</h2>
                                <div># de envios</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('admin/conectadosList') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-building fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="sucursales">0</h2>
                                <div>Sucursales Online!</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('sucursal/conectadosList') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
             <div class="col-lg-2 col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-clock-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="espera">0</h2>
                                <div>Pedidos en espera!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div>

                    <!-- Mapas -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#home" id="mapaenvivoa" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Mapa en vivo</a></li>
                      <li role="presentation"><a href="#profile" id="mapatraficoa" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-paper-plane"></i> Enviar mensaje</a></li>                      
                      <li role="presentation"><a href="#repartidores" id="maprepartidores" aria-controls="repartidores" role="tab" data-toggle="tab"><i class="fa fa-motorcycle"></i> Repartidores</a></li>                      
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div id="mapaenvivo" style="height:600px; width:100%">
                                Mapa
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div id="mapatrafico" style="height:600px; width:100%">
                                <?php $this->load->view('includes/_dash_mensajes'); ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="repartidores">
                            <div id="repartidores" style="height:600px; width:100%">
                                <?php 
                                    $this->load->view('includes/_dashRepartidoresTab'); 
                                ?>
                            </div>
                        </div>
                    </div>                                        
                </div>                 
                <!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bell fa-fw"></i> Transacciones
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="list-group">
                                <?php foreach($resumenes['transacciones'] as $t): ?>
                                    <a class="list-group-item" href="<?= base_url('sucursal/transacciones/read/'.$t->id) ?>">
                                        <i class="fa fa-comment fa-fw"></i> <?= $t->detalle ?>
                                        <span class="pull-right text-muted small"><em><?= $t->fecha ?></em>
                                        </span>
                                    </a>
                               <?php endforeach ?>
                            </div>
                        <!-- /.list-group -->
                        <a class="btn btn-default btn-block" href="<?= base_url('sucursal/transacciones') ?>">Ver todos</a>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->
    </div>
<div id="info" style="display:none">
    <div class="col-xs-2">
        <img src="" id="foto" style="width:100%">
    </div>
    <div class="col-xs-10">
        <div id="nombre"></div>
        <div id="email"></div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?signed_in=true&v=3.exp"></script>
<?php $this->load->view('predesign/datepicker') ?>
<script>
    //Inicializar mapa
    <?php $coords_default = $this->db->get('ajustes')->row(); ?>        
    var mapaContent = document.getElementById('mapaenvivo');
    var center = new google.maps.LatLng<?= $coords_default->mapa ?>;    
    var mapOptions = {
                zoom: <?= $coords_default->zoom ?>,
                center: center,               
    };
    var map = new google.maps.Map(mapaContent, mapOptions);
    var repartidores = [];   
    var sucursales = [];
    var centrado = 0;
    var repartidoresBD = <?php 
        $re = array();
        foreach($this->db->get_where('repartidores',array('repartidores.status <='=>5))->result() as $r){
            $re[$r->id] = $r;
        }
        echo json_encode($re);
    ?>;
        
     var sucursalesBD = <?php 
        $re = array();
        foreach($this->db->get_where('sucursales')->result() as $r){
            $re[$r->id] = $r;
        }
        echo json_encode($re);
    ?>;
        
     var marcadores = [];
    
    // Objeto repartidor
    function repartidor(){
        this.mark = undefined;
        this.position = undefined;
        this.info = undefined;
        this.id = undefined;
        this.addEvents = function(){
            var r = this;            
            google.maps.event.addListener(this.mark,'click',function(){
                r.info.open(r.map,r.mark);
            });
            
            google.maps.event.addListener(this.mark,'dragend',function(e){
                console.log(e);
                $.post('<?= base_url('api/sucursales/web/update/') ?>/'+r.id,{lat_mapa:e.latLng.lat(),lon_mapa:e.latLng.lng()},function(data){
                    
                });
            });
        }
        this.move = function(position){
            this.mark.setPosition(position);
        }
        this.remove = function(){
            this.mark.setMap(null);
        }        
    }
    function moverMapa(data){        
        var bounds = new google.maps.LatLngBounds();
        for(var i in data){
            if(data[i].lat!==null && data[i].lon!==null){
             bounds.extend(new google.maps.LatLng(data[i].lat,data[i].lon));
            }
        }
        $(document).ready(function() {
           map.fitBounds(bounds);
        });
        
    }
    
    function mostrar(url){
        $.post(url,{},function(data){
            emergente(data);
        });
    }
    
    /*** Mostrarlos todos ****/
    for(var i in repartidoresBD){
            l = repartidoresBD[i];
            r = new repartidor();
            r.id = l.id;
            r.map = map;
            r.position = new google.maps.LatLng(l.lat,l.lon);
            r.mark = new google.maps.Marker({
                position: r.position,
                map: r.map,
                icon:'<?= base_url('img/transport-4.png') ?>'
             });
            r.info =  new google.maps.InfoWindow();
            $("#info #foto").attr('src','<?= base_url('images/repartidores') ?>/'+l.foto);
            $("#info #nombre").html(l.nombre_repartidor);
            $("#info #email").html(l.email);
            r.info.setContent($("#info").html());
            r.addEvents();
            repartidores.push(r);
            marcadores.push({lat:l.lat,lon:l.lon});
    };
    
    /*** Mostrarlos todos ****/
    for(var i in sucursalesBD){
            l = sucursalesBD[i];
            r = new repartidor();
            r.id = l.id;
            r.map = map;
            r.position = new google.maps.LatLng(l.lat_mapa,l.lon_mapa);
            r.mark = new google.maps.Marker({
                position: r.position,
                map: r.map,
                icon:'<?= base_url('img/restaurant-2.png') ?>',
                draggable:true
             });
            r.info =  new google.maps.InfoWindow();
            $("#info #foto").attr('src','<?= base_url('img/restaurant-2.png') ?>');
            $("#info #nombre").html(l.nombre_sucursal);
            $("#info #email").html(l.email);
            r.info.setContent($("#info").html());
            r.addEvents();
            sucursales.push(r);
            marcadores.push({lat:l.lat,lon:l.lon});
    };
    moverMapa(marcadores);
    
     //Que hago cunado reciba los datos del core?
    $(document).on('refresh',function(evt,data){
        $("#motorizados").html(data.repartidores);
        $("#sucursales").html(data.sucursales.length);
        $("#pedidos").html(data.pedidos);
        $("#cobrado").html(data.cobrado);
        $("#espera").html(data.espera);
        $("#desde").val(data.time);
        //Quitarles la pintura
        for(var i in repartidores){
            repartidores[i].mark.setIcon('<?= base_url('img/transport-4.png') ?>');
        }
        //Añadimos nuevo color
        for(var k in data.locations){
            for(var i in repartidores){
                if(repartidores[i].id===data.locations[k].id){
                    repartidores[i].mark.setIcon('<?= base_url('img/transport.png') ?>');
                    repartidores[i].mark.setPosition(new google.maps.LatLng(data.locations[k].lat,data.locations[k].lon));
                }
            }
        }
        
        //Quitarles la pintura
        for(var i in sucursales){
            sucursales[i].mark.setIcon('<?= base_url('img/restaurant-2.png') ?>');
        }
        //Añadimos nuevo color        
        for(var k in data.sucursales){
            for(var i in sucursales){               
                if(sucursales[i].id===data.sucursales[k].id){
                    sucursales[i].mark.setIcon('<?= base_url('img/restaurant-3.png') ?>');
                    //sucursales[i].mark.setPosition(new google.maps.LatLng(data.sucursales[k].lat,data.sucursales[k].lon));
                }
            }
        }
    });
    
    $("#desde").change(function(){
        var da = $(this).val();
        d = da.split('-');
        d[2] = d[2].split(' ');
        d[2] = d[2][0];
        console.log(d);
        ws.emit('changeDate',{date:d[2]+'-'+d[1]+'-'+d[0]});
    });
</script>
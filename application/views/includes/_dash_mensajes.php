<form id='mensajeria' onsubmit="return sendMessage()">
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#_msj_rep"  aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-motorcycle"></i> Repartidores</a></li>
                <li role="presentation"><a href="#_msj_suc" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-users"></i> Sucursales</a></li>                      
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="_msj_rep" style="max-height:400px; overflow: auto">
                      <ul class="list-group">
                          <?php foreach($this->db->get('repartidores')->result() as $r): ?>
                                <li class='list-group-item'>
                                    <?php if(!empty($r->gcm)): ?>
                                        <input type='checkbox' name='app_id[]' value='<?= $r->gcm ?>'>
                                    <?php else: ?>
                                        [ID No encontado]
                                    <?php endif ?>
                                        <?= $r->nombre_repartidor ?>
                                </li>
                          <?php endforeach ?>
                      </ul>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="_msj_suc" style="max-height:400px; overflow: auto">
                      <?php $this->db->order_by('nombre_sucursal','asc');  foreach($this->db->get('sucursales')->result() as $r): ?>
                      <li class='list-group-item'>
                            <?php if(!empty($r->gcm)): ?>
                                <input type='checkbox' name='app_id[]' value='<?= $r->gcm ?>'>
                            <?php else: ?>
                                [ID No encontado]
                            <?php endif ?>
                                <?= $r->nombre_sucursal ?>
                      </li>
                      <?php endforeach ?>                    
                  </div>
              </div>
        </div>
        <div class="col-sm-12"><textarea name='mensaje' class="form-control" placeholder='Escribe tu mensaje'></textarea></div>
        <div class='col-sm-12'><button type='submit' class='btn btn-success'>Enviar mensaje</button></div>
      </div>
</form>
<script>
    function sendMessage(){
        var data = document.getElementById('mensajeria');
        var d  = new FormData(data);
        $.ajax({
            url:'<?= base_url('gcm/sendMensage') ?>',
            data:d,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                alert(data);
            }
        });
        return false;
    }
</script>
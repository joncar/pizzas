<?php
echo $output 
?>
<script>
    $(document).ready(function(){
        $("#field-estudiantes_id").change(function(){
            $.post('<?= base_url('planAcademico/planAcademicoJSON/getProgramacionCarrerasId') ?>',{estudiantes_id:$(this).val()},function(data){
                $("#field_programacion_carreras_id_chzn").remove();
                $("#field-programacion_carreras_id").remove();
                $("#programacion_carreras_id_field_box").append(data);
                $(".chosen-select,.chosen-multiple-select").chosen({allow_single_deselect:true});
            });
        });
    })
</script>
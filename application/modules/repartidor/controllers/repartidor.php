<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Repartidor extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function repartidores($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);
            $crud->field_type('password','password')
                    ->field_type('status','dropdown',
                            array(
                                '1'=>'<span class="label label-success">Esperando trabajo</span>',
                                '2'=>'<span class="label label-warning">Tránsito</span>',
                                '3'=>'Sin espacio',
                                '4'=>'<span class="label label-danger"></span> Siniestrado',
                                '5'=>'<span class="label label-default">Inactivo</span>',
                                '6'=>'Bloqueado'))
                    ->field_type('ubicacion','hidden')
                    ->field_type('tarifa','hidden',0)                    
                    ->field_type('calificacion','hidden',0)
                    ->field_type('capacidad_carga','hidden',$this->db->get('ajustes')->row()->max_tareas_simultaneas)
                    ->field_type('ubicacion','hidden')
                    ->field_type('tipo_pago','dropdown',array('1'=>'Por contrato','2'=>'Por distancia'))
                    ->field_type('fecha_registro','hidden',date("Y-m-d H:i:s"))
                    ->field_type('lat','invisible')
                    ->field_type('lon','invisible')
                    ->field_type('gcm','invisible');            
            $crud->columns('foto','nombre_repartidor','email','status','placa');
            if($crud->getParameters()=='list' || $crud->getParameters()=='ajax_list' || $crud->getParameters()=='success' || $crud->getParameters()=='delete'){
                $crud->set_field_upload('foto','images/repartidores');
            }else{
                $crud->field_type('foto','image',array('path'=>'images/repartidores','width'=>'300px','height'=>'300px'));
                //$crud->field_type('status','hidden',0);
            }            
            $crud->unset_edit_fields('email');
            $crud->set_rules('email','Email','required|valid_email|is_unique[repartidores.email]')
                     ->set_rules('password','Password','required|min_length[8]');                        
            $crud->add_action('<i class="fa fa-truck"></i> Detalles de servicios','',base_url('admin/pedidos/repartidor').'/');
            $crud->add_action('<i class="fa fa-clock-o"></i> Definir Horarios','',base_url('repartidor/repartidores_horarios').'/');
            $crud->callback_before_insert(array($this,'addCoords'));
            $crud->callback_before_update(array($this,'addCoords'));
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function repartidores_horarios($x = '',$y = ''){
            if(is_numeric($x)){
                $crud = $this->crud_function('','',$this);
                $crud->set_subject('Horario');
                $crud->where('repartidores_id',$x);
                $crud->unset_columns('repartidores_id');
                $crud->field_type('repartidores_id','hidden',$x);
                $crud->field_type('dia','dropdown',array(0=>'Domingo',1=>'Lunes',2=>'Martes',3=>'Miercoles',4=>'Jueves',5=>'Viernes',6=>'Sabado'));                
                $crud->field_type('status','dropdown',
                            array(
                                '1'=>'<span class="label label-success">Esperando trabajo</span>',
                                '2'=>'<span class="label label-warning">Tránsito</span>',
                                '3'=>'Sin espacio',
                                '4'=>'<span class="label label-danger"></span> Siniestrado',
                                '5'=>'<span class="label label-default">Inactivo</span>',
                                '6'=>'Bloqueado'));
                $crud->display_as('status','Poner a estatus');
                $output = $crud->render();
                $output->title = 'Horarios';
                $output->crud = 'horarios';
                $this->loadView($output);
            }else{
                header("Location:".base_url('repartidor/repartidores'));
            }
        }
        
        function addCoords($post){
            $p = str_replace('(','',$post['ubicacion']);
            $p = str_replace(')','',$p);
            $p = explode(',',$p);
            $post['lat'] = trim($p[0]);
            $post['lon'] = trim($p[1]);
            return $post;
        }
        
        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->field_type('mapa','map')
                    ->unset_add()
                    ->unset_delete()
                    ->unset_print()
                    ->unset_export()
                    ->unset_read();
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function conectadosList(){
            $this->as = array('conectadosList'=>'repartidores');
            $crud = $this->crud_function('','');
            $crud->set_subject('Repartidores conectados');            
            $crud->where('status !=',5);
            $crud->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_delete()
                     ->unset_export()
                     ->unset_print();
            $crud->field_type('status','dropdown',
                            array(
                                '1'=>'<span class="label label-success">Esperando trabajo</span>',
                                '2'=>'<span class="label label-warning">Tránsito</span>',
                                '3'=>'Sin espacio',
                                '4'=>'<span class="label label-danger"></span> Siniestrado',
                                '5'=>'<span class="label label-default">Inactivo</span>',
                                '6'=>'Bloqueado'));
            $crud->columns('nombre_repartidor','email','status');
            $crud = $crud->render();
            echo $crud->output;
        }
    }
?>

<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Sucursal extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function sucursales($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);            
            $crud->field_type('ubicacion','map');
            $crud->field_type('password','password')
                     ->field_type('status','dropdown',
                        array(
                            '0'=>'<span class="label label-default">Desconectado</span>',
                            '1'=>'<span class="label label-success">Conectado</span>'))
                 ->field_type('lat','invisible')
                 ->field_type('lon','invisible');
            $crud->set_rules('email','Email','required|valid_email|is_unique[sucursales.email]')
                 ->set_rules('password','Password','required|min_length[8]');    
            $crud->columns('nombre_sucursal','email','telefono_fijo','telefono_celular','envios_sin_costo','Pedidos Realizados','status');
            $crud->callback_column('envios_sin_costo',function($val,$row){               
               return $val>0?'<span style="color:blue">'.$val.'</span>':$val;
            });
            $crud->callback_column('Pedidos Realizados',function($val,$row){
               $pedidos = get_instance()->db->get_where('pedidos',array('sucursales_id'=>$row->id))->num_rows; 
               return $pedidos==0?'<span style="color:red">'.$pedidos.'</span>':$pedidos;
            });
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
                $crud->field_type('status','invisible');
            }
            $crud->add_action('<i class="fa fa-truck"></i> Detalles de servicios','',base_url('admin/pedidos/sucursal').'/');
            $crud->display_as('nombre_sucursal','Nombre');
            $crud->display_as('envios_sin_costo','Saldo disponible');
            $crud->unset_edit_fields('email');
            $crud->callback_before_insert(array($this,'addCoords'));
            $crud->callback_before_update(array($this,'addCoords'));
            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function transacciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);                        
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function pedidos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('ubicacion','map')
                    ->field_type('tipo_pago','dropdown',array('1'=>'Efectivo','2'=>'Debito','3'=>'Credito'))
                    ->field_type('status','dropdown',array('-2'=>'Cancelada por el banco','-1'=>'Cancelado','1'=>'Por recoger','2'=>'En camino','3'=>'Entregado','4'=>'Programado'));
            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function clientes($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
         function addCoords($post){
            $p = str_replace('(','',$post['ubicacion']);
            $p = str_replace(')','',$p);
            $p = explode(',',$p);
            $post['lat'] = trim($p[0]);
            $post['lon'] = trim($p[1]);
            return $post;
        }
        
        function estado_cuenta($x = '',$y = ''){
            if(empty($y)){
            $this->as = array('estado_cuenta'=>'transacciones');
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de cuenta de sucursales');
            $crud->set_relation('sucursales_id','sucursales','{nombre_sucursal}');
            $crud->callback_column('sdfe04682',function($val,$row){
                return '<a href="'.base_url('sucursal/estado_cuenta/'.$row->sucursales_id).'">'.$val.'</a>';
            });
            $crud->callback_column('monto',function($val){
                return '$'.$val;
            });
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_columns('tipo')
                 ->display_as('sucursales_id','Sucursal');
            if(empty($x) || !is_numeric($x)){
                $crud->where('tipo',2);
            }
            else{
                $crud->where('transacciones.sucursales_id',$x);
            }
            $output = $crud->render();
            $output->title = 'Estado de cuenta de sucursal';
            if(!empty($x)){
                $output->output = $this->load->view('includes/rangos',array('x'=>$x),TRUE).$output->output;
            }
            $this->loadView($output);
        }
        else{
            if(!empty($_POST['fechadesde'])){
                $this->db->where('transacciones.fecha >= ',date("Y-m-d",strtotime($_POST['fechadesde'])));
            }
            if(!empty($_POST['fechadesde'])){
                $this->db->where('transacciones.fecha <= ',date("Y-m-d",strtotime($_POST['fechahasta'])));
            }
            $this->db->where('transacciones.tipo',2);
            $this->db->select('transacciones.*, sucursales.*, sucursales.id as sucursalid,pedidos.cliente as cliente, pedidos.direccion_entrega, pedidos.precio, pedidos.id as pedidoid');
            $this->db->join('sucursales','sucursales.id = transacciones.sucursales_id');
            $this->db->join('pedidos','pedidos.id = transacciones.pedidos_id','left');
            $this->db->where('transacciones.sucursales_id',$x);
            $estado_cuenta = $this->db->get('transacciones');
            if($estado_cuenta->num_rows>0 && !empty($_POST['fechadesde'])){
                $output = $this->load->view('reportes/estado_cuenta',array('estado'=>$estado_cuenta,'fechadesde'=>date("d-m-Y",strtotime($_POST['fechadesde'])),'fechahasta'=>date("d-m-Y",strtotime($_POST['fechahasta']))),TRUE);
                $this->load->library('html2pdf');
                $html2pdf = new HTML2PDF('L','L','fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->writeHTML(utf8_decode($output));                
                $html2pdf->Output('Resumen de ventas por sucursal.pdf');
                //echo $output;
            }else{
                echo 'No se encontraron registros';
            }
        }
        }
        
        function conectadosList(){
            $this->as = array('conectadosList'=>'sucursales');
            $crud = $this->crud_function('','');
            $crud->set_subject('Sucursales conectadas');            
            $crud->where('status',1);
            $crud->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_delete()
                     ->unset_export()
                     ->unset_print();
            $crud->field_type('status','dropdown',
                            array(
                                '0'=>'<span class="label label-default">Desconectado</span>',
                                '1'=>'<span class="label label-success">Conectado</span>'));
            $crud->columns('nombre_sucursal','email','status');
            $crud = $crud->render();
            echo $crud->output;
        }
    }
?>

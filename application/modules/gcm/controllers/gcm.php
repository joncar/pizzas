<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Gcm extends Panel{
        function __construct() {
            parent::__construct();
        }                
        
        function sendMensage(){
            $this->form_validation->set_rules('mensaje','Mensaje','required');
            $this->form_validation->set_rules('app_id','Mensaje','required');
            if($this->form_validation->run()){
                $this->load->library('gcm_library');
                $this->gcm_library->Send($_POST['app_id'],$this->gcm_library->encodeMsj('Mensaje del administrador',$_POST['mensaje']));
                echo 'Mensaje enviado';
            }
        }
    }
?>

<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function pedidos($x = '',$y = ''){
            $this->load->library('multicrud');
            $c = new Multicrud();
            $crud = $this->crud_function('','');
            $crud->set_subject('Servicio');
            switch($x){
                case 'repartidor':if(is_numeric($y)){$crud->where('repartidores_id',$y); } break;
                case 'sucursal':if(is_numeric($y)){$crud->where('sucursales_id',$y); } break;
            }
            $crud->display_as('repartidores_id','Repartidor')
                     ->display_as('id','No. envio')
                     ->display_as('sucursales_id','Sucursal')
                     ->display_as('gratuitos','Gratis gastados');
            $crud->set_relation('repartidores_id','repartidores','nombre_repartidor');
            $crud->set_relation('sucursales_id','sucursales','nombre_sucursal');
            
            $crud->columns('id','fecha_solicitud','sucursales_id','repartidores_id','precio','gratuitos','Tareas','Tareas Pendientes','status');
            $crud->callback_column('Tareas',function($val,$row){
                return (string)get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id))->num_rows;
            });
            $crud->callback_column('Tareas Pendientes',function($val,$row){
                return (string)get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id,'status'=>0))->num_rows;
            });
            
            $crud->callback_column('sdfe04682',function($val,$row){
                $n = get_instance()->db->get_where('repartidores',array('id'=>$row->repartidores_id));
                if($n->num_rows>0){
                    $rep = $n->row();
                    $calificacion = empty($rep->calificacion)?'0':$rep->calificacion;
                    $foto = base_url('images/repartidores/'.$rep->foto);
                }
                return empty($val) || $n->num_rows==0?
                '<span class="label label-danger">'.$val.'</span>':
                '<a class="label label-info" href="javascript:notificarSucursal('.$row->sucursales_id.','.$row->repartidores_id.','.$row->id.',\''.$foto.'\',\''.$rep->placa.'\',\''.$rep->nombre_repartidor.'\','.$calificacion.','.$row->tiempo_total.')" title="Notificar a sucursal"><i class="fa fa-bell"></i> '.$val.'</a>';
            });
            
            $crud->callback_column('s8c93d82d',function($val,$row){                
                return empty($val)?
                '<span class="label label-danger">Repartidor no asignado</span>':
                '<a class="label label-info" href="javascript:notificarRepartidor('.$row->id.','.$row->repartidores_id.')" title="Notificar al repartidor"><i class="fa fa-bell"></i> '.$val.'</a>';
            });
            $crud->fields('repartidores_id','status');
            $crud->field_type('status','dropdown',array('-2'=>'<span class="label label-danger">Cancelado por el banco</span>','-1'=>'<span class="label label-danger">Cancelado</span>','1'=>'<span class="label label-default">Por recoger</span>','2'=>'<span class="label label-info">En camino</span>','3'=>'<span class="label label-success">Entregado</span>','4'=>'<span class="label label-warning">Programado</span>'))
                 ->unset_add();
            $crud->callback_before_delete(function($primary){
                get_instance()->db->delete('pedidos_detalles',array('pedidos_id'=>$primary));
                return true;
            });          
            
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': return '<span class="label label-danger">Cancelado</span>';
                    break; 
                    
                    case '1': 
                        if($row->{'Tareas'} == $row->{'Tareas Pendientes'}){
                            return '<span class="label label-default">Por recoger</span>';
                        }else{
                            return '<span class="label label-info">En camino</span>';
                        }
                    
                    break;
                
                    case '2': return '<span class="label label-info">En camino</span>';
                    break;
                    case '3': return '<span class="label label-success">Entregado</span>';
                    break; 
                    case '4': return '<span class="label label-warning">Programado</span>';
                    break;
                }
            });
            $crud->callback_before_update(function($post,$primary){
                /*$gcm = get_instance()->db->get_where('repartidores',array('id'=>$post['repartidores_id']));
                if($gcm->num_rows>0){
                    $gcm = $gcm->row()->gcm;
                    get_instance()->load->library('gcm_library');
                    get_instance()->gcm_library->Send(array($gcm),$this->gcm_library->encodeMsj('Mensaje del administrador','El administrador te ha asignado un envio, puedes ver la información en la sección pedidos de la app deliverymix repartidor'));
                }*/
            });
            $c->addCrud($crud);
            
            $detalles = new ajax_grocery_CRUD();
            $detalles->set_table('pedidos_detalles');
            $detalles->set_subject('Tareas');
            $detalles->set_theme('crud_dependency');
            $detalles->dependency = array('pedidos_id'=>'pedidos.id');
            $detalles->fields('pedidos_id','direccion','status','tipo','tipo_pago','propina');
            $detalles->callback_field('status',function($val){
                return form_dropdown('status_0_pedidos_detalles',array(0=>'Pendiente',1=>'Procesado'),$val,'class="form-control" id="field-status" data-name="status" data-table="pedidos_detalles"');
            });
            $detalles->callback_field('tipo',function($val){
                return form_dropdown('tipo_0_pedidos_detalles',array(0=>'Entrega',1=>'Recolecta'),$val,'class="form-control" id="field-tipo" data-name="tipo" data-table="pedidos_detalles"');
            });
            $detalles->callback_field('tipo_pago',function($val){
                return form_dropdown('tipo_pago_0_pedidos_detalles',array(0=>'Efectivo',1=>'Tarjeta'),$val,'class="form-control" id="field-tipo_pago" data-name="tipo_pago" data-table="pedidos_detalles"');
            });
            $c->addCrud($detalles);
            
            $output = $c->render();
            $output->crud = 'pedidos';
            $output->title = 'Historial de servicios';
            $this->loadView($output);
        }
        
        function conectadosList(){
            $this->as = array('conectadosList'=>'pedidos');
            $crud = $this->crud_function('','');
            $crud->set_subject('Solicitudes');     
            $crud->where('pedidos.status !=',3);
            $crud->where('pedidos.status !=',-1);
            $crud->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_delete()
                     ->unset_export()
                     ->unset_print();
            $crud->callback_column('Tareas',function($val,$row){
                return (string)get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id))->num_rows;
            });
            $crud->callback_column('Tareas Pendientes',function($val,$row){
                return (string)get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id,'status'=>0))->num_rows;
            });
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': return '<span class="label label-danger">Cancelado</span>';
                    break; 
                    
                    case '1': 
                        if($row->{'Tareas'} == $row->{'Tareas Pendientes'}){
                            return '<span class="label label-default">Por recoger</span>';
                        }else{
                            return '<span class="label label-info">En camino</span>';
                        }
                    
                    break;
                
                    case '2': return '<span class="label label-info">En camino</span>';
                    break;
                    case '3': return '<span class="label label-success">Entregado</span>';
                    break; 
                    case '4': return '<span class="label label-warning">Programado</span>';
                    break;
                }
            });            
            $crud->display_as('repartidores_id','Repartidor')
                     ->display_as('id','No. envio')
                     ->display_as('sucursales_id','Sucursal')
                     ->display_as('gratuitos','Gratis gastados');
            $crud->set_relation('repartidores_id','repartidores','nombre_repartidor');
            $crud->set_relation('sucursales_id','sucursales','nombre_sucursal');
            $crud->columns('repartidores_id','sucursales_id','Tareas','Tareas Pendientes','status');
            $crud = $crud->render();
            echo $crud->output;
        }
    }
?>

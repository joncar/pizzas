<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Equipo extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function equipos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);     
            $crud->set_relation('repartidores_id','repartidores','nombre_repartidor');            
            $crud->display_as('anio','Año')
                     ->display_as('repartidores_id','Repartidor')
                     ->display_as('seguros_id','Poliza de seguro');
            $output = $crud->render();
            $this->loadView($output);
        }        
        function seguros($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);            
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>

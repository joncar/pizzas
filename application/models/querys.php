<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getSucursales(){                
        $this->db->where('status',1);
        return $this->db->get('sucursales')->num_rows;
    }
    
    function get_cobrado(){
        if(!empty($_SESSION['filtros'])){
            if(!empty($_SESSION['filtros']['fechadesde'])){
                $this->db->where('fecha >= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechadesde'])));
            }
            if(!empty($_SESSION['filtros']['fechahasta'])){
                $this->db->where('fecha <= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechahasta'])));
            }
        }
        $this->db->where('tipo',2);
        $this->db->select('SUM(monto) as total',TRUE);
        return $this->db->get('transacciones')->row()->total;
    }
    
    function getMotorizados(){
        $motos = array();
        $this->db->where('status != ',5);
        foreach($this->db->get('repartidores')->result() as $m){
            array_push($motos,array('lat'=>$m->lat,'lon'=>$m->lon,'nombre'=>$m->nombre_repartidor,'foto'=>$m->foto,'rank'=>$m->calificacion,'placa'=>$m->placa));
        }
        return $motos;
    }
    
    function getTransacciones(){
        if(!empty($_SESSION['filtros'])){
            if(!empty($_SESSION['filtros']['fechadesde'])){
                $this->db->where('fecha >= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechadesde'])));
            }
            if(!empty($_SESSION['filtros']['fechahasta'])){
                $this->db->where('fecha <= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechahasta'])));
            }
        }
        $this->db->order_by('id','DESC');
        $this->db->limit('10');
        $taxistas = array();
        foreach($this->db->get('transacciones')->result() as $m){
            array_push($taxistas,$m);
        }
        return $taxistas;
    }
    
    function getEntregas(){
        if(!empty($_SESSION['filtros'])){
            if(!empty($_SESSION['filtros']['fechadesde'])){
                $this->db->where('fecha >= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechadesde'])));
            }
            if(!empty($_SESSION['filtros']['fechahasta'])){
                $this->db->where('fecha <= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechahasta'])));
            }
        }
        $this->db->order_by('id','DESC');
        $this->db->limit('10');
        $this->db->where('tipo',1);
        $taxistas = array();
        foreach($this->db->get('transacciones')->result() as $m){
            array_push($taxistas,$m);
        }
        return $taxistas;
    }
    
    function getCobro(){
        if(!empty($_SESSION['filtros'])){
            if(!empty($_SESSION['filtros']['fechadesde'])){
                $this->db->where('fecha >= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechadesde'])));
            }
            if(!empty($_SESSION['filtros']['fechahasta'])){
                $this->db->where('fecha <= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechahasta'])));
            }
        }
        $this->db->order_by('id','DESC');
        $this->db->limit('10');
        $this->db->where('tipo',2);
        $taxistas = array();
        foreach($this->db->get('transacciones')->result() as $m){
            array_push($taxistas,$m);
        }
        return $taxistas;
    }
    
    function getServiciosSucursalDestino(){
        if(!empty($_SESSION['filtros'])){
            if(!empty($_SESSION['filtros']['fechadesde'])){
                $this->db->where('fecha_solicitud >= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechadesde'])));
            }
            if(!empty($_SESSION['filtros']['fechahasta'])){
                $this->db->where('fecha_solicitud <= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechahasta'])));
            }
        }
        $this->db->order_by('id','DESC');
        $this->db->limit('10');
        $this->db->where('pedidos.status',2);
        $this->db->select('pedidos.*, sucursales.nombre_sucursal as sucursal');
        $this->db->join('sucursales','sucursales.id = pedidos.sucursales_id');
        $taxistas = array();
        foreach($this->db->get('pedidos')->result() as $m){
            array_push($taxistas,$m);
        }
        return $taxistas;
    }
    
    function getPedidos(){
        if(!empty($_SESSION['filtros'])){
            if(!empty($_SESSION['filtros']['fechadesde'])){
                $this->db->where('fecha_solicitud >= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechadesde'])));
            }
            if(!empty($_SESSION['filtros']['fechahasta'])){
                $this->db->where('fecha_solicitud <= ',date("Y-m-d",strtotime($_SESSION['filtros']['fechahasta'])));
            }
        }
        
        return $this->db->get('pedidos')->num_rows;
    }
    
    function get_resumenes(){
        $resumenes = array();
        $resumenes['pedidos'] = $this->getPedidos();
        $resumenes['repartidores'] = $this->db->get('repartidores')->num_rows;
        $resumenes['sucursales'] = $this->getSucursales();
        $resumenes['cobrado'] = $this->get_cobrado();
        $resumenes['motorizados'] = $this->getMotorizados();
        $resumenes['transacciones'] = $this->getTransacciones();
        $resumenes['entregas'] = $this->getEntregas();
        $resumenes['cobros'] = $this->getCobro();
        $resumenes['servicios'] = $this->getServiciosSucursalDestino();
        return $resumenes;
    }
}
?>

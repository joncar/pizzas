<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Cron extends Main {
        
        public function __construct()
        {
                parent::__construct();                
        }
        
        public function index()
        {
            $this->load->database();
            $min = date("i");            
            if($min=='00' || $min=='30'){
                foreach($this->db->get_where('repartidores_horarios',array('hora'=>date("H").':'.$min.':00','dia'=>date("w")))->result() as $r){
                    $this->db->update('repartidores',array('status'=>$r->status),array('id'=>$r->repartidores_id));
                }
            }            
        }        
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */

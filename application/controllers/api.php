<?php
require_once APPPATH.'/controllers/main.php';
require_once APPPATH.'/libraries/Conekta.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();                
                if(empty($_SERVER['HTTP_VER'])){          
                    echo 'denied';
                    die();
                }            
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
        }
        
        public function loadView($data)
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $crud->set_model('api_model');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function ajustes(){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function direcciones_favoritas(){
            $crud = $this->crud_function('','');
            $crud->callback_column('nombre',function($val,$row){
                return $row->nombre;
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function repartidores($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            $crud->unset_fields('fecha_registro');
            $crud->required_fields('email');
            /*if(empty($y) || !empty($_POST) && $_POST['email']!=$this->db->get_where('usuarios',array('id'=>$y))->row()->email){
                $crud->set_rules('email','Email','required|valid_email|is_unique[usuarios.email]');
            }*/
            $crud->callback_column('tarifa',function($val,$row){
                $sql = "SELECT 
                pedidos.id, 
                pedidos.sucursales_id, 
                (SELECT count(pedidos.id) from pedidos where pedidos.status=3 AND repartidores_id=".$row->id." AND DATE(fecha_solicitud) = '".date("Y-m-d")."') as pedidos,
                (SELECT count(pedidos.id) from pedidos where pedidos.status=3 AND repartidores_id=".$row->id." AND MONTH(fecha_solicitud) = '".date("m")."') as mes
                FROM `pedidos` 
                INNER JOIN pedidos_detalles ON pedidos_detalles.pedidos_id = pedidos.id
                WHERE pedidos.status=3 AND repartidores_id=".$row->id." AND DATE(fecha_solicitud) = '".date("Y-m-d")."'";
                $pedidos = get_instance()->db->query($sql);
                $hoy = $pedidos->num_rows>0?$pedidos->row()->pedidos:0;
                $mes = $pedidos->num_rows>0?$pedidos->row()->mes:0;
                return json_encode(array('hoy'=>$hoy,'mes'=>$mes));
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }    
        
         public function sucursales($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            if($x!='web'){
                $crud->unset_fields('fecha_registro','ubicacion','lat','lon');            
            }
            if($crud->getParameters()=='add' || $crud->getParameters()=='insert_validation' || $crud->getParameters()=='insert'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[sucursales.email]');
                $crud->required_fields('responsable','nombre_sucursal','telefono_fijo','telefono_celular','password');            
            }else{
                $crud->required_fields('');
            }
            
            $crud->field_type('precio_por_envio','invisible');
            $crud->callback_before_insert(function($post){
                $post['precio_por_paquete'] = $this->db->get('ajustes')->row()->precio_por_envio_sin_contrato;
                return $post;
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function refreshLocation(){
            $data = array();
            if(!empty($_POST['lat']) && !empty($_POST['lon'])){
                $data['lat'] = $_POST['lat'];
                $data['lon'] = $_POST['lon'];
                $data['ubicacion'] = '('.$_POST['lat'].','.$_POST['lon'].')';
            }
            if(!empty($_POST['status'])){
                $data['status'] = $_POST['status'];
            }
            //Traer pedidos activos
            $pedidos = $this->db->get_where('pedidos',array('status != '=>'3','status >'=>'0','repartidores_id'=>$_POST['id']))->num_rows;
            if($data['status']==1 && $pedidos>0){
                $data['status'] = 2;
            }
            $this->db->update('repartidores',$data,array('id'=>$_POST['id']));
            echo json_encode(array());
        }
        
        public function pedidos(){
            $crud = $this->crud_function('',''); 
            $crud->set_relation('repartidores_id','repartidores','nombre_repartidor');
            $crud->set_relation('sucursales_id','sucursales','{nombre_sucursal}|{telefono_celular}');
            $crud->columns('precio_cliente','repartidores_id','sucursales_id','status','precio','gratuitos'
                                      ,'fecha_solicitud','fecha_entregado','tiempo_entrega','tiempo_recoleccion',
                                      'tiempo_total','tiempo_transito','trayecto','telefono','sucursal','banco_deposito','tarjeta_debito');            
            $crud->callback_column('trayecto',function($val,$row){
               $pedido = [];
               foreach(get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id))->result() as $p){
                   $pedido[] = $p;
               }
               return json_encode($pedido);
            });
            
            $crud->callback_column('banco_deposito',function($val,$row){               
               $suc = get_instance()->db->get_where('sucursales',array('id'=>$row->sucursales_id));
               if($suc->num_rows>0){
                   return $suc->row()->banco_deposito;
               }else{
                   return '';
               }               
            });
            
            $crud->callback_column('tarjeta_debito',function($val,$row){               
               $suc = get_instance()->db->get_where('sucursales',array('id'=>$row->sucursales_id));
               if($suc->num_rows>0){
                   return $suc->row()->tarjeta_debito;
               }else{
                   return '';
               }               
            });
            
            $crud->callback_column('direccion_entrega',function($val,$row){
                    $ped = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id));
                    if($ped->num_rows>0){
                        return $ped->row()->direccion;
                    }else{
                        return $val;
                    }
            });             
            $crud->callback_column('telefono',function($val,$row){              
               list($nombre,$tel) = explode('|',$row->sdfe04682);
               return $tel;
            });
            $crud->callback_column('sucursal',function($val,$row){
               list($nombre,$tel) = explode('|',$row->sdfe04682);
               return $nombre;
            });
            
            
            $crud->callback_before_delete(function($primary){
                $pedido = get_instance()->db->get_where('pedidos',array('id'=>$primary));
                if($pedido->num_rows>0){
                    $pedido = $pedido->row();
                    $gratis = get_instance()->db->get_where('sucursales',array('id'=>$pedido->sucursales_id));
                    if($gratis->num_rows>0){
                        $gratis = $gratis->row()->envios_sin_costo+1;
                        get_instance()->db->update('sucursales',array('envios_sin_costo'=>$gratis),array('id'=>$pedido->sucursales_id));
                    }
                }
                return true;
            });
            $crud->callback_before_insert(function($post){
                $post['fecha_solicitud'] = date("Y-m-d H:i:s",strtotime($post['fecha_solicitud']));
                $post['status'] = -2;
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('mensajes',array('pedidos_id'=>$primary,'mensajes'=>'[]'));
                $sucursal = get_instance()->db->get_where('sucursales',array('id'=>$post['sucursales_id']));
                if($sucursal->num_rows>0){
                    $sucursal = $sucursal->row();
                    if($sucursal->envios_sin_costo>0){
                        get_instance()->db->update('pedidos',array('status'=>1),array('id'=>$primary));
                        get_instance()->db->update('sucursales',array('envios_sin_costo'=>$sucursal->envios_sin_costo-1),array('id'=>$post['sucursales_id']));
                        get_instance()->db->insert('transacciones',
                        array(
                            'sucursales_id'=>$sucursal->id,
                            'monto'=>$sucursal->precio_por_paquete,
                            'fecha'=>date("Y-m-d H:i:s"),
                            'detalle'=>"Pedido entregado a ".$post['cliente'].' con el No. de identificación: '.$primary,
                            'tipo'=>2,
                            'pedidos_id'=>$primary
                        ));
                    }else{
                        $this->connectToRest($post,$primary);
                    }
                }
                return true;
            });            
            if($crud->getParameters()=='edit' || $crud->getParameters()=='update' || $crud->getParameters()=='update_validation'){
                $crud->required_fields('status');
            }            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function pedidos_detalles(){
            $crud = $this->crud_function('',''); 
            $crud->fields('status','tipo_pago');
            $crud->required_fields('status');
            
            $crud->columns('pedidos_id','lat','lon','ubicacion','direccion','fecha','status','tipo','precio','cliente','tipo_pago','propina','sucursales_id','sdfe04682','public_key','precio_cliente');

            $crud->callback_column('public_key',function($val,$row){
               $sucursal = get_instance()->db->get_where('sucursales',array('id'=>$row->sucursales_id));
               if($sucursal->num_rows>0){
                   if($sucursal->row()->public_key!=null && $sucursal->row()->private_key!=null && strlen($sucursal->row()->public_key)>10 && substr($sucursal->row()->public_key,0,3)=='key'){
                        return $sucursal->row()->public_key;
                   }else{
                       //return substr($sucursal->row()->public_key,0,3);
                   }
               }else{
                   return null;
               }
            }); 
            $crud->callback_column('sdfe04682',function($val,$row){
                get_instance()->db->join('pedidos','pedidos.sucursales_id = sucursales.id');
               return get_instance()->db->get_where('sucursales',array('pedidos.id'=>$row->pedidos_id))->row()->nombre_sucursal; 
            });
            $crud->callback_column('sucursales_id',function($val,$row){
                get_instance()->db->join('pedidos','pedidos.sucursales_id = sucursales.id');
               return get_instance()->db->get_where('sucursales',array('pedidos.id'=>$row->pedidos_id))->row()->sucursales_id; 
            });
            
            $crud->callback_column('precio_cliente',function($val,$row){
               $ta = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->pedidos_id));
               $tareas = $ta->num_rows-1;
               $precio = get_instance()->db->get_where('pedidos',array('id'=>$row->pedidos_id));
               if($precio->num_rows>0){
                   $p = $precio->row()->precio_cliente/$tareas;
                   if($row->id == $ta->row()->id){
                       return 0;
                   }else{
                       return $p;
                   }
               }else{
                   return 0;
               }               
            });
            
            $crud->callback_after_update(function($post,$id){
                $actualtask = get_instance()->db->get_where('pedidos_detalles',array('id'=>$id));
                if($actualtask->num_rows>0){
                    $task = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$actualtask->row()->pedidos_id,'status'=>0));
                    if($task->num_rows==0){
                        get_instance()->db->update('pedidos',array('status'=>3),array('id'=>$actualtask->row()->pedidos_id));
                    }
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function addPedido(){
            $this->form_validation->set_rules('task','Datos','required');
            $this->form_validation->set_rules('sucursales_id','ID','required');
            $this->form_validation->set_rules('precio','ID','required');
            $this->form_validation->set_rules('precio_cliente','ID','required');            
            if($this->form_validation->run()){
                $sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursales_id']));
                $task = json_decode($_POST['task']);
                //Validar si los datos recibidos del precio son correctos
                if($sucursal->num_rows>0 && (((count($task)-1)*$sucursal->row()->precio_por_paquete) == ($_POST['precio']+$_POST['precio_cliente']))){
                    $sucursal = $sucursal->row();
                    $fecha_solicitud = date("Y-m-d H:i:s",strtotime($_POST['fecha_solicitud']));
                    $this->db->insert('pedidos',array('sucursales_id'=>$_POST['sucursales_id'],'status'=>-2,'fecha_solicitud'=>$fecha_solicitud,'precio'=>$_POST['precio'],'precio_cliente'=>$_POST['precio_cliente']));
                    $primary = $this->db->insert_id();
                    //Añadir datos faltantes
                    foreach($task as $t){                    
                        $data = array();                    
                        $data['precio'] = !empty($t->precio)?$t->precio:'0';
                        $data['cliente'] = !empty($t->cliente)?$t->cliente:'0';
                        $data['tipo_pago'] = isset($t->tipo_pago)?$t->tipo_pago:'1';
                        $data['pedidos_id'] = $primary;
                        $data['lat'] = $t->lat;
                        $data['lon'] = $t->lon;
                        $data['ubicacion'] = '('.$t->lat.','.$t->lon.')';
                        $data['direccion'] = $t->direccion;
                        $data['status'] = 0;
                        $data['tipo'] = $t->tipo;
                        $this->db->insert('pedidos_detalles',$data);
                    }
                    //Aplicamos el after insert
                    //$this->db->insert('mensajes',array('pedidos_id'=>$primary,'mensajes'=>'[]'));                                                    
                    if($sucursal->envios_sin_costo>$_POST['precio']){                        
                        $saldo = $sucursal->envios_sin_costo-$_POST['precio'];
                        if($saldo<0){
                            echo 'denied';
                        }else{                            
                            $this->db->update('pedidos',array('status'=>1),array('id'=>$primary));
                            $this->db->update('sucursales',array('envios_sin_costo'=>$saldo),array('id'=>$_POST['sucursales_id']));
                            $this->db->insert('transacciones',
                            array(
                                'sucursales_id'=>$sucursal->id,
                                'monto'=>$_POST['precio'],
                                'fecha'=>date("Y-m-d H:i:s"),
                                'detalle'=>"Pedido solicitado por ".$sucursal->nombre_sucursal." con el No. de identificación: ".$primary,
                                'tipo'=>2,
                                'pedidos_id'=>$primary
                            ));
                        }                                               
                    }else{
                        $this->connectToRest($_POST,$primary);
                    }
                    echo json_encode(array('status'=>true,'insert_primary_key'=>$primary));
                }else{
                    echo json_encode(array('status'=>false));
                }                
            }else{
                echo json_encode(array('status'=>false));
            }
        }
        
        public function updateTask(){
            if(!empty($_POST['task'])){
                $task = json_decode($_POST['task']);
                foreach($task as $n=>$t){
                    if(!empty($t->primary)){
                        $data = array();                    
                        $data['precio'] = !empty($t->precio)?$t->precio:'0';
                        $data['cliente'] = !empty($t->cliente)?$t->cliente:'0';
                        $data['tipo_pago'] = isset($t->tipo_pago)?$t->tipo_pago:'1';                        
                        $data['lat'] = $t->lat;
                        $data['lon'] = $t->lon;
                        $data['ubicacion'] = '('.$t->lat.','.$t->lon.')';
                        $data['direccion'] = $t->direccion;
                        $data['status'] = $n==0?1:0;
                        $this->db->update('pedidos_detalles',$data,array('id'=>$t->primary));
                    }
                }
            }
        }
        
        public function pedidosMotos(){            
            if(!empty($_POST['search_field'])){
                $this->db->select('pedidos.*, pedidos_detalles.lat, pedidos_detalles.lon, pedidos_detalles.direccion, pedidos_detalles.tipo');
                $this->db->join('pedidos_detalles','pedidos_detalles.pedidos_id = pedidos.id AND pedidos_detalles.status=0');
                $pedido = $this->db->get_where('pedidos',array('repartidores_id'=>$_POST['search_text'][0],'pedidos.status != '=>3));
                if($pedido->num_rows>0){
                    echo json_encode(array(0=>$pedido->row()));
                }else{
                    echo '[]';
                }
            }
            else{
                echo '[]';
            }
        }
        
        public function transacciones(){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                $post['fecha'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->sendRecibo($_POST['pedidos_id'],$primary);
            });
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
            
        }
    
    public function getMensajes(){   
        $this->form_validation->set_rules('pedido_id','ID','required');            
        if($this->form_validation->run()){
            $mensajes = $this->db->get_where('mensajes',array('pedidos_id'=>$this->input->post('pedido_id')));
            if($mensajes->num_rows>0){
                echo json_encode(utf8_decode($mensajes->row()->mensajes));
            }
            else{
                echo json_encode(array());
            }
        }
        else{
            echo json_encode(array());
        }
    } 
    
    public function connectToRest($post,$id){        
        $key = get_instance()->db->get('ajustes')->row()->private_key;
        Conekta::setApiKey($key);
        $sucursal = $this->db->get_where('sucursales',array('id'=>$post['sucursales_id']));                
        if($sucursal->num_rows>0){            
            $sucursal = $sucursal->row();
            
            $saldopendiente=$_POST['precio']-$sucursal->envios_sin_costo;
            if($saldopendiente>0){
                //Descontar los gratuitos
                //Proseguir con el pago
                $sucursal->precio_por_paquete = $saldopendiente;
                try {
                    $charge = Conekta_Charge::create(array(
                        "description"=>"Nro Pedido: ".$id,
                        "amount"=> ((int)$sucursal->precio_por_paquete*100),
                        "currency"=>"MXN",
                        "reference_id"=>3,
                        "card"=> $_POST['card'],
                        "details" =>array(
                          "email" => $sucursal->email,
                          "name"=> "Pedido Nro: ".$id,
                          "phone"=>$sucursal->telefono_celular,
                          "line_items"=> array(array(
                            "name"=> "Pedido Nro: ".$id,
                            "description"=> "Pedido solicitado por ".$sucursal->nombre_sucursal." con el No. de identificación: ".$id,
                            "unit_price"=> ((int)$sucursal->precio_por_paquete*100),
                            "quantity"=> 1,
                            "sku"=> "cohb_s1",
                            "type"=> "pizza-purchase"
                          )),
                          "shipment"=>array(
                            "carrier"=>"deliveryMix",
                            "service"=>"local",
                            "price"=>0,
                            "address"=>array(
                                "street1"=>"250 Alexis St",
                                "street2"=>null,
                                "street3"=>null,
                                "city"=>"Red Deer",
                                "state"=>"Alberta",
                                "zip"=>"T4N 0B8",
                                "country"=>"mx"
                            ))
                          )
                        )
                    );
                    get_instance()->db->update('pedidos',array('status'=>1,'precio'=>$sucursal->precio_por_paquete),array('id'=>$id));
                    get_instance()->db->insert('transacciones',
                            array(
                                'sucursales_id'=>$sucursal->id,
                                'monto'=>$sucursal->precio_por_paquete,
                                'fecha'=>date("Y-m-d H:i:s"),
                                'detalle'=>"Pedido solicitado por ".$sucursal->nombre_sucursal." con el No. de identificación: ".$id,
                                'tipo'=>2,
                                'pedidos_id'=>$id
                                ));
                } catch (Conekta_Error $e) {
                     print_r($e);
                    //echo $e->message_to_purchaser;
                  exit;
                }
            }else{
                echo 'Error de concurrencia';
                 get_instance()->db->update('pedidos',array('status'=>-3),array('id'=>$id));
            }
        }
    }
    
    public function payFromClient($pedidoid){                    
        $this->db->select('pedidos_detalles.*,pedidos.id as pedidoid, sucursales.id as sucursales_id, sucursales.nombre_sucursal as sucursal, sucursales.private_key');
        $this->db->join('pedidos','pedidos.id = pedidos_detalles.pedidos_id');
        $this->db->join('sucursales','sucursales.id = pedidos.sucursales_id');
        $pedido = $this->db->get_where('pedidos_detalles',array('pedidos_detalles.id'=>$pedidoid));        
        if($pedido->num_rows>0){
            $pedido = $pedido->row();
            $enviarrecibo = true;
            //Verificar la api de la sucursal
            if($pedido->private_key!=null && strlen($pedido->private_key)>10 && substr($pedido->private_key,0,3)=='key'){
                $key = $pedido->private_key;
                $enviarrecibo = false;
            }else{
                //si no tiene api se le abona el pago a deliverymix
                $key = get_instance()->db->get('ajustes')->row()->private_key;
            }
            if($pedido->precio>0){
                Conekta::setApiKey($key);
                try {
                    $pedidoid = $pedido->pedidoid;
                    $total = $pedido->precio;
                    $propina = 0;
                    $charge = Conekta_Charge::create(array(
                        "description"=>"Nro Pedido: ".$pedidoid,
                        "amount"=> ((int)$total*100),
                        "currency"=>"MXN",
                        "reference_id"=>3,
                        "card"=> $_POST['tarjeta'],
                        "details" =>array(
                          "email" => $_POST['email'],
                          "name" => $_POST['nombre'],                      
                          "phone"=>$_POST['telefono'],
                          "line_items"=> array(array(
                            "name"=> "Pedido Nro: ".$pedidoid,
                            "description"=> 'Pago a la sucursal  '.$pedido->sucursal.' por un monto de $ '.$total.' por parte de '.$pedido->cliente.' con el #envio: '.$pedidoid,
                            "unit_price"=> ((int)$total*100),
                            "quantity"=> 1,
                            "sku"=> "cohb_s1",
                            "type"=> "pizza-purchase"
                          )),
                          "shipment"=>array(
                            "carrier"=>"deliveryMix",
                            "service"=>"local",
                            "price"=>0,
                            "address"=>array(
                                "street1"=>"250 Alexis St",
                                "street2"=>null,
                                "street3"=>null,
                                "city"=>"Red Deer",
                                "state"=>"Alberta",
                                "zip"=>"T4N 0B8",
                                "country"=>"mx"
                           ))
                    )));

                    $this->db->update('pedidos_detalles',array('status'=>1),array('id'=>$pedido->id));                
                    if($enviarrecibo){
                            get_instance()->db->insert('transacciones',
                                    array(
                                        'sucursales_id'=>$pedido->sucursales_id,
                                        'monto'=>$total,
                                        'fecha'=>date("Y-m-d H:i:s"),
                                        'detalle'=>"Pago a la sucursal $".$pedido->sucursal.' por un monto de '.$total.' por parte de '.$pedido->cliente.' con el #envio: '.$pedidoid,
                                        'tipo'=>1,
                                        'pedidos_id'=>$pedidoid
                                    ));
                            $this->sendRecibo($pedido->id,get_instance()->db->insert_id());
                    }
                    //Verificamos si hay que actualizar el pedido                
                    $task = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$pedido->pedidoid,'status'=>0));
                    if($task->num_rows==0){
                        get_instance()->db->update('pedidos',array('status'=>3),array('id'=>$pedidoid));
                    }

                    //Cobrar envio
                    if($this->get_costo_envio($pedidoid)>0){
                        $this->cobrar_envio($pedidoid,$pedido);
                    }else{
                        echo 'success';
                    }
                    
                } catch (Conekta_Error $e) {
                  //print_r($e);
                  echo $e->message_to_purchaser;
                  //El pago no pudo ser procesado
                }
            }
            else{
                //Cobrar envio
                $pedidoid = $pedido->pedidoid;
                if($this->get_costo_envio($pedidoid)>0){                    
                    $this->cobrar_envio($pedidoid,$pedido);
                }else{
                    echo 'success';
                }
            }
        }
    }

    function get_costo_envio($pedidoid){
       $tareas = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$pedidoid))->num_rows-1;
       $precio = get_instance()->db->get_where('pedidos',array('id'=>$pedidoid));
       if($precio->num_rows>0){
           return $precio->row()->precio_cliente/$tareas;
       }else{
           return 0;
       }
    }

    function cobrar_envio($pedidoid,$pedido){
       $total = $this->get_costo_envio($pedidoid);
       $key = get_instance()->db->get('ajustes')->row()->private_key;
       Conekta::setApiKey($key);
        try {
            $charge = Conekta_Charge::create(array(
                "description"=>"Nro Pedido: ".$pedidoid,
                "amount"=> ((int)$total*100),
                "currency"=>"MXN",
                "reference_id"=>3,
                "card"=> $_POST['tarjeta2'],
                "details" =>array(
                  "email" => $_POST['email'],
                  "name" => $_POST['nombre'],                      
                  "phone"=>$_POST['telefono'],
                  "line_items"=> array(array(
                    "name"=> "Pedido Nro: ".$pedidoid,
                    "description"=> 'Pago por envio de un cliente de la sucursal '.$pedido->sucursal.' por un monto de $ '.$total.' por parte de '.$pedido->cliente.' con el #envio: '.$pedidoid,
                    "unit_price"=> ((int)$total*100),
                    "quantity"=> 1,
                    "sku"=> "cohb_s1",
                    "type"=> "pizza-purchase"
                  )),
                  "shipment"=>array(
                    "carrier"=>"deliveryMix",
                    "service"=>"local",
                    "price"=>0,
                    "address"=>array(
                        "street1"=>"250 Alexis St",
                        "street2"=>null,
                        "street3"=>null,
                        "city"=>"Red Deer",
                        "state"=>"Alberta",
                        "zip"=>"T4N 0B8",
                        "country"=>"mx"
                   ))
            )));
            get_instance()->db->insert('transacciones',
            array(
                'sucursales_id'=>$pedido->sucursales_id,
                'monto'=>$total,
                'fecha'=>date("Y-m-d H:i:s"),
                'detalle'=>'Pago por envio de un cliente de la sucursal '.$pedido->sucursal.' por un monto de $ '.$total.' por parte de '.$pedido->cliente.' con el #envio: '.$pedidoid,
                'tipo'=>1,
                'pedidos_id'=>$pedidoid
            ));
            echo 'success';
        } catch (Conekta_Error $e) {
          //print_r($e);
          echo $e->message_to_purchaser;
          //El pago no pudo ser procesado
        }

    }
    
    public function getStructure($table){
        return $this->db->field_data($table);
    }
    
    public function sendRecibo($pedidoId,$nrorecibo){
         $this->db->select('pedidos_detalles.*,pedidos.id as pedidoid, pedidos.fecha_solicitud, repartidores.nombre_repartidor as repartidor,  sucursales.id as sucursales_id, sucursales.nombre_sucursal as sucursal');
        $this->db->join('pedidos','pedidos.id = pedidos_detalles.pedidos_id');
        $this->db->join('sucursales','sucursales.id = pedidos.sucursales_id');
        $this->db->join('repartidores','repartidores.id = pedidos.repartidores_id');
        $pedido = $this->db->get_where('pedidos_detalles',array('pedidos_detalles.id'=>$pedidoId));
        if($pedido->num_rows>0){
            $pedido = $pedido->row();
            $subtotal = (($pedido->precio*0.16)-$pedido->precio)*-1;
            $iva = ($pedido->precio*0.16);
            $msj = "
                <table border='1' width='100%'>
                    <thead style='background:#D9D9D9'>
                        <tr><th align='left'>RECIBO</th><th align='left' colspan='2'>No.</th></tr>
                    </thead>
                    <tbody>
                        <tr><td align='left'><b>DESCRIPCIÓN: </b> ".$pedido->sucursal."</td><td align='left' colspan='2'>".$pedido->pedidos_id."</td></tr>
                        <tr>
                            <td align='center' rowspan='3'><img src='".base_url('img/logo.png')."' style='width:70%'></td>
                            <th align='right'>SubTotal</th>
                            <td align='right'>$".$subtotal."</td>
                        </tr>
                        <tr>
                            <th align='right'>IVA</th>
                            <td align='right'>$".$iva."</td>
                        </tr>
                        <tr>
                            <th align='right'>Total</th>
                            <td align='right'>$".$pedido->precio."</td>
                        </tr>
                        <tr>
                            <td align='left'>Fecha: ".date("d-m-Y H:i:s",strtotime($pedido->fecha_solicitud))."</td>
                            <td align='left' colspan='3'>Recibido por: ".$pedido->repartidor."</td>
                        </tr>
                    </tbody>
                </table>
                <p>Si el cargo fue realizado con tarjeta de Débito o Crédito, en su estado de cuenta bancario aparecerá una leyenda con el nombre de Conekta relacionado a este recibo.</p>
                <p align='center'>Para nosotros fue un placer atenderle</p>
                <p>Para	cualquier duda, por favor envíe un correo a <a href='mailto:info@deliverymix.com.mx'>info@deliverymix.com.mx</a> o ingrese a <a href='http://www.deliverymix.com.mx'>www.deliverymix.com.mx</a> donde con gusto le atenderemos.</p>
            ";

            correo('joncar.c@gmail.com, sulkin@gmail.com','Hola '.$pedido->cliente.' te enviamos tu recibo de pago',$msj);
        }
    }
    
    public function calificaciones(){        
        $crud = $this->crud_function('','');                 
        $crud->callback_after_insert(function($post){
            $calificaciones = get_instance()->db->get_where('calificaciones',array('repartidores_id'=>$post['repartidores_id']));
            $suma = 0;
            foreach($calificaciones->result() as $s){
                $suma+= $s->calificacion;
            }
            
            get_instance()->db->update('repartidores',array('calificacion'=>$suma/$calificaciones->num_rows),array('id'=>$post['repartidores_id']));
        });        
        $crud = $crud->render();
        $this->loadView($crud);            
    }
    
    public function getBalance(){
        $this->form_validation->set_rules('sucursales_id','Sucursal','required|integer|greather_than[0]');
        if($this->form_validation->run()){
            $balance = array();
            $sql = "SELECT  
                sucursales.total_paquete,
                sucursales.envios_sin_costo,
                (select count(pedidos_detalles.id) from pedidos inner join pedidos_detalles on pedidos_detalles.pedidos_id = pedidos.id where DATE(pedidos.fecha_solicitud) = '".date("Y-m-d")."' AND sucursales_id=".$_POST['sucursales_id'].") as pedidos_hoy,
                (select count(pedidos_detalles.id) from pedidos inner join pedidos_detalles on pedidos_detalles.pedidos_id = pedidos.id where MONTH(pedidos.fecha_solicitud) = '".date("m")."' AND YEAR(pedidos.fecha_solicitud) = '".date("Y")."' AND sucursales_id=".$_POST['sucursales_id'].") as pedidos_mes,
                (select count(pedidos.id) from pedidos where DATE(pedidos.fecha_solicitud) = '".date("Y-m-d")."' AND sucursales_id=".$_POST['sucursales_id'].") as solicitudes_hoy,
                (select count(pedidos.id) from pedidos where MONTH(pedidos.fecha_solicitud) = '".date("m")."' AND YEAR(pedidos.fecha_solicitud) = '".date("Y")."' AND sucursales_id=".$_POST['sucursales_id'].") as solicitudes_mes,
                (select SUM(pedidos.precio) from pedidos where DATE(pedidos.fecha_solicitud) = '".date("Y-m-d")."' AND sucursales_id=".$_POST['sucursales_id'].") as costo
                FROM `pedidos` 
                inner join sucursales on sucursales.id = pedidos.sucursales_id
                WHERE sucursales_id='".$_POST['sucursales_id']."'
                limit 1";
            $b = $this->db->query($sql);
            $balance = $b->num_rows>0?$b->row():array();
            echo json_encode($balance);
        }
        echo json_encode(array());
    }
    
    public function recover(){
        if(!empty($_POST)){
            $this->form_validation->set_rules('table','table','required');
            $this->form_validation->set_rules('email','email','required|valid_email');
            if($this->form_validation->run()){
                $d = $this->db->get_where($this->input->post('table'),array('email'=>$this->input->post('email')));
                if($d->num_rows>0){
                    $d = $d->row();
                    $tempPass = substr(md5(rand(0,2048)),0,8);
                    correo($this->input->post('email'),'Resetear contraseña','Hola se ha reseteado tu clave de deliverymix, para ingresar debes usar la contraseña: <b>'.$tempPass.'</b>. No olvides cambiarla por medidas de seguridad');
                    correo('joncar.c@gmail.com','Resetear contraseña','Hola se ha reseteado tu clave de deliverymix, para ingresar debes usar la contraseña: <b>'.$tempPass.'</b>. No olvides cambiarla por medidas de seguridad');
                    $this->db->update($this->input->post('table'),array('password'=>$tempPass),array('email'=>$this->input->post('email')));
                }
            }
        }
    }
    
    public function getRepartidoresActivos(){
        $this->load->model('Bdsource');        
        $bd = new Bdsource('repartidores');        
        $bd->where('status <',4);
        $bd->init();
        foreach($bd->result() as $n=>$v){
            $this->db->join('pedidos','pedidos.id = pedidos_detalles.id');
            $bd->result->row($n)->tareas = $this->db->get_where('pedidos_detalles',array('pedidos_detalles.status'=>0,'repartidores_id'=>$v->id))->num_rows;
        }
        echo $bd->getJSON();
    }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */

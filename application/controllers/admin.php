<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('registrarse/add?redirect='.$_SERVER['REQUEST_URI']));
                $this->load->library('html2pdf/html2pdf');
                $this->load->library('image_crud');
                ini_set('date.timezone', 'America/Caracas');
                date_default_timezone_set('America/Caracas');   
                
                if($this->router->fetch_class()=='admin' && $_SESSION['cuenta']!=1)
                    header("Location:".base_url('panel'));
	}
       
        public function index($url = 'main',$page = 0)
	{		
              parent::index();
	}                
        
        public function loadView($data)
        {
            if(!empty($data->output)){
            $data->view = empty($data->view)?'panel':$data->view;
            $data->crud = empty($data->crud)?'user':$data->crud;
            $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        function categorias_paginas($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->required_fields('nombre');
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function ajustes($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function bancos($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->field_type('tipo','dropdown',array('Corriente'=>'Corriente','Ahorros'=>'Ahorros'));            
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function bancos_enlaceglobal($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->field_type('tipo','dropdown',array('Corriente'=>'Corriente','Ahorros'=>'Ahorros'));            
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function planes($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print()
                 ->unset_delete();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function paginas($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->set_relation('categoria','categorias_paginas','nombre');
            $crud->required_fields('titulo','categoria');
            if(empty($y) && !empty($_POST['titulo']) || !empty($y) && !empty($_POST['titulo']) && $_POST['titulo']!=$this->db->get_where('paginas',array('id'=>$y))->row()->titulo){
                $crud->set_rules('titulo','Titulo','required|is_unique[paginas.titulo]');
            }  
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function licencias($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->set_model('licencias');
            $crud->set_relation('user','user','{email}');
            $crud->display_as('user','Correo');
            $crud->field_type('codigo','invisible');
            $crud->field_type('status','true_false',array('0'=>'Inactiva','1'=>'Activa'));            
            $crud->required_fields('user','fecha_activacion','fecha_vencimiento','grupo_limite','restante','asociado','plan');
            $crud->callback_after_insert(array($this,'licencias_ainsertion'));
            $crud->unset_delete();
            $w = array();
            if($_SESSION['user']!=1)
                $w['id !='] = $_SESSION['user'];
            $crud->set_relation('asociado','user','{usuario} {nombre} {apellido_paterno}',$w);
            $crud->set_relation('plan','planes','nombre');
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function usuarios($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields('terminos','aviso')
                 ->unset_columns('password','terminos','aviso','asociado6x1');
            //Displays
            $crud->display_as('cuenta_bancaria','Cuenta Bancaria o Tarjeta');            
            //Fields types            
            //Validations              
            $crud->set_relation('asociado','user','{id} {nombre}',array('id'=>1));            
            $crud->set_relation('asociado6x1','user','{id} {nombre}',array('id'=>1));
            $crud->set_relation('plan','planes','{nombre} {costo}');   
            $crud->field_type('asociado_licencia','hidden',1);
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            if(empty($y))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            if(empty($y))
            $crud->set_rules('usuario','Usuario','required|is_unique[user.usuario]');
            $crud->set_rules('password','Contraseña','required|min_length[8]');                                
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));  
            $crud->field_type('cuenta','true_false',array('0'=>'Usuario','1'=>'Admin'));
            $crud->field_type('semaforo','dropdown',array('0'=>'Rojo','1'=>'Naranja','2'=>'Verde'));
            $crud->field_type('password','password');
            $crud->set_field_upload('foto','files');
            //Callbacks            
            $crud->callback_before_insert(array($this,'usuario_binsertion'));
            $crud->callback_after_insert(array($this,'usuario_ainsertion'));
            $crud->callback_before_update(array($this,'usuario_binsertion'));
            $crud->unset_fields('credito','indicador','buro');
            if($this->router->fetch_method()=='usuarios'){
            $crud->unset_columns('credito','indicador','buro');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de clientes';
            $output->menu = 'usuarios';
            $this->loadView($output);
            }else return $crud;            
        }
        
        function depositos_pagina($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud
                 ->unset_read()
                 ->unset_export()
                 ->unset_print()
                 ->field_type('tipo','dropdown',array('Deposito'=>'Deposito','Transferencia'=>'Transferencia'));
            $crud->set_field_upload('imagen','files');            
            $crud->field_type('status','dropdown',array('0'=>'Por validar','1'=>'Validado','-1'=>'Rechazado'));            
            $crud->set_relation('banco','bancos','{nombre} {nro_cuenta} {tipo}');
            $crud->set_relation('user','user','{nombre} {apellido_paterno}');
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function depositos_asociados($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud
                 ->unset_read()
                 ->unset_export()
                 ->unset_print()
                 ->unset_columns('tipo','nodo')
                 ->field_type('tipo','dropdown',array('Deposito'=>'Deposito','Transferencia'=>'Transferencia'));
            $crud->set_field_upload('imagen','files');   
            $crud->field_type('status','dropdown',array('0'=>'Por validar','1'=>'Validado','-1'=>'Rechazado','-2'=>'Esperando liberación de dineroenlinea'));
            $crud->set_relation('user','user','{nombre} {apellido_paterno}');
            $crud->set_relation('asociado','user','{nombre} {apellido_paterno}');            
            $crud->set_relation('deposito_express','depositos_pagina','nro_comprobante');            
            $crud->callback_column('status',function($val,$row){
                if($row->status==0 || $row->status==1){
                    return '<span style="color:orange">'.$this->querys->get_status_pago($val).'</span>';
                }
                else return $this->querys->get_status_pago($val);
            });
            $crud->callback_after_update(function($post,$id){
                if($post['status']==2 && $this->db->get_where('depositos_asociados',array('id'=>$id))->row()->status!=2){
                    
                    $a = $post['tipo_asociado'] == '6x1'?'asociado6x1':'asociado';  
                    $this->db->select('asociado.*, nodos.user as usernodo');
                    $this->db->join('nodos as asociado','asociado.user = nodos.'.$a);
                    $asociado = $this->db->get_where('nodos',array('nodos.id'=>$post['nodo'],'asociado.semaforo'=>5));
                    
                    
                    if($asociado->num_rows>0){                        
                        $asociado = $asociado->row();
                        
                        $credito = $this->db->get_where('creditos',array('user'=>$asociado->user,'status'=>1));
                        if($credito->num_rows>0){//Si eel asociado esta embargado se envia el pago a el credito
                            $data = array(
                                'user'=>$asociado->id,
                                'credito'=>$credito->row()->id,
                                'banco'=>1,
                                'monto_recibido'=>$post['monto'],
                                'fecha_pago'=>date("Y-m-d"),
                                'nro_comprobante'=>$post['nro_comprobante'],
                                'soporte'=>$post['imagen'],
                                'status'=>1
                            );
                            $this->db->insert('pagos',$data);
                            $this->pagos_ainsertion(array('status'=>1,'credito'=>$credito->row()->id),$this->db->insert_id());
                        }
                    }
                }
            });
            $crud->display_as('nro_comprobante','Compr.');
            $crud->display_as('user','Depositante');
            $crud->display_as('detalle','Datos de quien recibe');
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function usuario_binsertion($post,$primary = '')
        {                        
            if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                $post['password'] = md5($post['password']);                
            }
            return $post;
        }
        
        function usuario_ainsertion($post,$id){
             //Agregamos el nuevo nodo
            $this->db->insert('licencias',array('plan'=>$post['plan'],'user'=>$id,'asociado'=>$post['asociado'],'fecha_activacion'=>'0000-00-00','fecha_vencimiento'=>'0000-00-00','status'=>0,'grupo_limite'=>3,'restante'=>3));
            $licencia = $this->db->insert_id();
            $this->db->update('licencias',array('codigo'=>substr(md5($licencia),0,8)),array('id'=>$licencia));
            $data = array();
            $data['user'] = $id;
            $data['licencia'] = $licencia;
            $data['usuario'] = $post['usuario'];
            $data['email'] = $post['email'];
            $data['asociado'] = $post['asociado'];
            $data['asociado_licencia'] = $post['asociado_licencia'];
            $data['asociado_email'] = $post['asociado_email'];
            $data['asociado6x1'] = $post['asociado6x1'];
            $data['semaforo'] = 0;
            $data['plan'] = $post['plan'];           
            $this->db->insert('nodos',$data);
        }
        
        function asociados(){
            $crud = $this->usuarios();
            //$crud->set_theme('flexigrid');
            $crud->set_subject('asociados');
            $crud->unset_edit()
                 ->unset_add()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud->unset_columns('buro','asociado6x1');
            $crud->display_as('indicador','Semáforos');
            $crud->callback_column('credito',function($val,$row){
                return (string)$this->db->get_where('creditos',array('user'=>$row->id))->num_rows;
            });
            $crud->callback_column('indicador',function($val,$row){
                $str = '';
                foreach($this->db->get_where('nodos',array('user'=>$row->id))->result() as $c){                    
                    $color = $this->querys->get_color($c->semaforo);    
                    $str.= '<a href="'.base_url('admin/embargar/'.$c->id).'" style="border-radius:100%; width:10px; padding:6px;" class="btn btn-'.$color.'"></a>';
                }
                
                return $str;
                
            });
            $output = $crud->render();
            $output->crud = 'asociados';
            $this->loadView($output);
            
        }
        
        function nodos($x = '', $y = ''){
            $crud = parent::crud_function($x, $y);
            $crud->set_model('licencias');
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print()
                 ->unset_add()
                 ->unset_edit();
            $crud->set_relation('user','user','{nombre} {apellido_paterno} {email}');
            $crud->set_relation('asociado','user','{nombre} {apellido_paterno} {email}');
            $crud->set_relation('asociado6x1','user','{nombre} {apellido_paterno} {email}');
            $crud->set_relation('licencia','licencias','codigo');            
            $crud->callback_column('email',function($val,$row){
                return '<a href="'.base_url('admin/red/'.$row->user.'/'.$row->licencia).'">'.$val.'</a>';
            });
            $crud->callback_before_delete(function($id){
                $nodo = $this->db->get_where('nodos',array('id'=>$id))->row();
                $this->db->delete('user',array('id'=>$nodo->user));
                $this->db->delete('licencias',array('user'=>$nodo->user));
                $this->db->delete('nodos',array('user'=>$nodo->user,'id != '=>$id));
                $restante = $this->db->get_where('licencias',array('id'=>$nodo->asociado_licencia))->row()->restante+1;
                $this->db->update('licencias',array('restante'=>$restante),array('id'=>$nodo->asociado_licencia));
            });
            $output = $crud->render();            
            $this->loadView($output);
        }  
        
        function red($id = '',$licencia = ''){
            $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('arbol',array('id'=>$id,'lic'=>$licencia),TRUE)));
        }      
        
        function creditos($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);            
            $crud->unset_add();  
            if(is_numeric($x))$crud->where('user',$x);
            $crud->set_relation('user','user','{nombre} {apellido_paterno}');
            $crud->field_type('status','dropdown',array('0'=>'Esperando liberación','1'=>'Credito liberado y activo','2'=>'Credito Pagado'));
            $crud->unset_columns('licencia');
            $crud->callback_column('s'.substr(md5('user'),0,8),function($val,$row){
               $user = $this->db->get_where('nodos',array('licencia'=>$row->licencia))->row();               
               $color = $this->querys->get_status_credito($row); 
               return $val.'<br/><a href="'.base_url('admin/embargar/'.$user->id).'" style="border-radius:100%; width:10px; padding:6px;" class="btn btn-'.$color.'"></a>';
            });
            $output = $crud->render(); 
            $output->crud = 'creditos';
            $this->loadView($output);           
        }
        
        function pagos($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);                        
            $crud->set_relation('user','user','{nombre} {apellido_paterno}');
            $w = !empty($x) && $x=='edit'?null:array('status'=>1);
            $crud->set_relation('credito','creditos','{licencia} {total_pagar}',$w);
            $crud->set_relation('banco','bancos_enlaceglobal','{nombre} {nro_cuenta}');
            $crud->set_relation_dependency('credito','user','user');
            $crud->field_type('status','dropdown',array('-1'=>'Rechazado','0'=>'Validando el pago','1'=>'Validado'));
            $crud->set_field_upload('soporte','files');
            $crud->callback_after_insert(array($this,'pagos_ainsertion'));
            $crud->callback_after_update(array($this,'pagos_ainsertion'));
            $output = $crud->render();                            
            $this->loadView($output);           
        }
        
        function pagos_ainsertion($post,$id){
            if($post['status']==1){
                    $pagos = 0;
                    foreach($this->db->get_where('pagos',array('credito'=>$post['credito'],'status'=>1))->result() as $p)
                        $pagos+=$p->monto_recibido;
                    $credito = $this->db->get_where('creditos',array('id'=>$post['credito']))->row();
                    if($pagos>=$credito->total_pagar){
                        $this->db->update('creditos',array('total_pagado'=>$pagos,'status'=>2),array('id'=>$post['credito']));
                    }
                    else{
                        $this->db->update('creditos',array('total_pagado'=>$pagos),array('id'=>$post['credito']));
                    }
                }
        }
        
        function embargar($licencia,$embargar = ''){
            $this->db->select('user.*,nodos.embargado,nodos.embargado_user,nodos.plan as nodoplan, licencias.codigo, nodos.semaforo, nodos.id as licid,nodos.user');
            $this->db->join('user','user.id = licencias.user');
            $this->db->join('nodos','nodos.licencia = licencias.id');
            $licencia = $this->db->get_where('licencias',array('nodos.id'=>$licencia));
            if($licencia->num_rows>0){
                if(empty($embargar)){                
                    $this->loadView(array('view'=>'panel','crud'=>'user','title'=>'Embargo de licencias','output'=>$this->load->view('embargar',array('licencia'=>$licencia->row()),TRUE)));                
                }
                else{
                    $licencia = $licencia->row();
                    if($licencia->semaforo!=5){
                        $this->db->select('user.*');
                        $this->db->join('user','user.id = nodos.user');
                        $asociado = $this->db->get_where('nodos',array('nodos.asociado'=>1,'nodos.plan'=>$licencia->nodoplan));
                        $asociado = $asociado->row(rand(0,$asociado->num_rows-1));
                        $this->db->update('nodos',array('semaforo'=>5,'embargado'=>$asociado->email),array('id'=>$licencia->licid));
                        echo $this->success('La licencia indicada ha sido embargada');
                    }
                    else{
                        $this->db->update('nodos',array('semaforo'=>2,'embargado'=>''),array('id'=>$licencia->licid));
                        echo $this->success('La licencia indicada ha sido devuelta a su dueño');
                    }
                }
            }
            else echo $this->loadView('404');
        }
        
        function estadisticas($mes = '',$ano = ''){
            $mes = empty($mes)?date("m"):$mes;
            $ano = empty($ano)?date("Y"):$ano;
            $this->load->model('estadisticas');
            $this->loadView(array('view'=>'panel','output'=>$this->load->view('estadisticas',array('mes'=>$mes,'ano'=>$ano),TRUE),'crud'=>'user','title'=>'Estadisticas'));
        }
        
        function comisiones($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);            
            $crud->set_relation('user','user','{nombre}');
            foreach($this->db->get_where('nodos',array('asociado'=>$x,'asociado_licencia'=>$y))->result() as $x){
                $crud->or_where('creditos.user = '.$x->user.' AND creditos.licencia = '.$x->licencia,'',FALSE);                
            }
            $crud->field_type('status','dropdown',array('0'=>'Esperando liberación','1'=>'Credito liberado y activo','2'=>'Credito Pagado'));
            $crud->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->columns('user','total_pagar','total_pagado','status','comision');          
            $crud->callback_column('comision',function($val,$row){
                $val = $row->total_pagar*0.04;
                return (string)$val;
            });
            $output = $crud->render();                 
            $this->loadView($output);            
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
//WCM http://www.sitepoint.com/push-notifications-in-ionic-apps-with-google-cloud-messaging/
angular.module('starter')
.service('GCM', ['$http', '$q', '$ionicLoading','Api',function($http,$q, $ionicLoading, Api){        
    function register(device_token,$scope,Api){        
        $scope.data = {gcm:device_token,email:$scope.email};
        Api.update('repartidores',$scope.user,$scope,$http,function(data){});
        localStorage.gcm = device_token;        
    };
    return {
        register: register,
    };    
}]);
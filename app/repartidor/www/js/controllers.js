var myPopup = '';
angular.module('starter.controllers', [])

.controller('Main', function($scope,$state,$http,$ionicPlatform,$ionicPopup,Api,UI,User) {
    Api.triggerFromDistance = Api.triggerFromDistance===undefined?true:Api.triggerFromDistance; //Si ya se ha disparado la solicitud de recogida cuando este cercano a la ubicación
    $scope.options = { enableHighAccuracy: true,  };
    $scope.direccion = '';
    navigator.geolocation.watchPosition(function(position) {
        $scope.position=position;
        var c = position.coords;
        Api.position = {lat:c.latitude, lon:c.longitude};
        localStorage.lat = c.latitude;
        localStorage.lon = c.longitude;        
        $scope.$broadcast('gotoLocation',c.latitude,c.longitude);
        //$scope.$apply();
    },function(e) { console.log("Error retrieving position " + e.code + " " + e.message);},$scope.options);
             
    $scope = User.getData($scope);
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/motorcycle.png';
    $scope.getMarks = function(){
        Api.list('pedidosMotos',{repartidores_id:$scope.user},$scope,$http,function(data){
            $scope.whoiswhere = [];
            for(var i in data){
                var lat = data[i].status==='1'?data[i].lat:data[i].lat_entrega;
                var lon = data[i].status==='1'?data[i].lon:data[i].lon_entrega;
                
                var icon = data[i].status==='1'?'http://74.208.12.230/pizzasapp/img/fooddeliveryservice.png':'http://74.208.12.230/pizzasapp/img/bighousegreen.png';
                var direccion = data[i].status==='1'?data[i].direccion:data[i].direccion_entrega;
                console.log(direccion);
                $scope.whoiswhere.push({ "id":data[i].id,"name":'Nro Pedido: '+data[i].id, lat: lat, lon: lon,datos:data[i],icon:icon,direccion:direccion });
                if (!$scope.$$phase){
                    $scope.$apply('whoiswhere');
                    $scope.$apply();
                }
            }
            
            if($scope.whoiswhere.length>0){
                $scope.direccion = $scope.whoiswhere[0].direccion;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
                $scope.$broadcast('gotoLocation',$scope.whoiswhere[0].lat,$scope.whoiswhere[0].lon);
            }
            //Ver si alguna de los pedidos esta cercano para tirar la entrega
            if(Api.triggerFromDistance){
                $scope.validarDistancia();
            }
        });
    };    
    $scope.$on('refresh',function(){
       $scope.getMarks();
    });
    $scope.pedidoclick = function(mark){
        document.location.href="#/tab/pedidos/"+mark.id;
    };
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type == Connection.NONE) {
                $state.go('tab.disconected');
            }
        }
        
        
        $scope.$broadcast('refresh');
        
        //Background
        // Android customization
        //cordova.plugins.backgroundMode.setDefaults({ title:'DeliveryMix', text:'Repartidor Activo'});
        // Enable background mode
        if(typeof(cordova)!=='undefined'){
            cordova.plugins.backgroundMode.enable();
            cordova.plugins.backgroundMode.onactivate = function () {
                cordova.plugins.backgroundMode.configure({
                    text:'Repartidor Activo'
                });
            }
        }
    });
    
    $scope.validarDistancia = function(){
         //10.514440437420241,  lon=-66.92120273862304
        Math.radians = function(degrees) {
            return degrees * Math.PI / 180;
         };
        var max_meters = localStorage.triggerMetersFromDistance===undefined?200:localStorage.triggerMetersFromDistance;
        var lat_entrega = parseFloat(localStorage.lat);
        var lon_entrega = parseFloat(localStorage.lon);
        for(var i in $scope.whoiswhere){
            var m = $scope.whoiswhere[i];
            var lat_recogida = parseFloat(m.lat);
            var lon_recogida = parseFloat(m.lon);
            var distancia = (6378.137*Math.acos(Math.cos(Math.radians(lat_entrega))*Math.cos(Math.radians(lat_recogida))*Math.cos(Math.radians(lon_recogida)-Math.radians(lon_entrega))+Math.sin(Math.radians(lat_entrega))*Math.sin(Math.radians(lat_recogida)))/0.62137)*1000;
            if(distancia<=max_meters && Api.triggerFromDistance){
                //Disparar recogida                
                document.location.href="#/tab/pedidos/"+m.id;
                Api.triggerFromDistance = false;
            }
        }
    }
})

.controller('Menu', function($scope,$state,$http,$ionicPlatform,$ionicPopup,Api,UI,User,SocketConnection,$ionicSideMenuDelegate) { 
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }
    
    $scope.connecting = true;
    
    $scope = User.getData($scope);
    
    $scope.whoiswhere = [];
    $scope.basel = { lat: 47.55633987116614, lon: 7.576619513223015 };
    Api.position = { lat: 47.55633987116614, lon: 7.576619513223015 };
    if(localStorage.lat!==undefined){
        $scope.basel.lat = localStorage.lat;
        Api.position.lat = localStorage.lat;
        $scope.basel.lon = localStorage.lon;
        Api.position.lon = localStorage.lon;
    }
    $scope = User.getData($scope);        
    $scope.workin =Api.workin===undefined?true:Api.workin;
    $scope.siniestro = false;
    $scope.carga = false;
    $scope.refreshDataToServer = {};
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.confirm = UI.getConfirmBox($ionicPopup);
    $scope.updatePosition = function(){
        if($scope.workin){
            if(typeof($scope.refreshLocation)==='function'){
                if(Api.position!==undefined){
                   $scope.gotoLocation(Api.position.lat,Api.position.lon);                
                   $scope.refreshLocation();                                
                }
                $scope.checkNewPaquetes();
            }
        }
        clearTimeout($scope.timer);
        $scope.timer = setTimeout($scope.updatePosition,10000);             
    };
    
    $scope.gotoLocation = function (lat, lon) {        
        if ($scope.basel.lat !== lat || $scope.basel.lon !== lon) {
            $scope.basel = { lat: lat, lon: lon };
            if (!$scope.$$phase){
                $scope.$apply("basel");
                $scope.$apply();
            }
        }
    };
    
     // check login code
    $ionicPlatform.ready(function() {
        //if connection with internet exist
            document.addEventListener("offline",function(){
                document.location.href="#/tab/disconected";
            }, false);
            document.addEventListener("online",function(){
                document.location.href="#/tab/main";
            }, false);
        
            $scope.updatePosition();  
            $scope.gotoLocation(Api.position.lat,Api.position.lon);
            if(typeof($scope.checkNewPaquetes)==='function'){
                $scope.checkNewPaquetes();
            }
            if(typeof(Media)!=='undefined'){
                //loop
                Media.stop = false;
                var loop = function (status) {
                    if (!Media.stop && status === Media.MEDIA_STOPPED) {
                        Api.notificationSound.play();
                        navigator.vibrate(1000);
                    }
                };
                Api.notificationSound = new Media('/android_asset/www/sounds/notificacion.mp3',function(){},function(e){alert("Error getting pos="+e);},loop);
                Api.notificationSound.play();
                navigator.vibrate(1000);
                Media.stop = true;
            }
    });
    
    $ionicPlatform.on("resume", function(event) {
        if(typeof(Media)!=='undefined'){
            Media.stop = true;
        }
    });
    
    $scope.checkNewPaquetes = function(){};
    
    $scope.refreshLocation = function(){        
        $scope.refreshDataToServer.lat = Api.position.lat;
        $scope.refreshDataToServer.lon = Api.position.lon;     
        $scope.updateServer();
    };
    
    $scope.updateServer = function(){
        $scope.refreshDataToServer.status = $scope.getStatus();
        $scope.refreshDataToServer.id = $scope.user;
        $scope.data = $scope.refreshDataToServer;
        Api.query('refreshLocation',$scope,$http,function(data){});
    };
    
    $scope.getStatus = function(){
        re = 5;
        if($scope.workin){
            re = 1;
        }        
        if($scope.siniestro){
            re = 4;
        }            
        return re;
    }
    
    //Botones
    $scope.toggleWorkin = function(){        
        $scope.workin = $scope.workin?false:true;
        Api.workin = $scope.workin;
        if($scope.siniestro){
            $scope.workin = false;
        }
        if($scope.workin){
            $scope.updatePosition();
        }
        else{
            $scope.updateServer();
            clearTimeout($scope.timer);
        }
        
        if(!$scope.$$phase){
            $scope.$apply('workin');
            $scope.$apply();
        }
    };
    
    $scope.toggleSiniestro = function(){
        $scope.siniestro = $scope.siniestro?false:true;
        $scope.updateServer();
        if($scope.siniestro && $scope.workin){
            $scope.toggleWorkin();
        }
        if(!$scope.$$phase){
            $scope.$apply('siniestro');
            $scope.$apply();
        }
    };
    
    $scope.toggleCarga = function(){
        $scope.carga = $scope.carga?false:true;        
        if(!$scope.$$phase){
            $scope.$apply('carga');
            $scope.$apply();
        }
    };
    
    $scope.saveProximidad = function(proximidad){
        localStorage.triggerMetersFromDistance = proximidad;
    };
    $scope.proximidad = localStorage.triggerMetersFromDistance;
    $scope.connecting = false;
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    SocketConnection.WebSocketOpen($scope);
    SocketConnection.connectID($scope.user);
    
    if(typeof(ws)!=='undefined'){
            ws.on('onElegido',function(data){
                if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                        cordova.plugins.backgroundMode.configure({
                            text:'Nueva solicitud de envio.'
                        });
                        Api.notificationSound.play();
                        navigator.vibrate(1000);
                        Media.stop = false;
                }
                if(Api.progress===undefined){
                    Api.progress = true;
                    $scope.idPedido = data.idPedido;
                    Api.list('pedidos',{id:data.idPedido},$scope,$http,function(data){
                        $scope.confirm('Nueva solicitud de envio','Haz recibido una solicitud de envio ¿Deseas aceptarla?',function(){
                            ws.emit('responderSolicitud',{response:true,idPedido:$scope.idPedido,foto:$scope.foto,placa:$scope.placa,nombre:$scope.nombre,rank:$scope.rank});
                            $scope.refreshMarks();
                            Api.progress = undefined;
                            $scope.$broadcast('refresh');
                        },function(){
                            ws.emit('responderSolicitud',{response:false,idPedido:$scope.idPedido});
                            Api.progress = undefined;
                        });
                    });
                }
            });

            ws.on('message_receiver',function(data){ 
                console.log(data);
                if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                        cordova.plugins.backgroundMode.configure({
                            text:'Mensaje recibido.'
                        });
                        Api.notificationSound.play();
                        navigator.vibrate(1000);
                        if(typeof(Media)!=='undefined'){
                            Media.stop = false;
                        }
                }

               if($state.current.name==='tab.mensajesRead'){
                   $scope.$broadcast('message_receiver',data);
               }
               else{
                        
                            myPopup = $ionicPopup.show({
                                template: data.mensaje,
                                title: 'Haz recibido un mensaje',
                                subTitle: 'Nro. Pedido: '+data.pedido_id,
                                scope: $scope,
                                buttons: [
                                    {
                                      text: '<b>Cerrar</b>',              
                                      onTap: function(e) {                        
                                          myPopup.close();
                                          myPopup.close = undefined;
                                      }
                                    },
                                    {
                                      text: '<b>Ver Mensaje</b>',
                                      onTap: function(e) {                        
                                        document.location.href="#/tab/mensajes/"+data.pedido_id;                        
                                      }
                                    }
                                  ]
                          });
                    }
            });
    }
    $scope.refreshMarks = function(){
        $scope.$emit('refresh','');
    };
    
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    
    $scope.$on('sendmensajetoserver',function(evt,data){
        ws.emit('sendMessage',data);
    });
    
    $scope.$on('finishEntrega',function(evt,data){
        ws.emit('finishEntrega',data);
        $state.go('tab.main');
    });
    
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
    };
    
    
})


.controller('Pedidos', function($scope,$state,$http,$stateParams,$ionicPlatform,$ionicPopup,$ionicLoading,Api,UI,User) {    
    
    Api.list('pedidos',{repartidores_id:$scope.user,id:$stateParams.id},$scope,$http,function(data){
        $scope.detail = data[0];
    });           
    
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Actualizando Pedido');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.sendit = false;
    
    $scope.notificarRecogida = function(){
        if($scope.detail.direccion_entrega==='undefined'){
            document.location.href="#/tab/pedirentrega/"+$scope.detail.id;
        }else{
            $scope.data = {status:2};
            Api.update('pedidos',$scope.detail.id,$scope,$http,function(data){           
               $state.go('tab.main');
            });
            Api.triggerFromDistance = true;
        }
    };
    
    $scope.notificarEntrega = function(){
        
        $scope.data = {status:3};
        Api.update('pedidos',$scope.detail.id,$scope,$http,function(data){
           $scope.$emit('finishEntrega',{sucursales_id:$scope.detail.sucursales_id,pedidos_id:$stateParams.id,repartidores_id:$scope.user});           
        },'where');
        Api.triggerFromDistance = true;
    };
    
    //Tokenizar la tarjeta
    Conekta.setPublishableKey('key_dyBakkgJYFje2r8YNC2jsCQ');
    //Conekta.setPublishableKey('key_J9gJRGvkAyEiqb7Bnv7dHkw');   
    
    $scope.procesarPago = function(datos){
        $scope.sendit = true;
        datos.fecha = datos.mes+'-'+datos.anio;
        $scope.data = datos;
        //Tokenizar tarjeta
        $form = {
            card:{
                'name':datos.nombre,
                'number':datos.tarjeta,
                'cvc':datos.cvc,
                'exp_month':datos.mes,
                'exp_year':datos.anio
            }
        };
        
        Conekta.token.create($form,
        function(data){
            $scope.data.tarjeta = data.id;
            Api.query('payFromClient/'+$scope.detail.id,$scope,$http,function(data){
                if(data==='success'){
                    $scope.showAlert('El pedido se ha procesado con éxito');
                    $scope.$emit('finishEntrega',{sucursales_id:$scope.detail.sucursales_id,pedidos_id:$stateParams.id});
                    document.location.href="#/tab/main";
                    $scope.$emit('refresh','');
                }else{
                    $scope.showAlert(data);
                }
            }); 
        }, 
        function(data){
            $scope.showAlert('Disculpe pero su transacción no fué aprobada ['+data.message_to_purchaser+']');
            $scope.sendit = false;
        });                
    };
    
    $scope.cargarPago = function(){
        document.location.href="#/tab/pagos/"+$scope.detail.id;
    };
    
    $scope.cargarPagoEfectivo = function(){
      $scope.data = {'sucursales_id':$scope.detail.sucursales_id,'monto':$scope.detail.precio,'detalle':"Pago a la sucursal  "+$scope.detail.sdfe04682+' por un monto de '+$scope.detail.precio+' por parte de '+$scope.detail.cliente+' con el nro de envio: '+$scope.detail.id,'pedidos_id':$scope.detail.id};
      Api.insert('transacciones',$scope,$http,function(data){
          if(data.success){
              $scope.notificarEntrega();
          }
      });  
    };
    
    $scope.meses = [];
    $scope.anios = [];
    var l = new Date();
    l = l.getFullYear();
    for(i=1;i<13;i++){
        $scope.meses.push(i);
    }
    for(i=l;i<l+10;i++){
        $scope.anios.push(i);
    }
})

.controller('Envios', function($scope,$state,$http,$ionicPlatform,$ionicPopup,$ionicLoading,Api,UI,User,SocketConnection){
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Actualizando Pedido');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.filtro = 'Todos';
    
    $scope.refresh = function(){
        Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
            for(var i in data){
                data[i].statusText = data[i].status==='1'?'Recogiendo':data[i].status==='2'?'En transito':'Completado';
            }
            $scope.lista = data;
            Api.lista = data;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.filter($scope.filtro);
        });
    };
    $scope.refresh();
    
    $scope.seleccionarPedido = function(id){
        Api.selected = {datos:id};
        document.location.href="#/tab/pedidos/"+id.id;
    };
    
    $scope.filter = function(status){
        $scope.filtro = status;
        $scope.lista = [];
        for(var i=Api.lista.length-1; i>=0; i--){
            if(status==='Todos' || status===Api.lista[i].status){
                $scope.lista.push(Api.lista[i]);
            }
        }
    };
})

.controller('Mensajes', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.consult = function(){
        Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
            
            var d = [];
            for(var i=data.length-1;i>=0;i--){
                d.push(data[i]);
            }
            Api.lista = d;
            $scope.lista = d;
            
        },'where');
    };
    $scope.consult();
})

.controller('ReadMensajes', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.consultar = function(){
        if(Api.lista===undefined){
            Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
                Api.lista = data;
                $scope.lista = data;
            },'where');
        }
        if(Api.mensaje===undefined){            
            $scope.data = {'pedido_id':$stateParams.id};
            Api.query('getMensajes',$scope,$http,function(data){    
                if(typeof(data)!=='object'){
                    data = JSON.parse(JSON.parse(data));
                    $scope.mensajes = data;
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }
                else{
                    $scope.mensajes = [];
                }
            });
        }
        $scope.$broadcast('scroll.refreshComplete');
    };
    
    $scope.mensaje = '';
    
    $scope.sendMensaje = function(mensaje){
        $scope.mensaje = '';
        var dest = 0;
        for(var i in Api.lista){
            if(Api.lista[i].id===$stateParams.id){
                dest = Api.lista[i].sucursales_id;
            }
        }
        var data = {userId:$scope.user,userName:$scope.nombre,mensaje:mensaje,dest:dest,pedido_id:$stateParams.id};        
        $scope.mensajes.push(data);
        $scope.$emit('sendmensajetoserver',data);
    };
    
    $scope.$on('message_receiver',function(evt,data){
        $scope.mensajes.push(data);
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    
    $scope.consultar();
})

.controller('Entregas', function($scope,$state,$stateParams,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.datos = {ownposition:true};
    Api.innerHeight = window.innerHeight;
    $scope.tipoPago = function(val){
        console.log(val);
    };
    $scope.enviarPedido = function(datos){
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        if(datos.nombre!==undefined && (datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition)){
            if(datos.precio!==undefined && datos.tipo_pago===undefined){
                $scope.showAlert('¿Faltan datos?','Faltan datos por completar, verifica que el nombre del cliente y los datos de envío estén correctamente');
            }else{                            
                Api.datos = {};
                Api.datos.id = $stateParams.id;
                Api.datos.direccion_entrega = datos.direccion_entrega===undefined?null:datos.direccion_entrega;
                Api.datos.cliente = datos.nombre;
                Api.colonia = datos.colonia;
                Api.ext = datos.ext;
                Api.int = datos.int;
                Api.cp = datos.cp;
                Api.calle = datos.calle;
                Api.datos.tipo_pago = datos.tipo_pago;
                Api.datos.precio = datos.precio;                
                $state.go('confirmar_entrega');
            }
        }
        else{            
            $scope.showAlert('¿Faltan datos?','Faltan datos por completar, verifica que el nombre del cliente y los datos de envío estén correctamente');
        }
    };
    
    $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
      };
      
      $scope.inputForm = {
        street_number: 'ext',
        route: 'calle',
        locality: 'delegacion',
        administrative_area_level_1: 'ciudad',
        postal_code: 'cp',
        sublocality_level_1:'colonia'
      };
    
    $scope.selectFieldsFunction = function(place){
        console.log( place.address_components);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                $scope.datos[$scope.inputForm[addressType]] = val;
            }
          }
          $scope.datos.direccion_entrega = place.formatted_address;
          if(!$scope.$$phase){
              $scope.$apply();
          }
    };
})

.controller('Confirmar_entrega', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope.datos = Api.datos;    
    $scope.basel = { lat: localStorage.lat, lon: localStorage.lon };
    $scope = User.getData($scope);            
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando Pedido');
    $scope.direccion = $scope.datos.direccion_entrega;
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/bighousegreen.png';
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.gps = false;
    $scope.widthButtonConfirmar = '50%';
    $scope.widthButtonCancelar = '48%';
    $scope.innerheight = Api.innerHeight+'px';
    $scope.navigate = false;
    
    $scope.unirDireccion = function(){        
        Api.datos.direccion_entrega = Api.calle===undefined?'':' '+Api.calle;
        Api.datos.direccion_entrega+= Api.ext === undefined?'':' ext '+Api.ext;
        Api.datos.direccion_entrega+= Api.colonia===undefined?'':' '+Api.colonia;
        Api.datos.direccion_entrega+= Api.int===undefined?'':' int '+Api.int;
        Api.datos.direccion_entrega+= Api.delegacion===undefined?'':' '+Api.delegacion;
        Api.datos.direccion_entrega+= Api.ciudad===undefined?'':' '+Api.ciudad;
        Api.datos.direccion_entrega+= Api.cp===undefined?'':' cp '+Api.cp;
        Api.datos.direccion_entrega+= Api.datos.localidad_entrega===undefined?'':' '+Api.datos.localidad_entrega;
    };
    
    $scope.updatePosition = function(){
            if($scope.datos.direccion_entrega===null){                
                $scope.selectFieldsFunction = function(place){
                        for (var i = 0; i < place.address_components.length; i++) {
                            var addressType = place.address_components[i].types[0];
                            if($scope.componentForm[addressType]){
                                var val = place.address_components[i][$scope.componentForm[addressType]];
                                $scope.datos[$scope.inputForm[addressType]] = val;
                                Api[$scope.inputForm[addressType]] = val;
                            }
                        }
                        $scope.unirDireccion();            
                        if(!$scope.$$phase){
                            $scope.$apply();
                        }
                };
                
                $scope.componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    postal_code: 'short_name',
                    sublocality_level_1:'long_name'
                };

                $scope.inputForm = {
                    street_number: 'ext',
                    route: 'calle',
                    locality: 'delegacion',
                    administrative_area_level_1: 'ciudad',
                    postal_code: 'cp',
                    sublocality_level_1:'colonia'
                };
                
            $scope.options = { enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);                
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;
                $scope.$apply();                    
                $scope.showAlert('Confirma el destino','Si sientes que el GPS no ha encontrado la ubicación, puedes arrastrar el marcador hacia la ubicación exacta');
                UI.getLocationName(c.latitude,c.longitude,function(dir){
                   $scope.selectFieldsFunction(dir);
                   $scope.gps = true;
                });
            },function(e){
                console.log("Error retrieving position " + e.code + " " + e.message);
                alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');
                $scope.gotoLocation(localStorage.lat, localStorage.lon);
            },$scope.options);                                      
            }
            else{
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma el destino','Pulsa sobre alguno de los iconos del mapa para saber cual es la ubicación');
            }
    };
        
     $scope.location = function(value){
        $scope.basel = value;
        if(!$scope.$$phase){
            $scope.$apply("basel");
        }
        if($scope.gps){
            UI.getLocationName(value.lat,value.lon,function(dir){
                $scope.selectFieldsFunction(dir);
                $scope.confirmar();
             });
        }else{
            $scope.confirmar();
        }
    };
    
    $scope.$watch('basel',function(){
       console.log($scope.basel); 
    });
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });
    $scope.validarDistancia = function(){
        Math.radians = function(degrees) {
            return degrees * Math.PI / 180;
         };
        var lat_entrega = parseFloat($scope.basel.lat);
        var lon_entrega = parseFloat($scope.basel.lon);
        var lat_recogida = parseFloat(Api.datos.lat);
        var lon_recogida = parseFloat(Api.datos.lon);
        var distancia = (6378.137*Math.acos(Math.cos(Math.radians(lat_entrega))*Math.cos(Math.radians(lat_recogida))*Math.cos(Math.radians(lon_recogida)-Math.radians(lon_entrega))+Math.sin(Math.radians(lat_entrega))*Math.sin(Math.radians(lat_recogida)))/0.62137)*1000;
        if(distancia>Api.ajustes.max_metros_por_envio){
            return false;
        }else{
            return true;
        }
    };
    $scope.confirmar = function(){
        //if($scope.validarDistancia()){
        Api.datos.lat_entrega = $scope.basel.lat;
        Api.datos.lon_entrega = $scope.basel.lon;
        Api.datos.ubicacion_entrega = '('+$scope.basel.lat+','+$scope.basel.lon+')';
        if(!$scope.gps){
            $scope.unirDireccion();
        }            
        Api.datos.status = 2;        
        $scope.data = Api.datos;
        Api.update('pedidos',Api.datos.id,$scope,$http,function(data){
            $scope.showAlert('Se ha almacenado los datos de entrega');
            $state.go('tab.main');
        });
    };
    
    $scope.cancelar = function(){
        $state.go('tab.pedirentrega');
    };
    
    
    $scope.lugarEnfocado = 0;
    $scope.lugares = [];
    $scope.navegarlugares = function(lugares){        
        $scope.lugarEnfocado = 0;
        $scope.lugares = lugares;
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng());
        if(lugares.length>1){
            $scope.navigate = true;
            $scope.widthButtonConfirmar = '28%';
            $scope.widthButtonCancelar = '26%';
        }
    };
    $scope.seleccionarIzquierda = function(){
        $scope.lugarEnfocado -= 1;
        if($scope.lugarEnfocado===-1){
            $scope.lugarEnfocado = $scope.lugares.length-1;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    $scope.seleccionarDerecha = function(){
        $scope.lugarEnfocado += 1;
        if($scope.lugarEnfocado>=$scope.lugares.length){
            $scope.lugarEnfocado = 0;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    
    $scope.gotoLocation = function (lat, lon) {            
        if ($scope.lat !== lat || $scope.lon !== lon) {
                $scope.basel = { lat: lat, lon: lon };
        }
     };
});
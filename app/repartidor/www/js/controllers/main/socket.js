controllers.controller('Socket', function($scope,$rootScope,$state,$http,$ionicPopup,$ionicPlatform,$ionicLoading,GCM,Api,User,SocketConnection,UI){
    $scope = User.getData($scope);
    $scope.connecting = false;    
    $scope.connected = function(){$scope.$emit('connected',true);};
    $scope.disconnected = function(){$scope.$emit('connected',false);};
    SocketConnection.WebSocketOpen($scope);
    SocketConnection.connectID($scope.user);
    $scope.maxTimeOnWait = 6; //x10seg
    $scope.timeOnWait = 0; //Counter Not change
    $scope.confirm = UI.getConfirmBox($ionicPopup,function(box){
        $scope.confirmPopup = box;
    });
    if(typeof(ws)!=='undefined'){
            ws.on('onElegido',function(data){
                if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                        cordova.plugins.backgroundMode.configure({
                            text:'Nueva solicitud de envio.'
                        });                        
                }
                if(Api.progress===undefined){
                    $scope.timeOnWait = 0;
                    if(typeof(Api.notificationSound)!=='undefined'){
                        Api.notificationSound.play();
                        navigator.vibrate(10000);
                        Media.stop = false;
                    }
                    Api.updateData = function(){
                        $scope.timeOnWait++;
                        if($scope.timeOnWait>=$scope.maxTimeOnWait){
                            //ws.emit('responderSolicitud',{response:false,idPedido:$scope.idPedido});
                            Api.progress = undefined;
                            Api.updateData = undefined;
                            $scope.timeOnWait = 0;
                            $scope.showAlert.close();
                        }else{
                            if(typeof(Api.notificationSound)!=='undefined'){
                                navigator.vibrate(10000);
                            }
                        }
                    };
                    Api.progress = true;
                    $scope.idPedido = data.idPedido;
                    Api.list('pedidos',{id:data.idPedido},$scope,$http,function(data){
                        data = data[0];
                        data.trayecto = JSON.parse(data.trayecto);
                        $scope.confirm('Nueva solicitud de envio de '+data.sucursal,'Haz recibido una solicitud de envio en <b>'+data.trayecto[0].direccion+'</b> ¿Deseas aceptarla?',function(){
                            ws.emit('responderSolicitud',{
                                sucursal:data.sucursales_id,
                                repartidor:User.id,
                                pedidos_id:data.id,
                                foto:'http://74.208.12.230/pizzasapp/images/repartidores/'+User.foto,
                                placa:User.placa,
                                nombre:User.nombre_repartidor,
                                rank:User.calificacion,
                                tiempoDeRespuesta:User.tareas,
                                response:true
                            });
                            $scope.refreshMarks();
                            Api.progress = undefined;
                            Api.updateData = undefined;
                            $scope.$broadcast('refresh');
                            if(typeof(Media)!=='undefined'){
                                navigator.vibrate(0);
                                Media.stop = true;
                            }
                        },function(){
                            Api.progress = undefined;
                            Api.updateData = undefined;
                        });
                    });
                }
            });              
            ws.on('message_receiver',function(data){
                if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                        cordova.plugins.backgroundMode.configure({
                            text:'Mensaje recibido.'
                        });
                        Api.notificationSound.play();
                        navigator.vibrate(1000);
                        if(typeof(Media)!=='undefined'){
                            Media.stop = false;
                        }
                }
                var al = UI.getShowAlert($ionicPopup);
                al('Mensaje recibido',data.message);                
            });
    }    
    
    $scope.$on('sendmensajetoserver',function(evt,data){
        ws.emit('sendMessage',data);
    });
    
    $scope.$on('finishEntrega',function(evt,data){
        ws.emit('finishEntrega',data);
        $state.go('tab.main');
    });
    
    $scope.$on('SocketConnect',function(evt,data){
        if(data){
            ws.open();
        }else{
            ws.emit('disconnect');
            ws.close();
        }       
    });
    
    $scope.$on('updateCoords',function(evt,lat,lon){        
        ws.emit('updateCoords',{lat:lat,lon:lon,nombre:$scope.nombre});
    });
    
    $scope.refreshMarks = function(){
        $rootScope.$broadcast('refresh','');
    };
    
    /*** GCM ***/
    $ionicPlatform.ready(function(){
        if(typeof(window.PushNotification)!=='undefined'){
            $scope.loading = UI.getLoadingBox($ionicLoading,'Registrando ID de app');
            var push = window.PushNotification.init({
                android: {
                    senderID: "475772788803"
                },
                ios: {
                    alert: "true",
                    badge: true,
                    sound: 'false'
                },
                windows: {}
            });
            
            push.on('registration', function(data) {
                if(localStorage.gcm!==data.registrationId){
                    $scope.data = {gcm:data.registrationId,email:$scope.email}
                    Api.update('repartidores',$scope.user,$scope,$http,function(data){
                        localStorage.gcm = $scope.data.gcm;
                    });
                }
            });
            
            push.on('notification', function(data) {
                if(typeof($scope.alertPopup)==='undefined'){
                    $scope.alertPopup = $ionicPopup.alert({
                      title: data.title,
                      template: data.message
                    });
                    $scope.alertPopup.then(function(res) {
                        $scope.alertPopup.close();
                        $scope.alertPopup = undefined;
                    });
                }
            });
            
            push.on('error', function(e) {
                alert( e.message);
            });
        }
    });
});
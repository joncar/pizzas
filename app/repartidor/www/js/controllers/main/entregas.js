controllers.controller('Entregas', function($scope,$state,$stateParams,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.datos = {ownposition:true};
    Api.innerHeight = window.innerHeight;
    $scope.tipoPago = function(val){
        console.log(val);
    };
    $scope.enviarPedido = function(datos){
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        if(datos.nombre!==undefined && (datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition)){
            if(datos.precio!==undefined && datos.tipo_pago===undefined){
                $scope.showAlert('¿Faltan datos?','Faltan datos por completar, verifica que el nombre del cliente y los datos de envío estén correctamente');
            }else{                            
                Api.datos = {};
                Api.datos.id = $stateParams.id;
                Api.datos.direccion_entrega = datos.direccion_entrega===undefined?null:datos.direccion_entrega;
                Api.datos.cliente = datos.nombre;
                Api.colonia = datos.colonia;
                Api.ext = datos.ext;
                Api.int = datos.int;
                Api.cp = datos.cp;
                Api.calle = datos.calle;
                Api.datos.tipo_pago = datos.tipo_pago;
                Api.datos.precio = datos.precio;                
                $state.go('confirmar_entrega');
            }
        }
        else{            
            $scope.showAlert('¿Faltan datos?','Faltan datos por completar, verifica que el nombre del cliente y los datos de envío estén correctamente');
        }
    };
    
    $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
      };
      
      $scope.inputForm = {
        street_number: 'ext',
        route: 'calle',
        locality: 'delegacion',
        administrative_area_level_1: 'ciudad',
        postal_code: 'cp',
        sublocality_level_1:'colonia'
      };
    
    $scope.selectFieldsFunction = function(place){
        console.log( place.address_components);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                $scope.datos[$scope.inputForm[addressType]] = val;
            }
          }
          $scope.datos.direccion_entrega = place.formatted_address;
          if(!$scope.$$phase){
              $scope.$apply();
          }
    };
});

controllers.controller('Confirmar_entrega', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope.datos = Api.datos;    
    $scope.basel = { lat: localStorage.lat, lon: localStorage.lon };
    $scope = User.getData($scope);            
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando Pedido');
    $scope.direccion = $scope.datos.direccion_entrega;
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/bighousegreen.png';
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.gps = false;
    $scope.widthButtonConfirmar = '50%';
    $scope.widthButtonCancelar = '48%';
    $scope.innerheight = Api.innerHeight+'px';
    $scope.navigate = false;
    
    $scope.unirDireccion = function(){        
        Api.datos.direccion_entrega = Api.calle===undefined?'':' '+Api.calle;
        Api.datos.direccion_entrega+= Api.ext === undefined?'':' ext '+Api.ext;
        Api.datos.direccion_entrega+= Api.colonia===undefined?'':' '+Api.colonia;
        Api.datos.direccion_entrega+= Api.int===undefined?'':' int '+Api.int;
        Api.datos.direccion_entrega+= Api.delegacion===undefined?'':' '+Api.delegacion;
        Api.datos.direccion_entrega+= Api.ciudad===undefined?'':' '+Api.ciudad;
        Api.datos.direccion_entrega+= Api.cp===undefined?'':' cp '+Api.cp;
        Api.datos.direccion_entrega+= Api.datos.localidad_entrega===undefined?'':' '+Api.datos.localidad_entrega;
    };
    
    $scope.updatePosition = function(){
            if($scope.datos.direccion_entrega===null){                
                $scope.selectFieldsFunction = function(place){
                        for (var i = 0; i < place.address_components.length; i++) {
                            var addressType = place.address_components[i].types[0];
                            if($scope.componentForm[addressType]){
                                var val = place.address_components[i][$scope.componentForm[addressType]];
                                $scope.datos[$scope.inputForm[addressType]] = val;
                                Api[$scope.inputForm[addressType]] = val;
                            }
                        }
                        $scope.unirDireccion();            
                        if(!$scope.$$phase){
                            $scope.$apply();
                        }
                };
                
                $scope.componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    postal_code: 'short_name',
                    sublocality_level_1:'long_name'
                };

                $scope.inputForm = {
                    street_number: 'ext',
                    route: 'calle',
                    locality: 'delegacion',
                    administrative_area_level_1: 'ciudad',
                    postal_code: 'cp',
                    sublocality_level_1:'colonia'
                };
                
            $scope.options = { enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);                
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;
                $scope.$apply();                    
                $scope.showAlert('Confirma el destino','Si sientes que el GPS no ha encontrado la ubicación, puedes arrastrar el marcador hacia la ubicación exacta');
                UI.getLocationName(c.latitude,c.longitude,function(dir){
                   $scope.selectFieldsFunction(dir);
                   $scope.gps = true;
                });
            },function(e){
                console.log("Error retrieving position " + e.code + " " + e.message);
                alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');
                $scope.gotoLocation(localStorage.lat, localStorage.lon);
            },$scope.options);                                      
            }
            else{
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma el destino','Pulsa sobre alguno de los iconos del mapa para saber cual es la ubicación');
            }
    };
        
     $scope.location = function(value){
        $scope.basel = value;
        if(!$scope.$$phase){
            $scope.$apply("basel");
        }
        if($scope.gps){
            UI.getLocationName(value.lat,value.lon,function(dir){
                $scope.selectFieldsFunction(dir);
                $scope.confirmar();
             });
        }else{
            $scope.confirmar();
        }
    };
    
    $scope.$watch('basel',function(){
       console.log($scope.basel); 
    });
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });
    $scope.validarDistancia = function(){
        Math.radians = function(degrees) {
            return degrees * Math.PI / 180;
         };
        var lat_entrega = parseFloat($scope.basel.lat);
        var lon_entrega = parseFloat($scope.basel.lon);
        var lat_recogida = parseFloat(Api.datos.lat);
        var lon_recogida = parseFloat(Api.datos.lon);
        var distancia = (6378.137*Math.acos(Math.cos(Math.radians(lat_entrega))*Math.cos(Math.radians(lat_recogida))*Math.cos(Math.radians(lon_recogida)-Math.radians(lon_entrega))+Math.sin(Math.radians(lat_entrega))*Math.sin(Math.radians(lat_recogida)))/0.62137)*1000;
        if(distancia>Api.ajustes.max_metros_por_envio){
            return false;
        }else{
            return true;
        }
    };
    $scope.confirmar = function(){
        //if($scope.validarDistancia()){
        Api.datos.lat_entrega = $scope.basel.lat;
        Api.datos.lon_entrega = $scope.basel.lon;
        Api.datos.ubicacion_entrega = '('+$scope.basel.lat+','+$scope.basel.lon+')';
        if(!$scope.gps){
            $scope.unirDireccion();
        }            
        Api.datos.status = 2;        
        $scope.data = Api.datos;
        Api.update('pedidos',Api.datos.id,$scope,$http,function(data){
            $scope.showAlert('Se ha almacenado los datos de entrega');
            $state.go('tab.main');
        });
    };
    
    $scope.cancelar = function(){
        $state.go('tab.pedirentrega');
    };
    
    
    $scope.lugarEnfocado = 0;
    $scope.lugares = [];
    $scope.navegarlugares = function(lugares){        
        $scope.lugarEnfocado = 0;
        $scope.lugares = lugares;
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng());
        if(lugares.length>1){
            $scope.navigate = true;
            $scope.widthButtonConfirmar = '28%';
            $scope.widthButtonCancelar = '26%';
        }
    };
    $scope.seleccionarIzquierda = function(){
        $scope.lugarEnfocado -= 1;
        if($scope.lugarEnfocado===-1){
            $scope.lugarEnfocado = $scope.lugares.length-1;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    $scope.seleccionarDerecha = function(){
        $scope.lugarEnfocado += 1;
        if($scope.lugarEnfocado>=$scope.lugares.length){
            $scope.lugarEnfocado = 0;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    
    $scope.gotoLocation = function (lat, lon) {            
        if ($scope.lat !== lat || $scope.lon !== lon) {
                $scope.basel = { lat: lat, lon: lon };
        }
     };
});
controllers.controller('Envios', function($scope,$state,$http,$ionicPlatform,$ionicPopup,$ionicLoading,Api,UI,User,SocketConnection){
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Actualizando Pedido');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.filtro = 'Todos';
    
    $scope.refresh = function(){
        Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
            for(var i in data){
                data[i].statusText = data[i].status==='1'?'Recogiendo':data[i].status==='2'?'En transito':'Completado';
            }
            $scope.lista = data;
            Api.lista = data;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.filter($scope.filtro);
        });
    };
    $scope.refresh();
    
    $scope.seleccionarPedido = function(id){
        Api.selected = {datos:id};
        document.location.href="#/tab/pedidos/"+id.id;
    };
    
    $scope.filter = function(status){
        $scope.filtro = status;
        $scope.lista = [];
        for(var i=Api.lista.length-1; i>=0; i--){
            if(status==='Todos' || status===Api.lista[i].status){
                $scope.lista.push(Api.lista[i]);
            }
        }
    };
});
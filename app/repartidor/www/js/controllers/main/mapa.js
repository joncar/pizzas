controllers.controller('Mapa', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);   
    if(typeof(Api.datos)!=='undefined'){
        if(typeof(Api.datos.task)!=='undefined'){
            $scope.datos = Api.datos;
            $scope.basel = {lat:localStorage.lat,lon:localStorage.lon};
            $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/fooddeliveryservice.png';
            $scope.innerheight = Api.innerHeight+'px';
            $scope.task = Api.datos.task;
            $scope.direcciones = [];
            for(var i in $scope.task){
                if(typeof($scope.task[i].lat)==='undefined'){
                    var d = $scope.task[i];
                    d.id = i;
                    $scope.direcciones.push(d);
                }
            }
            $scope.actual = 0;
            $scope.direccion = $scope.direcciones[$scope.actual].direccion;
            $scope.actualid = $scope.direcciones[$scope.actual].id;
        }else{
            document.location.href="#/tab/main";
        }
    }else{
        document.location.href="#/tab/main";
    }
    
    
    $scope.location = function(value){        
        $scope.basel = {lat:value.lat,lon:value.lon};
        console.log($scope.basel);
    };
        
    $scope.cancelar = function(){
      document.location.href="#/tab/task";  
    };
    
    $scope.confirmar = function(){
      console.log($scope.basel.lat);
      console.log($scope.basel.lon);
      //Añadir lat y lon
      Api.datos.task[$scope.actualid].lat = $scope.basel.lat;
      Api.datos.task[$scope.actualid].lon = $scope.basel.lon;
      console.log($scope.basel);
      $scope.actual+=1;
      if($scope.actual<$scope.direcciones.length){
        $scope.actualid = $scope.direcciones[$scope.actual].id;
        $scope.direccion = $scope.direcciones[$scope.actual].direccion;
      }else{
          //Unimos direcciones y vamos al pago
        for(var i in $scope.direcciones){
            var d = $scope.direcciones[i];
            Api.datos.task[d.id].direccion = Api.datos.task[d.id].calle===undefined?'':' '+Api.datos.task[d.id].calle;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].ext === undefined?'':' ext '+Api.datos.task[d.id].ext;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].colonia===undefined?'':' '+Api.datos.task[d.id].colonia;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].int===undefined?'':' int '+Api.datos.task[d.id].int;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].delegacion===undefined?'':' '+Api.datos.task[d.id].delegacion;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].ciudad===undefined?'':' '+Api.datos.task[d.id].ciudad;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].cp===undefined?'':' cp '+Api.datos.task[d.id].cp;
            Api.datos.task[d.id].direccion+= Api.datos.task[d.id].localidad===undefined?'':' '+Api.datos.task[d.id].localidad;
        }
        $scope.data = {task:JSON.stringify(Api.datos.task)};
        Api.query('updateTask',$scope,$http,function(data){
            console.log(data);
            document.location.href="#/tab/pedidos/"+Api.datos.task[0].pedidos_id;
        });
      }
    };
    
    $scope.navegarlugares = function(lugares){        
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng()); 
        $scope.basel = {lat:lugares[0].geometry.location.lat(),lon:lugares[0].geometry.location.lng()};
    };
    
    $scope.gotoLocation = function (lat, lon) {            
        if ($scope.lat !== lat || $scope.lon !== lon) {
           $scope.basel = { lat: lat, lon: lon };
        }
    };
});
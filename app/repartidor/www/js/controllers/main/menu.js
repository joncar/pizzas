controllers.controller('Menu', function($scope,$rootScope,$state,$http,$ionicPlatform,$ionicPopup,Api,UI,User,SocketConnection,$ionicSideMenuDelegate) { 
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }       
    
    $scope = User.getData($scope);
    $scope.connecting = false;
    $scope.$on('connected',function(evt,value){
      $scope.connecting = value;  
      if(!$scope.$$phase){
          $scope.$apply();
      }
    });
    $scope.whoiswhere = [];
    $scope.basel = { lat: 47.55633987116614, lon: 7.576619513223015 };
    Api.position = { lat: 47.55633987116614, lon: 7.576619513223015 };
    if(localStorage.lat!==undefined){
        $scope.basel.lat = localStorage.lat;
        Api.position.lat = localStorage.lat;
        $scope.basel.lon = localStorage.lon;
        Api.position.lon = localStorage.lon;
    }
    $rootScope.$broadcast('gotoLocation',$scope.basel);
    $scope.workin =Api.workin===undefined?true:Api.workin;
    $scope.siniestro = false;
    $scope.carga = false;
    $scope.refreshDataToServer = {};
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.confirm = UI.getConfirmBox($ionicPopup);
    $scope.options = { enableHighAccuracy: true};
    $scope.updatePosition = function(){
        if($scope.workin){
            $rootScope.$broadcast('gotoLocation',Api.position);
            $scope.refreshLocation();
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                Api.position = {lat:c.latitude, lon:c.longitude};
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;        
                $scope.basel = {lat:c.latitude,lon:c.longitude};                
                console.log('Ubicación traida');
                $rootScope.$broadcast('gotoLocation',Api.position);
                $scope.refreshLocation();
                $scope.checkNewPaquetes();                
            },function(e) { 
                    cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
                        for (var permission in statuses){
                            switch(statuses[permission]){
                                case cordova.plugins.diagnostic.runtimePermissionStatus.GRANTED:
                                    alert("Permission granted to use "+permission);
                                    break;
                                case cordova.plugins.diagnostic.runtimePermissionStatus.NOT_REQUESTED:
                                    alert("Permission to use "+permission+" has not been requested yet");
                                    break;
                                case cordova.plugins.diagnostic.runtimePermissionStatus.DENIED:
                                    alert("Permission denied to use "+permission+" - ask again?");
                                    break;
                                case cordova.plugins.diagnostic.runtimePermissionStatus.DENIED_ALWAYS:
                                    alert("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
                                    break;
                            }
                        }
                    }, function(error){
                        console.error("The following error occurred: "+error);
                    }, [
                        cordova.plugins.diagnostic.runtimePermission.ACCESS_FINE_LOCATION,
                        cordova.plugins.diagnostic.runtimePermission.ACCESS_COARSE_LOCATION
                        ]);
            },$scope.options);
        }
        clearTimeout($scope.timer);
        $scope.timer = setTimeout($scope.updatePosition,10000);
    };        
    
     // check login code
    $ionicPlatform.ready(function() {
        //if connection with internet exist
            document.addEventListener("offline",function(){
                document.location.href="#/tab/disconected";
            }, false);
            document.addEventListener("online",function(){
                document.location.href="#/tab/main";
            }, false);
        
            $scope.updatePosition();  
            $rootScope.$broadcast('gotoLocation',Api.position);
            if(typeof($scope.checkNewPaquetes)==='function'){
                $scope.checkNewPaquetes();
            }
            if(typeof(Media)!=='undefined'){
                //loop
                Media.stop = false;
                var loop = function (status) {
                    if (!Media.stop && status === Media.MEDIA_STOPPED) {
                        Api.notificationSound.play();
                        navigator.vibrate(1000);
                    }
                };
                Api.notificationSound = new Media('/android_asset/www/sounds/notificacion.mp3',function(){},function(e){alert("Error getting pos="+e);},loop);
                Api.notificationSound.play();
                navigator.vibrate(1000);
                Media.stop = true;
            }
    });
    
    $ionicPlatform.on("resume", function(event) {
        if(typeof(Media)!=='undefined'){
            Media.stop = true;
        }
    });
    
    $scope.checkNewPaquetes = function(){};
    
    $scope.refreshLocation = function(){        
        $scope.refreshDataToServer.lat = Api.position.lat;
        $scope.refreshDataToServer.lon = Api.position.lon;
        $rootScope.$broadcast('updateCoords',Api.position.lat,Api.position.lon);
        $scope.updateServer();
    };
    
    $scope.updateServer = function(){
        $scope.refreshDataToServer.status = $scope.getStatus();
        $scope.refreshDataToServer.id = $scope.user;
        $scope.data = $scope.refreshDataToServer;
        Api.query('refreshLocation',$scope,$http,function(data){});
    };
    
    $scope.getStatus = function(){
        re = 5;
        if($scope.workin){
            re = 1;
        }        
        if($scope.siniestro){
            re = 4;
        }            
        return re;
    };
    
    //Botones
    $scope.repartidorKeepRunning = true;
    $scope.toggleRepartidorKeepRunning = function(value){
        if(value){
            cordova.plugins.repartidorKeepRunning.enable();
        }else{
            cordova.plugins.repartidorKeepRunning.disable();
        }
    }
    
    $scope.toggleWorkin = function(){        
        $scope.workin = $scope.workin?false:true;
        Api.workin = $scope.workin;
        if($scope.siniestro){
            $scope.workin = false;
        }
        if($scope.workin){
            $scope.updatePosition();
            $rootScope.$broadcast('SocketConnect',true);
        }
        else{
            $scope.updateServer();
            $rootScope.$broadcast('SocketConnect',false);
            clearTimeout($scope.timer);
        }
        
        if(!$scope.$$phase){
            $scope.$apply('workin');
            $scope.$apply();
        }
    };
    
    $scope.toggleSiniestro = function(){
        $scope.siniestro = $scope.siniestro?false:true;
        $scope.updateServer();
        if($scope.siniestro && $scope.workin){
            $scope.toggleWorkin();
        }
        if(!$scope.$$phase){
            $scope.$apply('siniestro');
            $scope.$apply();
        }
    };
    
    $scope.toggleCarga = function(){
        $scope.carga = $scope.carga?false:true;        
        if(!$scope.$$phase){
            $scope.$apply('carga');
            $scope.$apply();
        }
    };
    
    $scope.saveProximidad = function(proximidad){
        localStorage.triggerMetersFromDistance = proximidad;
    };
    $scope.proximidad = localStorage.triggerMetersFromDistance;                
    
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };        
    
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
    };        
});
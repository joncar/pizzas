controllers.controller('Pedidos', function($scope,$rootScope,$state,$ionicModal,$http,$stateParams,$ionicPlatform,$ionicPopup,$ionicLoading,Api,UI,User) {        
    if(typeof(Api.ajustes)==='undefined'){
        $state.go('tab.main');
    }else{
        $scope.consultar = function(){
            if(typeof($stateParams.id)!=='undefined'){
                Api.list('pedidos',{repartidores_id:$scope.user,id:$stateParams.id},$scope,$http,function(data){
                    $scope.detail = data[0];
                    $scope.detail.precio_cliente = parseFloat($scope.detail.precio_cliente);
                    $scope.detail.trayecto = JSON.parse($scope.detail.trayecto);
                    $scope.$broadcast('scroll.refreshComplete');
                    Api.task = $scope.detail.trayecto;
                     $scope.initModals();
                });
            }
            else if(typeof($stateParams.iddetalle)!=='undefined'){
                Api.list('pedidos_detalles',{id:$stateParams.iddetalle},$scope,$http,function(data){
                    $scope.detail = data[0];
                    $scope.datos = {nombre:$scope.detail.cliente,tarjeta:''};
                    $scope.detail.precio = parseFloat($scope.detail.precio);
                    $scope.detail.costo_entrega = parseFloat($scope.detail.precio_cliente);
                    if($scope.detail.public_key===null){
                        Conekta.setPublishableKey(Api.ajustes.public_key);                           
                    }else{
                        Conekta.setPublishableKey($scope.detail.public_key);                           
                    }
                });
            }else{
                document.location.href="#/tab/main";
            }
        };
        $scope.consultar();
        $scope = User.getData($scope);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Actualizando Pedido');
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        $scope.sendit = false;           

        $scope.initModals = function(){
            UI.getModalBox($ionicModal,'modals/entrega.html',$scope,function(){
                $scope.entregaModal = $scope.modal;        
                 $scope.$on('closeModalPedido',function(evt,data){
                    $scope.entregaModal.remove();
                    $scope.initModals();
                });
            });
        };

        $scope.notificarRecogida = function(iddetalle){            
            if($scope.detail.trayecto[1].direccion==='' || $scope.detail.trayecto[1].direccion===null){
               $scope.entregaModal.show();
            }else{                
                var id = iddetalle.id || '';
                var precio = iddetalle===undefined?0:parseFloat(iddetalle.precio);
                for(var i in $scope.detail.trayecto){
                    var task = $scope.detail.trayecto[i];
                    if(id==='' && task.status==='0'){
                        id = task.id;
                        precio = parseFloat(task.precio);                        
                        if(task.precio>0){
                            document.location.href="#/tab/pagos/"+id;
                        }
                    }
                }                
                if((precio===0 && $scope.detail.precio_cliente===0) || id===$scope.detail.trayecto[0].id){
                    $scope.data = {status:1};
                    Api.update('pedidos_detalles',id,$scope,$http,function(data){
                       $scope.verificarNotificacion($stateParams.id);
                       $state.go('tab.main');
                    });    
                    Api.triggerFromDistance = true;
                }else{
                    document.location.href="#/tab/pagos/"+id;
                }
            }
        };

        //Tokenizar la tarjeta
        $scope.procesarPago = function(datos){
            $scope.sendit = true;
            datos.fecha = datos.mes+'-'+datos.anio;
            $scope.data = datos;
            //Tokenizar tarjeta
            $form = {
                card:{
                    'name':datos.nombre,
                    'number':datos.tarjeta,
                    'cvc':datos.cvc,
                    'exp_month':datos.mes,
                    'exp_year':datos.anio
                }
            };            
            if($scope.detail.costo_entrega>0 && $scope.detail.precio===0){
                $scope.cobrar_envio($form);
            }else{
                Conekta.token.create($form,
                function(data){
                    $scope.data.tarjeta = data.id;
                    //Procesar otra tarjeta
                    if($scope.detail.costo_entrega>0){
                        $scope.cobrar_envio($form);
                    }else{
                        $scope.payFromClient();
                    }
                },
                function(data){
                    $scope.showAlert('Disculpe pero su transacción no fué aprobada ['+data.message_to_purchaser+']');
                    $scope.sendit = false;
                });
            }
        };

        $scope.cobrar_envio = function($form){
            Conekta.setPublishableKey(Api.ajustes.public_key);
            Conekta.token.create($form,
            function(data){
                $scope.data.tarjeta2 = data.id;
                //Procesar otra tarjeta
                $scope.payFromClient();
            },
            function(data){
                $scope.showAlert('Disculpe pero su transacción no fué aprobada ['+data.message_to_purchaser+']');
                $scope.sendit = false;
            });
        };
        
        $scope.cobrar_envio_efectivo = function(){
            $scope.data = {'sucursales_id':$scope.detail.sucursales_id,'monto':$scope.detail.costo_entrega,'detalle':"Pago por envio de un cliente de la sucursal "+$scope.detail.sdfe04682+" por un monto de $ "+$scope.detail.costo_entrega+"  por parte de "+$scope.detail.cliente+" con el #envio: "+$scope.detail.pedidos_id,'pedidos_id':$scope.detail.pedidos_id,'tipo':'2'};
                Api.insert('transacciones',$scope,$http,function(data){
                    $scope.sendit = false;
                    $state.go('tab.main');
                });
        };

        $scope.payFromClient = function(){
            Api.query('payFromClient/'+$scope.detail.id,$scope,$http,function(data){
                if(data==='success'){
                    $scope.showAlert('El pedido se ha procesado con éxito');       
                    $scope.verificarNotificacion($scope.detail.pedidos_id);
                    document.location.href="#/tab/main";
                    $scope.$emit('refresh','');
                    $scope.sendit = false;
                }else{
                    $scope.sendit = false;
                    $scope.datos.tarjeta = '';
                    $scope.showAlert(data);
                }
            }); 
        };

        $scope.cargarPagoEfectivo = function(){
          $scope.sendit = true;
          if($scope.detail.precio>0){              
                $scope.data = {'sucursales_id':$scope.detail.sucursales_id,'monto':$scope.detail.precio,'detalle':"Pago a la sucursal  "+$scope.detail.sdfe04682+' por un monto de '+$scope.detail.precio+' por parte de '+$scope.detail.cliente+' con el #envio: '+$scope.detail.pedidos_id,'pedidos_id':$scope.detail.pedidos_id,'tipo':'2'};
                Api.insert('transacciones',$scope,$http,function(data){
                    if(data.success){
                        $scope.data = {'status':1,'tipo_pago':$scope.detail.tipo_pago};
                        Api.update('pedidos_detalles',$scope.detail.id,$scope,$http,function(data){
                            $scope.verificarNotificacion($scope.detail.pedidos_id);
                            //Cargar pago por envio
                            if($scope.detail.costo_entrega>0){
                                $scope.cobrar_envio_efectivo();
                            }else{
                              $scope.sendit = false;
                              $state.go('tab.main');
                            }
                        });
                    }
                });
            }else{
                $scope.data = {'status':1,'tipo_pago':$scope.detail.tipo_pago};
                Api.update('pedidos_detalles',$scope.detail.id,$scope,$http,function(data){
                    $scope.verificarNotificacion($scope.detail.pedidos_id);
                    if($scope.detail.costo_entrega>0){
                       $scope.cobrar_envio_efectivo();
                    }else{
                       $scope.sendit = false;
                       $state.go('tab.main');
                    }
                });                
            }
        };
        $scope.verificarNotificacion = function(id){
            Api.list('pedidos_detalles',{'pedidos_detalles.status':'0',pedidos_id:id},$scope,$http,function(data){
               if(data.length===0){
                   $rootScope.$broadcast('finishEntrega',{sucursales_id:$scope.detail.sucursales_id,pedidos_id:id,repartidores_id:$scope.user});           
               } 
            },'where');
        };
        $scope.meses = [];
        $scope.anios = [];
        var l = new Date();
        l = l.getFullYear();
        for(i=1;i<13;i++){
            $scope.meses.push(i);
        }
        for(i=l;i<l+10;i++){
            $scope.anios.push(i);
        }
    }
});
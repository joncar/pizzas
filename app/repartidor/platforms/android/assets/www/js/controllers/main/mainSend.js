controllers.controller('MainSend', function($scope,$state,$rootScope,$ionicModal,$ionicSideMenuDelegate,$ionicPopup,$ionicPlatform,Api,UI,User,SocketConnection,$http) {
    $scope = User.getData($scope);
    $scope.datos = [];
    $scope.task = [];
    $scope.datos.init = '';
    $scope.datos.direccion = [''];
    $scope.tipo = '';
    $scope.divid = '';
    $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
    };

    $scope.inputForm = {
      street_number: 'ext',
      route: 'calle',
      locality: 'delegacion',
      administrative_area_level_1: 'ciudad',
      postal_code: 'cp',
      sublocality_level_1:'colonia'
    };

    $scope.selectFieldsFunction = function(place){
        var datos = {};
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                datos[$scope.inputForm[addressType]] = val;
            }
          }
          datos.direccion = place.formatted_address;
          datos.tipo = $scope.tipo;
          var posicion = undefined;
          if(typeof($scope.focused)!=='undefined'){
              posicion = $scope.focused==='init'?0:parseInt($scope.focused)+1;
          }          
          $scope.fillDatos(datos,posicion);
          if(!$scope.$$phase){
              $scope.$apply();
          }
    };

    $scope.disabletap = function(id){
        $scope.focused = id;
        container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        angular.element(container).css('pointer-events', 'auto');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
            var e = document.getElementsByClassName('autocompletegoogleplace').length;
            for(var i=0;i<e;i++){
                document.getElementsByClassName('autocompletegoogleplace')[i].blur();
            }
        });
    };        
    
    $scope.initInputs = function(){
            var task = Api.task;
            $scope.datos.init = task[0].direccion;
            $scope.entregainput = {};
            $scope.entregainput.input = [];
            for(var i=1;i<task.length;i++){
                $scope.entregainput.input.push(i-1);
                $scope.datos.direccion[i-1] = '';
            }
            $scope.task = task;
            $scope.drawText();
    };
    
    $scope.fillDatos = function(datos,posicion){
        if($scope.task.length===0){
          $scope.tipo = $scope.tipo===0?1:0;
        }
        if(typeof(posicion)==='undefined'){
            $scope.task.push(datos);
        }else{
            var id = $scope.task[posicion].id;
            $scope.task[posicion] = datos;
            $scope.task[posicion].id = id;
        }        
    };          
    
    $scope.drawText = function(){
        for(var i=0;i<document.getElementsByClassName('autocompletegoogleplace').length;i++){
            if(typeof($scope.task[i])!=='undefined'){
                document.getElementsByClassName('autocompletegoogleplace')[i].value = $scope.task[i].direccion;
                console.log($scope.task[i]);
            }else{
                document.getElementsByClassName('autocompletegoogleplace')[i].value = '';
            }            
        }
    };
    
    $scope.sendDatos = function(datos,programar){
        if($scope.task.length>=2){
            //Ver si a alguno le falta las coordenadas
            var ok = true;
            for(var i in $scope.task){
                if($scope.task[i].direccion===''){                    
                    ok = false;
                }
            }
            if(ok){
                //Ir al pago
                $scope.close();
                //Ir a pedir datos faltantes
                Api.datos = {task:$scope.task};
                document.location.href="#/tab/task";
            }
            else{
                alert('Por favor, llene los datos del pedido para poder ser procesado');
            }
        }else{
            alert('Por favor, llene los datos del pedido para poder ser procesado');
        }
    };          
    
    $scope.close = function(){
        $rootScope.$broadcast('closeModalPedido',{});
    };
    $scope.initInputs();
});
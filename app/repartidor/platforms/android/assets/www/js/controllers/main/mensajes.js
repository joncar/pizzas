controllers.controller('Mensajes', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.consult = function(){
        Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
            
            var d = [];
            for(var i=data.length-1;i>=0;i--){
                d.push(data[i]);
            }
            Api.lista = d;
            $scope.lista = d;
            
        },'where');
    };
    $scope.consult();
});

controllers.controller('ReadMensajes', function($scope,$rootScope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.consultar = function(){
        if(Api.lista===undefined){
            Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
                Api.lista = data;
                $scope.lista = data;
            },'where');
        }
        if(Api.mensaje===undefined){            
            $scope.data = {'pedido_id':$stateParams.id};
            Api.query('getMensajes',$scope,$http,function(data){    
                if(typeof(data)!=='object'){
                    data = JSON.parse(JSON.parse(data));
                    $scope.mensajes = data;
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }
                else{
                    $scope.mensajes = [];
                }
            });
        }
        $scope.$broadcast('scroll.refreshComplete');
    };
    
    $scope.mensaje = '';
    
    $scope.sendMensaje = function(mensaje){
        $scope.mensaje = '';
        var dest = 0;
        for(var i in Api.lista){
            if(Api.lista[i].id===$stateParams.id){
                dest = Api.lista[i].sucursales_id;
            }
        }
        var data = {userId:$scope.user,userName:$scope.nombre,mensaje:mensaje,dest:dest,pedido_id:$stateParams.id};        
        $scope.mensajes.push(data);
        $rootScope.$broadcast('sendmensajetoserver',data);
    };
    
    $scope.$on('message_receiver',function(evt,data){
        console.log('mensaje recibido');
        $scope.mensajes.push(data);
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    console.log(controllers);
    $scope.consultar();
});
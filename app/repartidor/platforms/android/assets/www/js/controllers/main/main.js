var myPopup = '';
var controllers = angular.module('starter.controllers', []);

controllers.controller('Main', function($scope,$rootScope,$state,$http,$ionicPlatform,$ionicPopup,Api,UI,User) {
    Api.triggerFromDistance = Api.triggerFromDistance===undefined?true:Api.triggerFromDistance; //Si ya se ha disparado la solicitud de recogida cuando este cercano a la ubicación    
    $scope.direccion = '';    
    
    $scope.$on('gotoLocation',function(evt,data){
       $scope.basel = {lat:data.lat,lon:data.lon}; 
    });
    $scope = User.getData($scope);
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/motorcycle.png';
    $scope.getMarks = function(){
        Api.list('pedidosMotos',{repartidores_id:$scope.user},$scope,$http,function(data){
            $scope.whoiswhere = [];
            for(var i in data){
                var lat = data[i].lat;
                var lon = data[i].lon;
                
                var icon = data[i].tipo==='1'?'http://74.208.12.230/pizzasapp/img/fooddeliveryservice.png':'http://74.208.12.230/pizzasapp/img/bighousegreen.png';
                var direccion = data[i].direccion;
                console.log(direccion);
                $scope.whoiswhere.push({ "id":data[i].id,"name":'Nro Pedido: '+data[i].id, lat: lat, lon: lon,datos:data[i],icon:icon,direccion:direccion });                
            }
            
            if($scope.whoiswhere.length>0){
                $scope.direccion = $scope.whoiswhere[0].direccion;
                $scope.coordsOfDelivery = {lat:$scope.whoiswhere[0].lat,lon:$scope.whoiswhere[0].lon};
                if(!$scope.$$phase){
                    $scope.$apply();
                }
                $scope.basel = {lat:$scope.whoiswhere[0].lat,lon:$scope.whoiswhere[0].lon};
            }
            //Ver si alguna de los pedidos esta cercano para tirar la entrega
            if(Api.triggerFromDistance){
                $scope.validarDistancia();
            }
        });
    };        
    $scope.$on('refresh',function(){
       $scope.getMarks();
    });
    $scope.pedidoclick = function(mark){
        document.location.href="#/tab/pedidos/"+mark.id;
    };
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type === Connection.NONE) {
                $state.go('tab.disconected');
            }
        }
        
        
        $scope.$broadcast('refresh');
        
        //Background
        // Android customization
        //cordova.plugins.backgroundMode.setDefaults({ title:'DeliveryMix', text:'Repartidor Activo'});
        // Enable background mode
        if(typeof(cordova)!=='undefined'){
            cordova.plugins.backgroundMode.enable();
            cordova.plugins.backgroundMode.onactivate = function () {
                cordova.plugins.backgroundMode.configure({
                    text:'Repartidor Activo'
                });
            };
            //Comenzar demonio interrumpible
            cordova.plugins.repartidorKeepRunning.configure({id:$scope.user,nombre:$scope.nombre});
        }        
    });
    
    $scope.validarDistancia = function(){
         //10.514440437420241,  lon=-66.92120273862304
        Math.radians = function(degrees) {
            return degrees * Math.PI / 180;
         };
        var max_meters = localStorage.triggerMetersFromDistance===undefined?200:localStorage.triggerMetersFromDistance;
        var lat_entrega = parseFloat(localStorage.lat);
        var lon_entrega = parseFloat(localStorage.lon);
        for(var i in $scope.whoiswhere){
            var m = $scope.whoiswhere[i];
            var lat_recogida = parseFloat(m.lat);
            var lon_recogida = parseFloat(m.lon);
            var distancia = (6378.137*Math.acos(Math.cos(Math.radians(lat_entrega))*Math.cos(Math.radians(lat_recogida))*Math.cos(Math.radians(lon_recogida)-Math.radians(lon_entrega))+Math.sin(Math.radians(lat_entrega))*Math.sin(Math.radians(lat_recogida)))/0.62137)*1000;
            if(distancia<=max_meters && Api.triggerFromDistance){
                //Disparar recogida                
                document.location.href="#/tab/pedidos/"+m.id;
                Api.triggerFromDistance = false;
            }
        }
    };
    if(typeof(Api.ajustes)==='undefined'){
        Api.list('ajustes',{},$scope,$http,function(data){ 
            Api.ajustes = data[0]; 
        });
    }
    
    $scope.openNavigation = function(){
        window.open('http://maps.google.com/maps?q='+$scope.coordsOfDelivery.lat+','+$scope.coordsOfDelivery.lon, '_system');
    };
});
controllers.controller('Balance', function($scope,$ionicLoading,$http,Api,UI,User) {        
        $scope = User.getData($scope);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando datos');
        $scope.envioshoy = 0;
        $scope.enviosmes = 0;        
        Api.list('repartidores',{id:$scope.user},$scope,$http,function(data){
            data = JSON.parse(data[0].tarifa);
            $scope.envioshoy = data.hoy;
            $scope.enviosmes = data.mes;
        });
});
package joncar.repartidorKeepRunning;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

public class RepartidorKeepRunningService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private boolean connected = false;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Intent inten = new Intent(this, RepartidorKeepRunningService.class);
        inten.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification noti = new Notification.Builder(getApplicationContext())
                .setContentTitle(RepartidorKeepRunning.setting.optString("nombre"))
                .setContentText("Conectado")
                .setSmallIcon(getIconResId())
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1234, noti);
        return flags;

    }

    private int getIconResId() {
        Context context = getApplicationContext();
        Resources res   = context.getResources();
        String pkgName  = context.getPackageName();

        int resId;
        resId = res.getIdentifier("icon", "drawable", pkgName);

        return resId;
    }
}
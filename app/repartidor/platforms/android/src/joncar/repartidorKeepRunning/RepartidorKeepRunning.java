package joncar.repartidorKeepRunning;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URISyntaxException;

public class RepartidorKeepRunning extends CordovaPlugin implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    private final ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            // Nothing to do here
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // Nothing to do here
        }
    };

    public static JSONObject setting = null;
    public static boolean keepRunning;
    private boolean enable = true;
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if("configure".equals(action)){
            setting = args.getJSONObject(0);
        }
        else if("enable".equals(action)){
            enable = true;
        }
        else if("disable".equals(action)){
            enable = false;
        }
        return true;  // Returning false results in a "MethodNotFound" error.
    }

    @Override
    public void onDestroy(){
        //Abrir demonio
        if(enable && setting!=null) {
            keepRunning = true;
            Activity context = cordova.getActivity();
            Intent intent = new Intent(context, RepartidorKeepRunningService.class);
            context.bindService(intent, connection, Context.BIND_AUTO_CREATE);
            context.startService(intent);
            getLocation();
            keepRunning();
        }
        super.onDestroy();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        Activity context = cordova.getActivity();
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.v("TESTING",serviceClass.getName());
                return true;
            }
        }
        return false;
    }

    @Override
    public void onStart(){
        //Matar demonio
        if(isMyServiceRunning(RepartidorKeepRunningService.class)) {
            Activity context = cordova.getActivity();
            Intent intent = new Intent(context, RepartidorKeepRunningService.class);
            context.stopService(intent);
            keepRunning = false;
        }
    }

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public void getLocation(){
        Log.v("TESTING","Consiguiendo Locacion");
        // Create an instance of GoogleAPIClient.
        Activity context = cordova.getActivity();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mLocationRequest=new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mGoogleApiClient.connect();
        }
    }

    Location mLastLocation;
    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.v("TESTING", String.valueOf(mLastLocation.getLatitude()) + "," + String.valueOf(mLastLocation.getLongitude()));
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Actualizar coordenadas en el servicio
        if(RepartidorKeepRunning.keepRunning) {
            Log.v("TESTING", String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()));
            try {
                JSONObject s = new JSONObject();
                s.put("lat", location.getLatitude());
                s.put("lon", location.getLongitude());
                s.put("nombre", setting.optString("nombre"));
                mSocket.emit("updateCoords", s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
            mSocket.disconnect();
        }
    }


    /*** Conexion con el socket
     * /*Connect with socket */
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://74.208.12.230:3000");
        } catch (URISyntaxException e) {}
    }

    public void keepRunning(){
        try {
            /**** En este lugar se puede seguir con el codigo de refresco de las coordenadas para que el nodejs le haga seguimiento****/
            /** Testing nodejs ***/
            Log.v("TESTING", "Comenzando NodeJs");
            JSONObject s = new JSONObject();
            s.put("type","repartidorKeepRunning");
            s.put("id",setting.optInt("id"));
            s.put("nombre",setting.optString("nombre"));
            mSocket.connect();
            mSocket.emit("connectID", s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/main/tabs.html',
    controller: 'Socket'
  })

  // Each tab has its own nav history stack:

  .state('tab.main', {
    url: '/main',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/main.html',
        controller:'Main'
      }
    }
  })
  
  .state('tab.task', {
    url: '/task',
    cache:false,    
    views: {
      'tab-main': {
        templateUrl: 'templates/main/task.html',
        controller: 'Task'
      }
    }
  })
  
  .state('tab.mapa', {
    url: '/mapa',
    cache:false,    
    views: {
      'tab-main': {
        templateUrl: 'templates/main/mapa.html',
        controller: 'Mapa'
      }
    }
  })
  
  //Obsoleta
.state('tab.envio', {
    url: '/envio',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/envios.html',
        controller: 'Envios'
      }
    }
  })
  //Obsoleta
  .state('confirmar', {
    cache:false,
    url: '/confirmar',
    templateUrl: 'templates/main/confirmar.html',
    controller: 'Confirmar'    
  })
  //Obsoleta
  .state('tab.pedirentrega', {
    url: '/pedirentrega',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/entregas.html',
        controller: 'Entregas'
      }
    }
  })
  //Obsoleta
  .state('confirmar_entrega', {
    cache:false,
    url: '/confirmar_entrega',
    templateUrl: 'templates/main/confirmar_destino.html',
    controller: 'Confirmar_entrega'    
  })
  
  .state('tab.aceptarprecio', {
    cache:false,
    url: '/aceptarprecio',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/aceptarprecio.html',
        controller: 'aceptarprecio'    
      }
    }    
  })
  
.state('tab.buscarRepartidor', {
    cache:false,
    url: '/buscarRepartidor',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/buscarRepartidor.html',
        controller: 'buscarRepartidor'
      }
    }
  })
  
.state('tab.cuenta', {
    cache:false,
    url: '/cuenta',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/cuenta.html',
        controller: 'Cuenta'
      }
    }
  })
 
  .state('tab.envios', {
    cache:false,
    url: '/envios',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/envioslist.html',
        controller: 'EnviosList'
      }
    }
  })
  
  .state('tab.envioDetail', {
    cache:false,
    url: '/pedido/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/enviosdetail.html',
        controller: 'EnviosDetail'
      }
    }
  })
  
  .state('tab.AsignarRepartidor', {
    cache:false,
    url: '/AsignarRepartidor/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/buscarRepartidor.html',
        controller: 'AsignarRepartidor'
      }
    }
  })
  
  .state('tab.mensajes', {
    cache:false,
    url: '/mensajes',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/mensajes.html',
        controller: 'Mensajes'
      }
    }
  })
  
  .state('tab.mensajesRead', {
    cache:false,
    url: '/mensajes/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/mensajesRead.html',
        controller: 'ReadMensajes'
      }
    }
  })
  
  .state('tab.calificar', {
    cache:false,
    url: '/calificar/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/calificar.html',
        controller: 'Calificar'
      }
    }
  })
  
  .state('tab.balance', {
    cache:false,
    url: '/balance',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/balance.html',
        controller: 'Balance'
      }
    }
  })
  
  .state('tab.favoritos', {
    url: '/favoritos',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/favoritos.html',
        controller:'Favoritos'
      }
    }
  })
  
  .state('tab.favoritosAdd', {
    url: '/favoritos/add/:fav',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/favoritosAdd.html',
        controller:'Envios'
      }
    }
  })
  
  .state('tab.favoritosEdit', {
    url: '/favoritos/add/:fav/:id',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/favoritosEdit.html',
        controller:'Favoritos'
      }
    }
  })
  
  .state('confirmarFav', {
    cache:false,
    url: '/confirmar/:fav',
    templateUrl: 'templates/main/confirmar.html',
    controller: 'Confirmar'    
  })
  
  .state('tab.envioFav', {
    cache:false,
    url: '/enviosFav',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/favoritos.html',
        controller:'EnviosFav'
      }
    }
  })
  
  .state('tab.disconected', {
    url: '/disconected',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/disconnected.html',
        controller:'Main'
      }
    }
  })
  
  .state('tab.test', {
    url: '/test',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/test.html',
        controller:'Test'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/main');

});

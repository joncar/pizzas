var myPopup = [];
angular.module('starter.controllers', [])

.controller('Main', function($scope,$state,$ionicSideMenuDelegate,$ionicPopup,$ionicPlatform,Api,UI,User,SocketConnection,$http) {
    $scope.auto = function(el){
        console.log(el);
    };
    $scope = User.getData($scope);
    if(localStorage.lat===undefined){
        localStorage.lat = '47.55633987116614';
        localStorage.lon = '7.576619513223015';
    }
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }        
   
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    
    $scope.newEnvio = function(){
        $state.go('tab.envio');
    };
    $scope.newEnvioFav = function(){
        $state.go('tab.envioFav');
    };
    
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
        navigator.app.exitApp();
    };
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    SocketConnection.WebSocketOpen($scope);
    SocketConnection.connectID($scope.user);
    
    //Background
    // Android customization        
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type == Connection.NONE) {
                $state.go('tab.disconected');
            }
        }
        
        
        Api.list('ajustes',{},$scope,$http,function(data){ 
            Api.ajustes = data[0]; 
        });                
        if(typeof(cordova)!=='undefined'){
            cordova.plugins.backgroundMode.setDefaults({ title:'DeliveryMix', text:'Sucursal conectada'});
            // Enable background mode
            cordova.plugins.backgroundMode.enable();
            cordova.plugins.backgroundMode.onactivate = function () {
                cordova.plugins.backgroundMode.configure({
                    title:'App Activo',
                    text:'App Activa'
                });
            };
                         
            Api.notificationSound = new Media('sounds/notificacion.mp3',function(){},function(e){alert("Error getting pos="+e);});
            Api.notificationSound.play();               
        }
        
        ws.on('respuestaSolicitud',function(data){
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Respuesta de solicitud.',
                    text:'Haz recibido una respuesta de tu solicitud.'
                });
                Api.notificationSound.play();
                navigator.vibrate(1000);
        }
        Api.respuestaSolicitudRepartidor(data);
    });
        
    ws.on('message_receiver',function(data){
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Mensaje recibido.',
                    text:'Te han enviado un mensaje.'
                });
                Api.notificationSound.play();
                navigator.vibrate(1000);
        }
        
       if($state.current.name==='tab.mensajesRead'){
           $scope.$broadcast('message_receiver',data);
       }
       else{            
            myPopup = $ionicPopup.show({
                template: data.mensaje,
                title: 'Haz recibido un mensaje',
                subTitle: 'Nro. Pedido: '+data.pedido_id,
                scope: $scope,
                buttons: [
                    {
                      text: '<b>Cerrar</b>',              
                      onTap: function(e) {                        
                          myPopup.close();
                          myPopup.close = undefined;
                      }
                    },
                    {
                      text: '<b>Ver Mensaje</b>',
                      onTap: function(e) {                        
                        document.location.href="#/tab/mensajes/"+data.pedido_id;                        
                      }
                    }
                  ]
          });
       }
    });
    
    $scope.$on('sendmensajetoserver',function(evt,data){
        ws.emit('sendMessage',data);
    });    
    
    ws.on('finishEntrega',function(data){
        console.log(data);
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Entrega finalizada.',
                    text:'Se ha realizado una entrega.'
                });
                Api.notificationSound.play();
                navigator.vibrate(1000);
        }
        if($state.current.name!=='tab.envioDetail'){
            var myPopup = $ionicPopup.show({
                template: data.mensaje,
                title: ' El pedido #'+data.pedidos_id,
                subTitle: 'ha sido entregado con éxito.',
                scope: $scope,
                buttons: [
                    {
                      text: '<b>Cerrar</b>',              
                      onTap: function(e) {                        
                          myPopup.close();
                      }
                    },
                    {
                      text: '<b>Ver Pedido</b>',
                      onTap: function(e) {                        
                        document.location.href="#/tab/pedido/"+data.pedidos_id;                        
                      }
                    }
                ]
            });
        }
    });
    
    ws.on('rankearRepartidor',function(data){
        
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Rankea a tu repartidor.',
                    text:'Como te ha ido con tu repartidor?.'
                });
                Api.notificationSound.play();
                navigator.vibrate(1000);
        }
        if($state.current.name!=='tab.calificar'){
            var myPopup = $ionicPopup.show({
                template: data.mensaje,
                title: '¿Como fue la entrega del repartidor?',
                subTitle: 'Danos tu opinión sobre la entrega de tu envío #'+data.idPedido,
                scope: $scope,
                buttons: [
                    {
                      text: '<b>Cerrar</b>',              
                      onTap: function(e) {                        
                          myPopup.close();
                      }
                    },
                    {
                      text: '<b>Calificar</b>',
                      onTap: function(e) {                        
                        document.location.href="#/tab/calificar/"+data.repartidores_id;                        
                      }
                    }
                ]
            });
        }
    });
    });                
})

.controller('Envios', function($scope,$state,$http,$ionicPlatform,$stateParams,$ionicSideMenuDelegate,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);    
    $scope.modal = UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope);
    Api.innerHeight = window.innerHeight;
    var f = new Date();
    var fecha_solicitud = f.getFullYear()+'-'+f.getMonth()+'-'+f.getDate()+' '+f.getHours()+':'+f.getMinutes()+':'+f.getSeconds();
    $scope.datos = {ownposition:true,fecha_solicitud:fecha_solicitud,ciudad:'',delegacion:'',ext:''};
    $scope.d = {fechas:'',hora:''};
    $scope.closeProgramar = function(){
        $scope.toggleModal('hide');
    };
    $scope.toggleProgramar = function(datos){
        if(datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition){            
            $scope.datos = datos;
            if(!$scope.$$phase){
                $scope.$apply();
            }
            $scope.toggleModal('show');
        }else{
            alert('Debe llenar el campo Int antes de continuar');
        }
    };
    
    $scope.selectFecha = function(datos,d){
        datos.fecha_solicitud = d.fechas.getFullYear()+'-'+d.fechas.getMonth()+'-'+d.fechas.getDate()+' '+d.hora.getHours()+':'+d.hora.getMinutes()+':'+d.hora.getSeconds();
        d.fechas.setHours(d.hora.getHours(), d.hora.getMinutes(), d.hora.getSeconds(), 0);
        var fecha = new Date();
        if(fecha.getTime()>=d.fechas.getTime()){
            alert('Debe asignar una fecha con un intervalo mayor a 2 horas como mínimo');
        }else{            
            var m = (d.fechas.getTime()-fecha.getTime())/1000;
            if(m<=7200){
                alert('Debe asignar una fecha con un intervalo mayor a 2 horas como mínimo');
            }else{
                $scope.toggleModal('hide');
                $scope.modal = UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope);
                Api.time = m;
                $scope.enviarPedido(datos,true);
            }
        }
    };
    
   $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
      };
      
      $scope.inputForm = {
        street_number: 'ext',
        route: 'calle',
        locality: 'delegacion',
        administrative_area_level_1: 'ciudad',
        postal_code: 'cp',
        sublocality_level_1:'colonia'
      };
    
    $scope.selectFieldsFunction = function(place){
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                $scope.datos[$scope.inputForm[addressType]] = val;
            }
          }
          $scope.datos.direccion = place.formatted_address;
          if(!$scope.$$phase){
              $scope.$apply();
          }
    };
    
    $scope.enviarPedido = function(datos,programar){
        if(datos.localidad!==undefined && datos.localidad!=='' && (datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition)){
            datos.direccion = datos.direccion===undefined?null:datos.direccion;        
            Api.datos = datos;
            Api.programar = programar;
            if($stateParams.fav===undefined){             
             $state.go('confirmar');
            }else{
                document.location.href="#/confirmar/1";
            }            
        }else{
            alert('Debe llenar el campo localidad antes de continuar');
        }
    };
            
    $scope.disabletap = function(){
            
        container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        angular.element(container).css('pointer-events', 'auto');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
            document.getElementById('autocompletegoogleplace').blur();
        });
    };
})

.controller('Confirmar', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User,$stateParams) {
    $scope.datos = Api.datos;
    $scope.direccion = Api.datos.direccion;
    $scope.basel = { lat: localStorage.lat, lon: localStorage.lon };
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando Pedido');
    $scope.gps = false;
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/fooddeliveryservice.png';
    $scope.navigate = false;
    $scope.widthButtonConfirmar = '50%';
    $scope.widthButtonCancelar = '48%';
    $scope.innerheight = Api.innerHeight+'px';
    $scope.unirDireccion = function(){
        Api.datos.direccion = Api.datos.calle===undefined?'':' '+Api.datos.calle;
        Api.datos.direccion+= Api.datos.ext === undefined?'':' ext '+Api.datos.ext;
        Api.datos.direccion+= Api.datos.colonia===undefined?'':' '+Api.datos.colonia;
        Api.datos.direccion+= Api.datos.int===undefined?'':' int '+Api.datos.int;
        Api.datos.direccion+= Api.datos.delegacion===undefined?'':' '+Api.datos.delegacion;
        Api.datos.direccion+= Api.datos.ciudad===undefined?'':' '+Api.datos.ciudad;
        Api.datos.direccion+= Api.datos.cp===undefined?'':' cp '+Api.datos.cp;
        Api.datos.direccion+= Api.datos.localidad===undefined?'':' '+Api.datos.localidad;
    };
    
    $scope.updatePosition = function(){
            if($scope.datos.direccion===null){
            $scope.selectFieldsFunction = function(place){
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if($scope.componentForm[addressType]){
                            var val = place.address_components[i][$scope.componentForm[addressType]];
                            $scope.datos[$scope.inputForm[addressType]] = val;
                            Api.datos[$scope.inputForm[addressType]] = val;
                        }
                      }
                      $scope.unirDireccion();
                      if(!$scope.$$phase){
                          $scope.$apply();
                      }
                };
                
                $scope.componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    postal_code: 'short_name',
                    sublocality_level_1:'long_name'
                };

                $scope.inputForm = {
                    street_number: 'ext',
                    route: 'calle',
                    locality: 'delegacion',
                    administrative_area_level_1: 'ciudad',
                    postal_code: 'cp',
                    sublocality_level_1:'colonia'
                };
                
    
            $scope.options = { enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;
                $scope.$apply();                   
      
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma tu ubicación','Si sientes que el GPS no ha encontrado tu ubicación, puedes arrastrar el marcador hacia tu ubicación exacta');
                UI.getLocationName(c.latitude,c.longitude,function(dir){
                   $scope.selectFieldsFunction(dir);
                   $scope.gps = true;
                });                
             },function(e){
                 alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');
                 $scope.gotoLocation(localStorage.lat, localStorage.lon);
             },$scope.options);                                      
            }
            else{
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma tu ubicación','Pulsa sobre alguno de los iconos del mapa para saber cual es tu ubicación');
            }
    };
        
    $scope.location = function(value){
        $scope.basel = value;
        if(!$scope.$$phase){
            $scope.$apply("basel");
        }
        if($scope.gps){
            UI.getLocationName(value.lat,value.lon,function(dir){
                $scope.selectFieldsFunction(dir);
                $scope.confirmar();
             });
        }else{
            $scope.confirmar();
        }
    };
    
     // check login code
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });        
    
    $scope.confirmar = function(){
        Api.datos.lat = $scope.basel.lat;
        Api.datos.lon = $scope.basel.lon;
        Api.datos.sucursales_id = $scope.user;
        Api.datos.ubicacion = '('+$scope.basel.lat+','+$scope.basel.lon+')';
        if(!$scope.gps){
            $scope.unirDireccion();
            console.log(Api.datos.direccion);
        }
        Api.datos.fecha_solicitud = $scope.fecha_solicitud;        
        Api.datos.status = 1;
        //Solicitar datos de entrega
        if($stateParams.fav===undefined){
            $state.go('tab.pedirentrega');
        }else{
            Api.addFav();
        }
    };
    
    $scope.cancelar = function(){
        $state.go('tab.envio');
    };
    
    $scope.lugarEnfocado = 0;
    $scope.lugares = [];
    $scope.navegarlugares = function(lugares){        
        $scope.lugarEnfocado = 0;
        $scope.lugares = lugares;
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng());
        if(lugares.length>1){
            $scope.navigate = true;
            $scope.widthButtonConfirmar = '28%';
            $scope.widthButtonCancelar = '26%';
        }
    };
    $scope.seleccionarIzquierda = function(){
        $scope.lugarEnfocado -= 1;
        if($scope.lugarEnfocado===-1){
            $scope.lugarEnfocado = $scope.lugares.length-1;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    $scope.seleccionarDerecha = function(){
        $scope.lugarEnfocado += 1;
        if($scope.lugarEnfocado>=$scope.lugares.length){
            $scope.lugarEnfocado = 0;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    
    $scope.gotoLocation = function (lat, lon) {            
        if ($scope.lat !== lat || $scope.lon !== lon) {
                $scope.basel = { lat: lat, lon: lon };
        }
     };
})

.controller('Entregas', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.datos = {ownposition:true};
    $scope.tipoPago = function(val){
        console.log(val);
    };
    $scope.enviarPedido = function(datos,programar){
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        if(datos.nombre!==undefined && (datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition)){
            if(datos.precio!==undefined && datos.tipo_pago===undefined){
                $scope.showAlert('¿Faltan datos?','Faltan datos por completar, verifica que el nombre del cliente y los datos de envío estén correctamente');
            }else{                            
                Api.datos.direccion_entrega = datos.direccion_entrega===undefined?null:datos.direccion_entrega;
                Api.datos.cliente = datos.nombre;
                Api.colonia = datos.colonia;
                Api.ext = datos.ext;
                Api.int = datos.int;
                Api.cp = datos.cp;
                Api.calle = datos.calle;
                Api.datos.tipo_pago = datos.tipo_pago;
                Api.datos.precio = datos.precio;
                Api.datos.programar = programar;
                $state.go('confirmar_entrega');
            }
        }
        else{            
            $scope.showAlert('¿Faltan datos?','Faltan datos por completar, verifica que el nombre del cliente y los datos de envío estén correctamente');
        }
    };
    
    $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
      };
      
      $scope.inputForm = {
        street_number: 'ext',
        route: 'calle',
        locality: 'delegacion',
        administrative_area_level_1: 'ciudad',
        postal_code: 'cp',
        sublocality_level_1:'colonia'
      };
    
    $scope.selectFieldsFunction = function(place){
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                $scope.datos[$scope.inputForm[addressType]] = val;
            }
          }
          $scope.datos.direccion_entrega = place.formatted_address;
          if(!$scope.$$phase){
              $scope.$apply();
          }
    };
            
    $scope.disabletap = function(){
            console.log('focused');
            container = document.getElementsByClassName('pac-container');
            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');
            angular.element(container).css('pointer-events', 'auto')
            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function(){
                document.getElementById('autocompletegoogleplace').blur();
            });
        };
    })

.controller('Confirmar_entrega', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope.datos = Api.datos;    
    $scope.basel = { lat: localStorage.lat, lon: localStorage.lon };
    $scope = User.getData($scope);            
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando Pedido');
    $scope.direccion = $scope.datos.direccion_entrega;
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/bighousegreen.png';
    $scope.gps = false;
    $scope.widthButtonConfirmar = '50%';
    $scope.widthButtonCancelar = '48%';
    $scope.innerheight = Api.innerHeight+'px';
    $scope.navigate = false;
    $scope.unirDireccion = function(){
        Api.datos.direccion_entrega = Api.calle===undefined?'':' '+Api.calle;
        Api.datos.direccion_entrega+= Api.ext === undefined?'':' ext '+Api.ext;
        Api.datos.direccion_entrega+= Api.colonia===undefined?'':' '+Api.colonia;
        Api.datos.direccion_entrega+= Api.int===undefined?'':' int '+Api.int;
        Api.datos.direccion_entrega+= Api.delegacion===undefined?'':' '+Api.delegacion;
        Api.datos.direccion_entrega+= Api.ciudad===undefined?'':' '+Api.ciudad;
        Api.datos.direccion_entrega+= Api.cp===undefined?'':' cp '+Api.cp;
        Api.datos.direccion_entrega+= Api.datos.localidad_entrega===undefined?'':' '+Api.datos.localidad_entrega;
    };
    
    $scope.updatePosition = function(){
            $scope.navigate = false;
            if($scope.datos.direccion_entrega===null){                
                $scope.selectFieldsFunction = function(place){
                    for (var i = 0; i < place.address_components.length; i++) {
                        console.log(place.address_components);
                        var addressType = place.address_components[i].types[0];
                        if($scope.componentForm[addressType]){
                            var val = place.address_components[i][$scope.componentForm[addressType]];
                            $scope.datos[$scope.inputForm[addressType]] = val;
                            Api[$scope.inputForm[addressType]] = val;
                        }
                      }
                      $scope.unirDireccion();            
                      if(!$scope.$$phase){
                          $scope.$apply();
                      }
                };
                
                $scope.componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    postal_code: 'short_name',
                    sublocality_level_1:'long_name'
                };

                $scope.inputForm = {
                    street_number: 'ext',
                    route: 'calle',
                    locality: 'delegacion',
                    administrative_area_level_1: 'ciudad',
                    postal_code: 'cp',
                    sublocality_level_1:'colonia'
                };
                
                
                
            $scope.options = { enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);                
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;
                $scope.$apply();    
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma el destino','Si sientes que el GPS no ha encontrado la ubicación, puedes arrastrar el marcador hacia la ubicación exacta');
                UI.getLocationName(c.latitude,c.longitude,function(dir){
                   $scope.selectFieldsFunction(dir);
                   $scope.gps = true;
                });
            },function(e){
                console.log("Error retrieving position " + e.code + " " + e.message);
                alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');
                $scope.gotoLocation(localStorage.lat, localStorage.lon);
            },$scope.options);                                      
            }
            else{
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma el destino','Pulsa sobre alguno de los iconos del mapa para saber cual es la ubicación');
            }
    };
        
     $scope.location = function(value){
        $scope.basel = value;
        if(!$scope.$$phase){
            $scope.$apply("basel");
        }
        if($scope.gps){
            UI.getLocationName(value.lat,value.lon,function(dir){
                $scope.selectFieldsFunction(dir);
                $scope.confirmar();
             });
        }else{
            $scope.confirmar();
        }
    };
    
    $scope.$watch('basel',function(){
       console.log($scope.basel); 
    });
     // check login code
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });
    $scope.validarDistancia = function(){
         //10.514440437420241,  lon=-66.92120273862304
        Math.radians = function(degrees) {
            return degrees * Math.PI / 180;
         };
        var lat_entrega = parseFloat($scope.basel.lat);
        var lon_entrega = parseFloat($scope.basel.lon);
        var lat_recogida = parseFloat(Api.datos.lat);
        var lon_recogida = parseFloat(Api.datos.lon);
        console.log($scope.basel);
        console.log(Api.datos);
        var distancia = (6378.137*Math.acos(Math.cos(Math.radians(lat_entrega))*Math.cos(Math.radians(lat_recogida))*Math.cos(Math.radians(lon_recogida)-Math.radians(lon_entrega))+Math.sin(Math.radians(lat_entrega))*Math.sin(Math.radians(lat_recogida)))/0.62137)*1000;
        if(distancia>Api.ajustes.max_metros_por_envio){
            return false;
        }else{
            return true;
        }
    };
    $scope.confirmar = function(data){
        if($scope.validarDistancia()){
            Api.datos.lat_entrega = $scope.basel.lat;
            Api.datos.lon_entrega = $scope.basel.lon;
            Api.datos.sucursales_id = $scope.user;
            Api.datos.ubicacion_entrega = '('+$scope.basel.lat+','+$scope.basel.lon+')';
            if(!$scope.gps){
                $scope.unirDireccion();
            }            
            $state.go('tab.aceptarprecio');
        }else{
            alert('Has excedido la distancia permitida para realizar envíos. Verifica el punto de entrega y vuelve a intentarlo');
            document.location.href="main.html#/tab/pedirentrega";
        }
    };
    
    $scope.cancelar = function(){
        $state.go('tab.envio');
    };
    
    $scope.lugarEnfocado = 0;
    $scope.lugares = [];
    $scope.navegarlugares = function(lugares){        
        $scope.lugarEnfocado = 0;
        $scope.lugares = lugares;
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng());
        if(lugares.length>1){
            $scope.navigate = true;
            $scope.widthButtonConfirmar = '28%';
            $scope.widthButtonCancelar = '26%';
        }
    };
    $scope.seleccionarIzquierda = function(){
        $scope.lugarEnfocado -= 1;
        if($scope.lugarEnfocado===-1){
            $scope.lugarEnfocado = $scope.lugares.length-1;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    $scope.seleccionarDerecha = function(){
        $scope.lugarEnfocado += 1;
        if($scope.lugarEnfocado>=$scope.lugares.length){
            $scope.lugarEnfocado = 0;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    
    $scope.gotoLocation = function (lat, lon) {            
        if ($scope.lat !== lat || $scope.lon !== lon) {
                $scope.basel = { lat: lat, lon: lon };
        }
     };
     
})

.controller('aceptarprecio', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User){
    $scope = User.getData($scope);
    $scope.confirm = UI.getConfirmBox($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Por favor espere...');
    if(Api.ajustes===undefined || Api.datos===null){
        $state.go('tab.main');
    }else{
        $scope.precio = Api.ajustes.precio_por_envio_sin_contrato;
    }
    $scope.gratis = 0;
    Api.list('sucursales',{id:$scope.user},$scope,$http,function(data){ 
        data = data[0]; 
        if(parseInt(data.envios_sin_costo)>0){
            $scope.precio = 0;
            $scope.gratis = parseInt(data.envios_sin_costo);
            if(!$scope.$$phase){
                $scope.$apply();
            }
        }
    });
    //Tokenizar la tarjeta
    //key_J9gJRGvkAyEiqb7Bnv7dHkw
    Conekta.setPublishableKey('key_dyBakkgJYFje2r8YNC2jsCQ');
    //Conekta.setPublishableKey('key_J9gJRGvkAyEiqb7Bnv7dHkw');
    var fecha = $scope.fecha_vencimiento.split('-');
    $form = {
        card:{
            'name':$scope.nombre_tarjeta,
            'number':$scope.tarjeta,
            'cvc':$scope.cvc,
            'exp_month':fecha[0],
            'exp_year':fecha[1]
        }
    };
    $scope.aceptar = function(){
        if(Api.datos===null || Api.datos===undefined){
            $state.go('tab.main');
        }else{
            if($scope.gratis===0){
                if($scope.nombre_tarjeta==='undefined' || $scope.tarjeta==='undefined' || $scope.cvc==='undefined'){
                        $scope.confirm('No posee tarjetas asociadas a su cuenta','¿Desea asociar una tarjeta ahora?',function(data){
                            $state.go('tab.cuenta');
                        },function(){
                            $state.go('tab.main');
                        });
                }else{
                        $scope.toking = true;
                        Conekta.token.create($form,
                        function(data){
                            Api.datos.card = [data.id];            
                            $scope.insertarPedido();
                        }, 
                        function(data){
                            $scope.showAlert = UI.getShowAlert($ionicPopup);
                            $scope.showAlert('Disculpe pero su transacción no fué aprobada ['+data.message_to_purchaser+']');
                            $scope.toking = false;
                        });
                }
            }else{
                $scope.insertarPedido();
            }
        }
    };
    
    $scope.insertarPedido = function(){
        if(Api.datos===null || Api.datos===undefined){
            $state.go('tab.main');
        }
        else{
            $scope.data = Api.datos;
            if($scope.data.fecha_solicitud===undefined){
                var fecha = new Date();
                var f = fecha.getFullYear()+'-'+(parseInt(fecha.getMonth())+1)+'-'+fecha.getDate()+' '+fecha.getHours()+':'+fecha.getMinutes()+':'+fecha.getSeconds();
                $scope.data.fecha_solicitud = f;
            }
            $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando pedido...');
            Api.insert('pedidos',$scope,$http,function(data){
                Api.datos = null;
                Api.lista = undefined;
                if(Api.programar){
                    document.location.href="#/tab/main";
                }else{
                    document.location.href="#/tab/AsignarRepartidor/"+data.insert_primary_key;
                }
            });
        }
    };
    if(Api.triggerPay!==undefined){
        $scope.aceptar();
        Api.triggerPay = undefined;
    }
    $scope.toking = false;
})

.controller('buscarRepartidor', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {    
    $scope.datos = Api.datos;        
    $scope = User.getData($scope);               
    $scope.loading = UI.getLoadingBox($ionicLoading);
})

.controller('Cuenta', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    var f = $scope.fecha_vencimiento;
    f = f.split('-');    
    $scope.tarjetin = '***********'+$scope.tarjeta.substring(12);
    $scope.datos = {
        id:$scope.user,
        email:$scope.email,
        password:$scope.password,
        contrato:$scope.contrato,
        telefono_fijo:$scope.telefono,
        telefono_celular:$scope.celular,
        nombre_sucursal:$scope.nombre,
        responsable:$scope.responsable,        
        nombre_tarjeta:$scope.nombre_tarjeta,
        cvc:$scope.cvc,
        mes:parseInt(f[0]),
        anio:parseInt(f[1])
    };
    
    $scope.field_types = {
        password:'password'
    };
    
    $scope.send = function(datos){
        if(datos.tarjeta===''){
            datos.tarjeta = $scope.tarjeta;
        }
        $scope.data = datos;
        $scope.data.fecha_vencimiento = $scope.data.mes+'-'+$scope.data.anio;
        Api.update('sucursales',$scope.user,$scope,$http,function(data){
            if(data.success){                    
                $scope.data.id = $scope.user;
                User.setData($scope.data);
                if(Api.datos!==null && Api.datos!==undefined){
                    if($scope.data.nombre_tarjeta!=='undefined' && $scope.data.tarjeta!=='undefined' && $scope.data.cvc!=='undefined'){
                        Api.triggerPay = true;
                        $state.go('tab.aceptarprecio');
                    }else{
                        alert('Para proseguir con el envio debe llenar los datos bancarios');
                    }
                }else{
                    $scope.showAlert = UI.getShowAlert($ionicPopup);
                    $scope.showAlert('Sus datos','Sus datos han sido actualizados con éxito');
                }
            }
        });
    };
})

.controller('EnviosList', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.filtro = 'Todos';
    $scope.refresh = function(){
            Api.list('pedidos',{sucursales_id:$scope.user},$scope,$http,function(data){
                for(var i in data){
                    data[i].statusText = data[i].status==='1'?'Recogiendo':data[i].status==='2'?'En transito':'Completado';
                }
                $scope.lista = data;
                Api.lista = data;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.filter($scope.filtro);
            });
    };
    $scope.refresh();
    
    $scope.filter = function(status){
        $scope.filtro = status;
        $scope.lista = [];
        for(var i=Api.lista.length-1; i>=0; i--){
            if(status==='Todos' || status===Api.lista[i].status){
                $scope.lista.push(Api.lista[i]);
            }
        }
    };
    
})

.controller('EnviosDetail', function($scope,$state,$stateParams,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    
    $scope.asignar = function(){
        document.location.href = '#/tab/AsignarRepartidor/'+$scope.detail.id;
    };
    
    $scope.anular = function(){
        $scope.confirm = UI.getConfirmBox($ionicPopup);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Anulando Pedido');
        $scope.confirm('¿Estas seguro?','¿Estás seguro que deseas anular este pedido?, esta acción no podrá reversarse',function(){
           Api.deleterow/('pedidos',$scope.detail.id,$scope,$http,function(){
               document.location.href="#tab/envios";
           }); 
        });
    };
    
    $scope.consultar = function(){
        if(Api.lista===undefined){
            $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
            Api.list('pedidos',{sucursales_id:$scope.user,status:0},$scope,$http,function(data){
                Api.lista = data;
                for(i in Api.lista){
                    if(Api.lista[i].id===$stateParams.id){
                        $scope.detail = Api.lista[i];
                        $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
                        if(Api.buscarRepartidor){
                            $scope.asignar();
                        }
                    }
                }
                $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
            });
        }
        else{
            for(i in Api.lista){
                if(Api.lista[i].id===$stateParams.id){
                    $scope.detail = Api.lista[i];
                    $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
                }
            }
        }   
    };
    
    $scope.calificar = function(){
        Api.calificar = $scope.detail;  
        $state.go('tab.calificar');
    };
    
    $scope.consultar();
})

.controller('AsignarRepartidor', function($scope,$state,$stateParams,$http,$ionicLoading,$ionicPopup,Api,UI,User,SocketConnection) {
        /**
        * Created by PavelCSS on 13.01.15.
        */
       var time = 2;
       var peopleCount = 20;
       var peoples = [];

       for (i = 0; i < peopleCount; i++) {
           peoples.push({
               distance : Math.floor((Math.random() * 140) + 1),
               angle    : Math.floor((Math.random() * 360) + 1)
           });
       }

       (function radar(){

           var radius = 150;
           for (i = 0; i < peoples.length; i++) {
               var disX = 90 < peoples[i].angle + 90 < 270 ? radius - peoples[i].distance : radius,
                   disY = 180 < peoples[i].angle + 90 < 360 ? radius - peoples[i].distance : radius,
                   angleNew = (peoples[i].angle + 90) * Math.PI / 180,
                   getDegX = disX + peoples[i].distance - Math.round(peoples[i].distance * Math.cos(angleNew)),
                   getDegY = disY + peoples[i].distance - Math.round(peoples[i].distance * Math.sin(angleNew)),
                   delay = time / radius * (peoples[i].distance + 5);

               $('#guides').append($('<span>')
                   .addClass('dot')
                   .css({
                       left : getDegX,
                       top  : getDegY,
                       '-webkit-animation-delay' : delay + 's',
                       'animation-delay' : delay + 's'
                   })
                   .attr({
                       'data-atDeg' : peoples[i].angle
                   }));
             $("#sonar").addClass('animated');
           }
        })();
        
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);    
    $scope.reintentar = false;
    
    Api.respuestaSolicitudRepartidor = function(data){
        $scope.loading('hide');
        if(data.response){
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            var str = '<div> Se ha encontrado un repartidor</div>';
            str+= '<div class="card">';
            str+= '<img src="'+data.data.foto+'" style="width:100%">';
            str+= '<div><b>Nombre: </b>'+data.data.nombre+'</div>';
            str+= '<div><b>Placa: </b>'+data.data.placa+'</div>';
            $1 = parseInt(data.data.rank)>=1?'act':' ';
            $2 = parseInt(data.data.rank)>=2?'act':' ';
            $3 = parseInt(data.data.rank)>=3?'act':' ';
            $4 = parseInt(data.data.rank)>=4?'act':' ';
            $5 = parseInt(data.data.rank)>=5?'act':' ';
            str+= '<div align="center">\n\
            <ul class="rate2">\n\
                <li class="'+$1+'"></li>\n\
                <li class="'+$2+'"></li>\n\
                <li class="'+$3+'"></li>\n\
                <li class="'+$4+'"></li>\n\
                <li class="'+$5+'"></li>\n\
            </ul>\n\
            </div>';
            str+= '</div>';
            $scope.showAlert('Respuesta de asignación',str);               
            Api.lista=undefined;
            //document.location.href="#/tab/pedido/"+$stateParams.id;
            document.location.href="#/tab/main";
        }
        else{
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.showAlert('Respuesta de asignación','Su pedido se atenderá a la brevedad posible…');
            $scope.reintentar = true;
            $state.go('tab.main');
        }
    };
    $scope.asignar = function(){
        
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Repartidor');
        $scope.loading('show');
        if(Api.programar){
            SocketConnection.programar($scope.detail.id,Api.time,function(){
                $scope.loading('hide');
                alert('Se ha programado su envío');
                document.location.href="#/tab/main";
            });
        }else{
            SocketConnection.buscarRepartidor($scope.detail.id,function(){
                $scope.loading('hide');
            });
        }
    };
    
    
    
    $scope.consultar = function(){
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
        Api.list('pedidos',{sucursales_id:$scope.user,status:0},$scope,$http,function(data){
            Api.lista = data;
            for(i in Api.lista){
                if(Api.lista[i].id===$stateParams.id){
                    $scope.detail = Api.lista[i];
                    $scope.asignar();
                }
            }                
        });        
    };
    
    $scope.consultar();
})

.controller('Mensajes', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    Api.list('pedidos',{sucursales_id:$scope.user,'pedidos.repartidores_id != ':'null'},$scope,$http,function(data){
        var d = [];
            for(var i=data.length-1;i>=0;i--){
                d.push(data[i]);
            }
            Api.lista = d;
            $scope.lista = d;
    },'where');
})

.controller('ReadMensajes', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    $scope.position = '0';
    $scope.togglePosition = function(d){
      $scope.position = d;
      if(!$scope.$$phase){
          $scope.$apply();
      }
    };
    $scope.consultar = function(){
        if(Api.lista===undefined){
            Api.list('pedidos',{repartidores_id:$scope.user},$scope,$http,function(data){
                Api.lista = data;
                $scope.lista = data;
            },'where');
        }
        if(Api.mensaje===undefined){
            $scope.data = {'pedido_id':$stateParams.id};
            Api.query('getMensajes',$scope,$http,function(data){
                
                if(typeof(data)!=='object'){
                    data = JSON.parse(JSON.parse(data));
                    $scope.mensajes = data;
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }
                else{
                    $scope.mensajes = [];
                }
            });
        }
    };
    
    $scope.mensaje = '';
    
    $scope.sendMensaje = function(mensaje){
        $scope.mensaje = '';
        var dest = 0;
        for(var k in Api.lista){
            if(Api.lista[k].id===$stateParams.id){
                dest = Api.lista[k].repartidores_id;
            }
        }
        var data = {userId:$scope.user,userName:$scope.nombre,mensaje:mensaje,dest:dest,pedido_id:$stateParams.id};        
        $scope.mensajes.push(data);
        $scope.$emit('sendmensajetoserver',data);
    };
    
    $scope.$on('message_receiver',function(evt,data){
        $scope.mensajes.push(data);
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    
    $scope.consultar();
})

.controller('Calificar', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    Api.list('repartidores',{id:$stateParams.id},$scope,$http,function(data){
        $scope.detail = data[0];
        $scope.foto = 'http://74.208.12.230/pizzasapp/images/repartidores/'+$scope.detail.foto;
        if(!$scope.$$phase){
            $scope.$apply();
        }
    },'where');    
    $scope.datos = {calidad:0};
    $scope.detail = {nombre_repartidor:'Buscando'};
    $scope.rankear = function(datos){
        $scope.data = {calificacion:datos.calidad,repartidores_id:$stateParams.id};
        Api.insert('calificaciones',$scope,$http,function(data){
           $scope.showAlert = UI.getShowAlert($ionicPopup);
           $scope.showAlert('Se ha enviado su calificación con éxito');
           $state.go('tab.main');
        });
    };
})

.controller('Balance', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');    
    $scope.data = {'sucursales_id':$scope.user};
    $scope.detail = {envios_hoy:0,envios_mes:0,costo:0,cobranza:0};
    Api.query('getBalance',$scope,$http,function(data){
        data = data.replace('[]','');
        data = JSON.parse(data);
        $scope.detail = data;        
        console.log(data);
    });
})

.controller('Favoritos', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.add = true;
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    $scope.showAlert = UI.getShowAlert($ionicPopup);        
    $scope.refresh = function(){
        Api.list('direcciones_favoritas',{'sucursales_id':$scope.user},$scope,$http,function(data){        
            $scope.lista = data;
            localStorage.direcciones = JSON.stringify(data);
            $scope.$broadcast('scroll.refreshComplete');
        });
    };        
    $scope.refresh();       
    $scope.clickFav = function(id,i){
        console.log(i);
        document.location.href="#/tab/favoritos/add/1/"+i.id;
    };
    
    Api.addFav = function(){
      if(Api.datos!==undefined){
          $scope.data = {sucursales_id:$scope.user,lat:Api.datos.lat,lon:Api.datos.lon,nombre:Api.datos.direccion,apodo:Api.datos.localidad};
          Api.insert('direcciones_favoritas',$scope,$http,function(data){
              $scope.showAlert('Direccion almacenada con éxito');
              localStorage.direcciones = '[]';
              $scope.refresh();
              $state.go('tab.favoritos');
          });
      }else{
          $state.go('tab.favoritos');
      }
    };
    
    if($stateParams.id!==undefined){
        $scope.basel = {lat:0,lon:0};
        Api.list('direcciones_favoritas',{id:$stateParams.id},$scope,$http,function(data){
            if(data.length>0){
                $scope.detail = data[0];
                $scope.basel = {lat:data[0].lat,lon:data[0].lon};
            }
        });
        
        $scope.eliminar = function(id){
            Api.deleterow('direcciones_favoritas',$stateParams.id,$scope,$http,function(data){
                $scope.showAlert('Direccion Eliminada');
                localStorage.direcciones = '[]';
                $scope.refresh();
                $state.go('tab.favoritos');                
            });
        };
    }
})

.controller('EnviosFav', function($scope,$state,$http,$ionicPlatform,$stateParams,$ionicSideMenuDelegate,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.add = false;
    if(localStorage.direcciones===undefined){
        Api.list('direcciones_favoritas',{'sucursales_id':$scope.user},$scope,$http,function(data){        
            $scope.lista = data;
            localStorage.direcciones = JSON.stringify(data);
        });
    }else{
        $scope.lista = JSON.parse(localStorage.direcciones);
    }
    
    $scope.clickFav = function(id,i){
        Api.datos = {};
        Api.datos.direccion = i.nombre;
        Api.datos.lat = i.lat;
        Api.datos.lon = i.lon;
        Api.datos.sucursales_id = $scope.user;
        Api.datos.ubicacion = '('+i.lat+','+i.lon+')';
        //Se llenan por defectos datos de entrega
        Api.datos.lat_entrega = 0;
        Api.datos.lon_entrega = 0;
        Api.datos.cliente = 'undefined';
        Api.datos.direccion_entrega = 'undefined';
        Api.datos.ubicacion_entrega = "(0,0)";
        var f = new Date();
        Api.datos.fecha_solicitud = f.getFullYear()+'-'+f.getMonth()+'-'+f.getDate()+' '+f.getHours()+':'+f.getMinutes()+':'+f.getSeconds();
        Api.datos.status = 1;
        $state.go('tab.aceptarprecio');
    };
});
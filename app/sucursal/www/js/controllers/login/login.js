angular.module('starter.controllers', ['ngOpenFB'])

.controller('Login', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);       
       $scope.validEnter = function(){
            if(localStorage.email!==undefined){                     
               $scope = User.getData($scope);
               document.location.href="main.html";
            }
       }

       $scope.login = function(param){
            $scope.email = param.email;
            $scope.password = param.password;
            $scope.isRegister();
       }

       $scope.isRegister = function(){
           data =  {email:$scope.email,password:$scope.password}; 
           console.log(data);
           Api.list('sucursales',data,$scope,$http,function(data){
                 if(data.length==0){                   
                    $scope.showAlert('Inicio de sesión','Email o Contraseña Incorrecta');
                 }
                 else{
                       data = data[0];
                       User.setData(data);
                       document.location.href="main.html";
                 }
           },'where');         
       }
       $scope.validEnter();
})

.controller('Registrar', function($scope,$state,$http,$ionicLoading,$ionicPopup,$ionicModal,User,Api,UI,ngFB) {
            $scope.loading = UI.getLoadingBox($ionicLoading);       
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.modal = UI.getModalBox($ionicModal,'templates/modals/condiciones.html',$scope);
            $scope.data = {};
            $scope.fbLogin = function () {
            ngFB.login({scope: 'email,user_location'}).then(
                function (response) {
                    if (response.status === 'connected') {                        
                        $scope.getInfo();
                    } else {
                        alert('Facebook login failed');
                    }
                });
            };
            
            $scope.getInfo = function() {
                ngFB.api({path: '/me',params: {fields: 'id,name,location,email'}}).then(
                    function(user) {
                        console.log(JSON.stringify(user));
                        $scope.data = {email:user.email,responsable:user.name,calle:user.location.name};
                        if(!$scope.$$phase()){
                            $scope.$apply();
                        }
                    },
                    function(e){
                        console.log(e);
                    });
            }
        
            $scope.personales = function(data){
              if(data!=undefined){
                 
                 if(data.ciudad!=undefined && data.condiciones){
                    data.direccion = data.ciudad+' '+data.calle;
                    Api.data = data;
                    Api.data.condiciones = null;
                    $state.go('tab.registrar3');
                 }
                 else{
                     $scope.showAlert('Debe indicar sus datos personales para continuar');
                 }
              }  
              else{
                    $scope.showAlert('Debe indicar sus datos personales para continuar');
              }
            };
        if(Api.data!=undefined){
            $scope.finanza = function(data){
              Api.data.tarjeta = data.tarjeta;
              Api.data.nombre_tarjeta = data.nombre_tarjeta;
              Api.data.cvc = data.cvc;
              Api.data.fecha_vencimiento = data.mes+'-'+data.anio;
              Api.data.anio = null;
              Api.data.mes = null;
              $scope.registro();
            };

            $scope.registro = function(){
                $scope.data = Api.data;
                $scope.data.ubicacion = '('+$scope.basel.lat+','+$scope.basel.lon+')';
                Api.insert('sucursales',$scope,$http,function(data){
                  if(data.success){
                      $scope.showAlert('Sus datos han sido guardados');
                      $scope.data.id = data.insert_primary_key;
                      User.setData($scope.data);
                      document.location.href="main.html";
                  }
                });
            };

            $scope.basel = { lat: 47.55633987116614, lon: 7.576619513223015 };
            $scope.location = function(coords){
                Api.data.lat = coords.lat;
                Api.data.lon = coords.lon;
                Api.data.ubicacion = '('+coords.lat+','+coords.lon+')';
                $state.go('tab.registrar3');
            };                        
            $scope.datos = Api.data;
       }
       $scope.mostrarCondiciones = function(data){
            if(data){
                $scope.toggleModal('show');
            }
        };
})

.controller('Recover', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);
       
       $scope.login = function(data){
            $scope.data = {table:'sucursales',email:data.email};
            Api.query('recover',$scope,$http,function(data){
                alert('Su contraseña ha sido reseteada, por favor verifique su correo para continuar con el proceso de activación');
                document.location.href="index.html";
            });
       };
});

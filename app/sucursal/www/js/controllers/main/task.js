controllers.controller('Task', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    if(typeof(Api.datos)!=='undefined'){
        if(typeof(Api.datos.task)!=='undefined'){
            Api.innerHeight = window.innerHeight;
            $scope.task = Api.datos.task;
            console.log(Api.datos.task);
            $scope.groups = [];
            for(var i=0; i<Api.datos.task.length;i++){
                var d = Api.datos.task[i];
                if(typeof(d.lon)==='undefined'){
                    d.id = parseInt(i);
                    $scope.groups.push(d);
                }
            }
        }else{
            document.location.href="#/tab/main";
        }
    }else{
        document.location.href="#/tab/main";
    }
    
    $scope.continuar = function(task){
        var err = false;
        for(var i in task){
            if(typeof(task[i].lat)==='undefined' && (task[i].localidad===undefined || task[i].localidad==='' || task[i].colonia===undefined || task[i].colonia==='')){
                err = true;
            }
        }
        if(err){
            alert('Faltaron datos por llenar como la localidad o la colonia en alguno de los datos');
        }else{
            //Vamos al mapa
            Api.datos.task = task;
            console.log(JSON.stringify(Api.datos));
            console.log(task);
            document.location.href="#/tab/mapa";
        }
    };
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
});
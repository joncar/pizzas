controllers.controller('Cuenta', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    
    var f = $scope.fecha_vencimiento;
    f = f.split('-');    
    $scope.tarjetin = '***********'+$scope.tarjeta.substring(12);
    $scope.datos = {
        id:$scope.user,
        email:$scope.email,
        password:$scope.password,
        contrato:$scope.contrato,
        telefono_fijo:$scope.telefono,
        telefono_celular:$scope.celular,
        nombre_sucursal:$scope.nombre,
        responsable:$scope.responsable,        
        nombre_tarjeta:$scope.nombre_tarjeta,
        cvc:$scope.cvc,
        mes:parseInt(f[0]),
        anio:parseInt(f[1]),
        private_key:$scope.private_key!=='undefined'?$scope.private_key:'',
        public_key:$scope.public_key!=='undefined'?$scope.public_key:'',
        banco_deposito:$scope.banco_deposito,
        tarjeta_debito:$scope.tarjeta_debito,
        porcentaje_sucursal:$scope.porcentaje_sucursal,
        porcentaje_cliente:$scope.porcentaje_cliente
    };
    
    $scope.field_types = {
        password:'password'
    };
    
    $scope.send = function(datos){
        if(datos.tarjeta===''){
            datos.tarjeta = $scope.tarjeta;
        }
        $scope.data = datos;
        $scope.data.fecha_vencimiento = $scope.data.mes+'-'+$scope.data.anio;
        Api.update('sucursales',$scope.user,$scope,$http,function(data){
            if(data.success){                    
                $scope.data.id = $scope.user;
                User.setData($scope.data);
                if(Api.datos!==null && Api.datos!==undefined){
                    if($scope.data.nombre_tarjeta!=='undefined' && $scope.data.tarjeta!=='undefined' && $scope.data.cvc!=='undefined'){
                        Api.triggerPay = true;
                        $state.go('tab.aceptarprecio');
                    }else{
                        alert('Para proseguir con el envio debe llenar los datos bancarios');
                    }
                }else{
                    $scope.showAlert = UI.getShowAlert($ionicPopup);
                    $scope.showAlert('Sus datos','Sus datos han sido actualizados con éxito');
                }
            }
        });
    };
    
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
});

controllers.controller('Balance', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');    
    $scope.data = {'sucursales_id':$scope.user};
    $scope.detail = {envios_hoy:0,envios_mes:0,costo:0,cobranza:0};
    Api.query('getBalance',$scope,$http,function(data){
        data = data.replace('[]','');
        data = JSON.parse(data);
        $scope.detail = data;
        $scope.detail.envios_hoy = parseInt($scope.detail.pedidos_hoy)-parseInt($scope.detail.solicitudes_hoy);
        $scope.detail.envios_mes = parseInt($scope.detail.pedidos_mes)-parseInt($scope.detail.solicitudes_mes);
        console.log(data);
    });
});
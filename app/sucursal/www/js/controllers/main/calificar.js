controllers.controller('Calificar', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    Api.list('repartidores',{id:$stateParams.id},$scope,$http,function(data){
        $scope.detail = data[0];
        $scope.foto = 'http://74.208.12.230/pizzasapp/images/repartidores/'+$scope.detail.foto;
        if(!$scope.$$phase){
            $scope.$apply();
        }
    },'where');    
    $scope.datos = {calidad:0};
    $scope.detail = {nombre_repartidor:'Buscando'};
    $scope.rankear = function(datos){
        $scope.data = {calificacion:datos.calidad,repartidores_id:$stateParams.id};
        Api.insert('calificaciones',$scope,$http,function(data){
           $scope.showAlert = UI.getShowAlert($ionicPopup);
           $scope.showAlert('Se ha enviado su calificación con éxito');
           $state.go('tab.main');
        });
    };
});
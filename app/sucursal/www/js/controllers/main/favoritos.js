controllers.controller('Favoritos', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.add = true;    
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    $scope.showAlert = UI.getShowAlert($ionicPopup);        
    $scope.refresh = function(){
        Api.list('direcciones_favoritas',{'sucursales_id':$scope.user},$scope,$http,function(data){        
            $scope.lista = data;
            localStorage.direcciones = JSON.stringify(data);
            $scope.$broadcast('scroll.refreshComplete');
        });
    };        
    $scope.refresh();       
    $scope.clickFav = function(id,i){
        console.log(i);
        document.location.href="#/tab/favoritos/add/1/"+i.id;
    };
    
    Api.addFav = function(){
      if(Api.datos!==undefined){
          $scope.data = {sucursales_id:$scope.user,lat:Api.datos.lat,lon:Api.datos.lon,nombre:Api.datos.direccion,apodo:Api.datos.localidad};
          Api.insert('direcciones_favoritas',$scope,$http,function(data){
              $scope.showAlert('Direccion almacenada con éxito');
              localStorage.direcciones = '[]';
              $scope.refresh();
              $state.go('tab.favoritos');
          });
      }else{
          $state.go('tab.favoritos');
      }
    };
    
    if($stateParams.id!==undefined){
        $scope.basel = {lat:0,lon:0};
        Api.list('direcciones_favoritas',{id:$stateParams.id},$scope,$http,function(data){
            if(data.length>0){
                $scope.detail = data[0];
                $scope.basel = {lat:data[0].lat,lon:data[0].lon};
            }
        });
        
        $scope.eliminar = function(id){
            Api.deleterow('direcciones_favoritas',$stateParams.id,$scope,$http,function(data){
                $scope.showAlert('Direccion Eliminada');
                localStorage.direcciones = '[]';
                $scope.refresh();
                $state.go('tab.favoritos');                
            });
        };
    }        
});

controllers.controller('EnviosFav', function($scope,$state,$http,$ionicPlatform,$stateParams,$ionicSideMenuDelegate,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.add = false;
    if(localStorage.direcciones===undefined){
        Api.list('direcciones_favoritas',{'sucursales_id':$scope.user},$scope,$http,function(data){        
            $scope.lista = data;
            localStorage.direcciones = JSON.stringify(data);
        });
    }else{
        $scope.lista = JSON.parse(localStorage.direcciones);
    }
    
    $scope.clickFav = function(id,i){
        Api.datos = {};
        Api.datos.direccion = i.nombre;
        Api.datos.lat = i.lat;
        Api.datos.lon = i.lon;
        Api.datos.sucursales_id = $scope.user;
        Api.datos.ubicacion = '('+i.lat+','+i.lon+')';
        //Se llenan por defectos datos de entrega
        Api.datos.lat_entrega = 0;
        Api.datos.lon_entrega = 0;
        Api.datos.cliente = 'undefined';
        Api.datos.direccion_entrega = 'undefined';
        Api.datos.ubicacion_entrega = "(0,0)";
        var f = new Date();
        Api.datos.fecha_solicitud = f.getFullYear()+'-'+f.getMonth()+'-'+f.getDate()+' '+f.getHours()+':'+f.getMinutes()+':'+f.getSeconds();
        Api.datos.status = 1;
        $state.go('tab.aceptarprecio');
    };
    
    $scope.selFavMainSend = function(id,datos){
      if(typeof(Api.selFavMainSend)!=='undefined'){
        Api.selFavMainSend(id,datos);
      }
    };
    
    $scope.selFavMainSendFav = function(id,datos){
      if(typeof(Api.selFavMainSendFav)!=='undefined'){
        Api.selFavMainSendFav(id,datos);        
      }
    };
});
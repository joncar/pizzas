controllers.controller('Envios', function($scope,$state,$http,$ionicPlatform,$stateParams,$ionicSideMenuDelegate,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);    
    $scope.modal = UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope);
    $scope.buscada = '0';
    Api.innerHeight = window.innerHeight;
    var f = new Date();
    var fecha_solicitud = f.getFullYear()+'-'+f.getMonth()+'-'+f.getDate()+' '+f.getHours()+':'+f.getMinutes()+':'+f.getSeconds();
    $scope.datos = {ownposition:true,fecha_solicitud:fecha_solicitud,ciudad:'',delegacion:'',ext:''};
    $scope.d = {fechas:'',hora:''};
    $scope.closeProgramar = function(){
        $scope.toggleModal('hide');
    };
    $scope.toggleProgramar = function(datos){
        if(datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition){            
            $scope.datos = datos;
            if(!$scope.$$phase){
                $scope.$apply();
            }
            $scope.toggleModal('show');
        }else{
            alert('Debe llenar el campo Int antes de continuar');
        }
    };
    
    $scope.selectFecha = function(datos,d){
        datos.fecha_solicitud = d.fechas.getFullYear()+'-'+d.fechas.getMonth()+'-'+d.fechas.getDate()+' '+d.hora.getHours()+':'+d.hora.getMinutes()+':'+d.hora.getSeconds();
        d.fechas.setHours(d.hora.getHours(), d.hora.getMinutes(), d.hora.getSeconds(), 0);
        var fecha = new Date();
        if(fecha.getTime()>=d.fechas.getTime()){
            alert('Debe asignar una fecha con un intervalo mayor a 2 horas como mínimo');
        }else{            
            var m = (d.fechas.getTime()-fecha.getTime())/1000;
            if(m<=7200){
                alert('Debe asignar una fecha con un intervalo mayor a 2 horas como mínimo');
            }else{
                $scope.toggleModal('hide');
                $scope.modal = UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope);
                Api.time = m;
                $scope.enviarPedido(datos,true);
            }
        }
    };
    
   $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
      };
      
      $scope.inputForm = {
        street_number: 'ext',
        route: 'calle',
        locality: 'delegacion',
        administrative_area_level_1: 'ciudad',
        postal_code: 'cp',
        sublocality_level_1:'colonia'
      };
    
    $scope.selectFieldsFunction = function(place){
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                $scope.datos[$scope.inputForm[addressType]] = val;
            }
          }
          $scope.datos.direccion = place.formatted_address;
          $scope.buscada = '1';
          if(!$scope.$$phase){
              $scope.$apply();
          }
    };
    
    $scope.enviarPedido = function(datos,programar){
        if(datos.localidad!==undefined && datos.localidad!=='' && (datos.colonia!==undefined && datos.colonia!=='' || datos.ownposition)){
            datos.direccion = datos.direccion===undefined?null:datos.direccion;        
            Api.datos = datos;
            Api.programar = programar;
            if($stateParams.fav===undefined){             
             $state.go('confirmar');
            }else{
                document.location.href="#/confirmar/1";
            }            
        }else{
            alert('Debe llenar el campo localidad antes de continuar');
        }
    };
            
    $scope.disabletap = function(){
            
        container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        angular.element(container).css('pointer-events', 'auto');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
            document.getElementById('autocompletegoogleplace').blur();
        });
    };
});

controllers.controller('Confirmar', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User,$stateParams) {
    $scope.datos = Api.datos;
    $scope.direccion = Api.datos.direccion;
    $scope.basel = { lat: localStorage.lat, lon: localStorage.lon };
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando Pedido');
    $scope.gps = false;
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/fooddeliveryservice.png';
    $scope.navigate = false;
    $scope.widthButtonConfirmar = '50%';
    $scope.widthButtonCancelar = '48%';
    $scope.innerheight = Api.innerHeight+'px';
    $scope.unirDireccion = function(){
        Api.datos.direccion = Api.datos.calle===undefined?'':' '+Api.datos.calle;
        Api.datos.direccion+= Api.datos.ext === undefined?'':' ext '+Api.datos.ext;
        Api.datos.direccion+= Api.datos.colonia===undefined?'':' '+Api.datos.colonia;
        Api.datos.direccion+= Api.datos.int===undefined?'':' int '+Api.datos.int;
        Api.datos.direccion+= Api.datos.delegacion===undefined?'':' '+Api.datos.delegacion;
        Api.datos.direccion+= Api.datos.ciudad===undefined?'':' '+Api.datos.ciudad;
        Api.datos.direccion+= Api.datos.cp===undefined?'':' cp '+Api.datos.cp;
        Api.datos.direccion+= Api.datos.localidad===undefined?'':' '+Api.datos.localidad;
    };
    
    $scope.updatePosition = function(){
            if($scope.datos.direccion===null){
            $scope.selectFieldsFunction = function(place){
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if($scope.componentForm[addressType]){
                            var val = place.address_components[i][$scope.componentForm[addressType]];
                            $scope.datos[$scope.inputForm[addressType]] = val;
                            Api.datos[$scope.inputForm[addressType]] = val;
                        }
                      }
                      $scope.unirDireccion();
                      if(!$scope.$$phase){
                          $scope.$apply();
                      }
                };
                
                $scope.componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    postal_code: 'short_name',
                    sublocality_level_1:'long_name'
                };

                $scope.inputForm = {
                    street_number: 'ext',
                    route: 'calle',
                    locality: 'delegacion',
                    administrative_area_level_1: 'ciudad',
                    postal_code: 'cp',
                    sublocality_level_1:'colonia'
                };
                
    
            $scope.options = { enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;
                $scope.$apply();                   
      
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma tu ubicación','Si sientes que el GPS no ha encontrado tu ubicación, puedes arrastrar el marcador hacia tu ubicación exacta');
                UI.getLocationName(c.latitude,c.longitude,function(dir){
                   $scope.selectFieldsFunction(dir);
                   $scope.gps = true;
                });                
             },function(e){
                 alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');
                 $scope.gotoLocation(localStorage.lat, localStorage.lon);
             },$scope.options);                                      
            }
            else{
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma tu ubicación','Pulsa sobre alguno de los iconos del mapa para saber cual es tu ubicación');
            }
    };
        
    $scope.location = function(value){
        $scope.basel = value;
        if(!$scope.$$phase){
            $scope.$apply("basel");
        }
        if($scope.gps){
            UI.getLocationName(value.lat,value.lon,function(dir){
                $scope.selectFieldsFunction(dir);
                $scope.confirmar();
             });
        }else{
            $scope.confirmar();
        }
    };
    
     // check login code
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });        
    
    $scope.confirmar = function(){
        Api.datos.lat = $scope.basel.lat;
        Api.datos.lon = $scope.basel.lon;
        Api.datos.sucursales_id = $scope.user;
        Api.datos.ubicacion = '('+$scope.basel.lat+','+$scope.basel.lon+')';
        if(!$scope.gps){
            $scope.unirDireccion();
            console.log(Api.datos.direccion);
        }
        Api.datos.fecha_solicitud = $scope.fecha_solicitud;        
        Api.datos.status = 1;
        //Solicitar datos de entrega
        if($stateParams.fav===undefined){
            $state.go('tab.pedirentrega');
        }else{
            Api.addFav();
        }
    };
    
    $scope.cancelar = function(){
        if($stateParams.fav===undefined){
            $state.go('tab.envio');
        }else{
            $state.go('tab.favoritos');
        }        
    };
    
    $scope.lugarEnfocado = 0;
    $scope.lugares = [];
    $scope.navegarlugares = function(lugares){        
        $scope.lugarEnfocado = 0;
        $scope.lugares = lugares;
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng());
        if(lugares.length>1){
            $scope.navigate = true;
            $scope.widthButtonConfirmar = '28%';
            $scope.widthButtonCancelar = '26%';
        }
    };
    $scope.seleccionarIzquierda = function(){
        $scope.lugarEnfocado -= 1;
        if($scope.lugarEnfocado===-1){
            $scope.lugarEnfocado = $scope.lugares.length-1;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    $scope.seleccionarDerecha = function(){
        $scope.lugarEnfocado += 1;
        if($scope.lugarEnfocado>=$scope.lugares.length){
            $scope.lugarEnfocado = 0;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    
    $scope.gotoLocation = function (lat, lon) {            
        if ($scope.lat !== lat || $scope.lon !== lon) {
                $scope.basel = { lat: lat, lon: lon };
        }
     };
});

controllers.controller('EnviosList', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.filtro = 'Todos';
    $scope.refresh = function(){
            Api.list('pedidos',{sucursales_id:$scope.user},$scope,$http,function(data){
                for(var i in data){
                    data[i].statusText = data[i].status==='-2'?'Pago declinado':data[i].status==='1'?'Recogiendo':data[i].status==='2'?'En transito':'Completado';
                }
                $scope.lista = data;
                Api.lista = data;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.filter($scope.filtro);
            });
    };
    $scope.refresh();
    
    $scope.filter = function(status){
        $scope.filtro = status;
        $scope.lista = [];
        for(var i=Api.lista.length-1; i>=0; i--){
            if(status==='Todos' || status===Api.lista[i].status){
                $scope.lista.push(Api.lista[i]);
            }
        }
    };
    
});

controllers.controller('EnviosDetail', function($scope,$state,$stateParams,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    
    $scope.asignar = function(){
        document.location.href = '#/tab/AsignarRepartidor/'+$scope.detail.id;
    };
    
    $scope.anular = function(){
        $scope.confirm = UI.getConfirmBox($ionicPopup);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Anulando Pedido');
        $scope.confirm('¿Estas seguro?','¿Estás seguro que deseas anular este pedido?, esta acción no podrá reversarse',function(){
           Api.deleterow/('pedidos',$scope.detail.id,$scope,$http,function(){
               document.location.href="#tab/envios";
           }); 
        });
    };
    
    $scope.consultar = function(){
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
        Api.list('pedidos',{id:$stateParams.id},$scope,$http,function(data){
            Api.lista = data;
            for(var i in Api.lista){
                if(Api.lista[i].id===$stateParams.id){
                    $scope.detail = Api.lista[i];
                    $scope.detail.trayecto = JSON.parse($scope.detail.trayecto);                    
                    $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
                    $scope.$broadcast('scroll.refreshComplete');
                    if(Api.buscarRepartidor){
                        $scope.asignar();
                    }
                    $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
                }
            }            
        });                   
    };
    
    $scope.calificar = function(){
        Api.calificar = $scope.detail;  
        $state.go('tab.calificar');
    };
    
    $scope.consultar();
});
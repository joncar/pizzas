controllers.controller('Socket', function($scope,$state,UI,$ionicPopup,$ionicSideMenuDelegate,$ionicPlatform,$ionicLoading,User,SocketConnection,$http,Api) {
    $scope = User.getData($scope);
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    SocketConnection.WebSocketOpen($scope);
    SocketConnection.connectID($scope.user);
    $scope.showAlert = UI.getShowAlert($ionicPopup);    
    
    ws.on('respuestaSolicitud',function(data){
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Respuesta de solicitud.',
                    text:'Haz recibido una respuesta de tu solicitud.'
                });
                //Api.notificationSound.play();
                navigator.vibrate(1000);
        }
        console.log(data);
            if(data.response){
                var str = '<div> Se ha encontrado un repartidor</div>';
                str+= '<div class="card">';
                str+= '<img src="'+data.data.foto+'" style="width:100%">';
                str+= '<div><b>Nombre: </b>'+data.data.nombre+'</div>';
                str+= '<div><b>Placa: </b>'+data.data.placa+'</div>';
                str+= '<div><b>Tiempo estimado de arribo: </b>'+data.tiempoDeRespuesta+' min</div>';
                $1 = parseInt(data.data.rank)>=1?'act':' ';
                $2 = parseInt(data.data.rank)>=2?'act':' ';
                $3 = parseInt(data.data.rank)>=3?'act':' ';
                $4 = parseInt(data.data.rank)>=4?'act':' ';
                $5 = parseInt(data.data.rank)>=5?'act':' ';
                str+= '<div align="center">\n\
                <ul class="rate2">\n\
                    <li class="'+$1+'"></li>\n\
                    <li class="'+$2+'"></li>\n\
                    <li class="'+$3+'"></li>\n\
                    <li class="'+$4+'"></li>\n\
                    <li class="'+$5+'"></li>\n\
                </ul>\n\
                </div>';
                str+= '</div>';
                $scope.showAlert('Respuesta de asignación',str);               
                Api.lista=undefined;
                document.location.href="#/tab/main";
            }
            else{
                alert('Los repartidores están en tránsito, por su seguridad les notificamos del envío cuando se encuentran detenidos, pronto le mostraremos al repartidor asignado');
                $scope.reintentar = true;
                //$state.go('tab.main');
            }
        });

        ws.on('message_receiver',function(data){
            if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                    cordova.plugins.backgroundMode.configure({
                        title:'Mensaje recibido.',
                        text:'Te han enviado un mensaje.'
                    });
                    //Api.notificationSound.play();
                    navigator.vibrate(1000);
            }

           if($state.current.name==='tab.mensajesRead'){
               $scope.$broadcast('message_receiver',data);
           }
           else{            
                myPopup = $ionicPopup.show({
                    template: data.mensaje,
                    title: 'Haz recibido un mensaje',
                    subTitle: 'Nro. Pedido: '+data.pedido_id,
                    scope: $scope,
                    buttons: [
                        {
                          text: '<b>Cerrar</b>',              
                          onTap: function(e) {                        
                              myPopup.close();
                              myPopup.close = undefined;
                          }
                        },
                        {
                          text: '<b>Ver Mensaje</b>',
                          onTap: function(e) {                        
                            document.location.href="#/tab/mensajes/"+data.pedido_id;                        
                          }
                        }
                      ]
              });
           }
        });

        $scope.$on('sendmensajetoserver',function(evt,data){
            ws.emit('sendMessage',data);
        });    

        ws.on('finishEntrega',function(data){
            console.log(data);
            if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                    cordova.plugins.backgroundMode.configure({
                        title:'Entrega finalizada.',
                        text:'Se ha realizado una entrega.'
                    });
                    //Api.notificationSound.play();
                    navigator.vibrate(1000);
            }
            if($state.current.name!=='tab.envioDetail'){
                $scope.myPopup = $ionicPopup.show({
                    template: data.mensaje,
                    title: ' El pedido #'+data.pedidos_id,
                    subTitle: 'ha sido entregado con éxito.',
                    scope: $scope,
                    buttons: [
                        {
                          text: '<b>Cerrar</b>',              
                          onTap: function(e) {                        
                              $scope.myPopup.close();
                          }
                        },
                        {
                          text: '<b>Ver Pedido</b>',
                          onTap: function(e) {                        
                            document.location.href="#/tab/pedido/"+data.pedidos_id;                        
                          }
                        }
                    ]
                });
            }
        });

        ws.on('rankearRepartidor',function(data){

            if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                    cordova.plugins.backgroundMode.configure({
                        title:'Rankea a tu repartidor.',
                        text:'Como te ha ido con tu repartidor?.'
                    });
                    //Api.notificationSound.play();
                    navigator.vibrate(1000);
            }
            if($state.current.name!=='tab.calificar'){
                var myPopup = $ionicPopup.show({
                    template: data.mensaje,
                    title: '¿Como fue la entrega del repartidor?',
                    subTitle: 'Danos tu opinión sobre la entrega de tu envío #'+data.idPedido,
                    scope: $scope,
                    buttons: [
                        {
                          text: '<b>Cerrar</b>',              
                          onTap: function(e) {                        
                              myPopup.close();
                          }
                        },
                        {
                          text: '<b>Calificar</b>',
                          onTap: function(e) {                        
                            document.location.href="#/tab/calificar/"+data.repartidores_id;                        
                          }
                        }
                    ]
                });
            }
        });
        
        $scope.$on('buscarRepartidor',function(evt,idPedido){
           ws.emit('buscarRepartidor',{idPedido:idPedido}); 
        });
        
        $scope.$on('notificarAsignacion',function(evt,datos){            
            data = {                
                repartidor:datos.repartidores_id,
                idPedido:datos.id
            };        
            ws.emit('webBuscarRepartidor',data);            
            
        });                
        
        $scope.$on('programar',function(evt,idPedido,time){
           ws.emit('programar',{idPedido:idPedido,time:time}); 
        });
        
        $scope.auto = function(el){
        console.log(el);
        };
        $scope = User.getData($scope);
        if(localStorage.lat===undefined){
            localStorage.lat = '47.55633987116614';
            localStorage.lon = '7.576619513223015';
        }
        if(localStorage.user===undefined){
            document.location.href="index.html";
        }        

        $scope.showMenu = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };

        $scope.desconectar = function(){
            User.cleanData();
            document.location.href="index.html";
            navigator.app.exitApp();
        };    

        //Background
        // Android customization        

        $ionicPlatform.ready(function(){
            //Existe conexión?
            if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $state.go('tab.disconected');
                }
            }                
            Api.list('ajustes',{},$scope,$http,function(data){ 
                Api.ajustes = data[0]; 
            });                
            if(typeof(cordova)!=='undefined'){
                cordova.plugins.backgroundMode.setDefaults({ title:'DeliveryMix', text:'Sucursal conectada'});
                // Enable background mode
                cordova.plugins.backgroundMode.enable();
                cordova.plugins.backgroundMode.onactivate = function () {
                    cordova.plugins.backgroundMode.configure({
                        title:'App Activo',
                        text:'App Activa'
                    });
                };
                if(ionic.Platform.isIOS()){
                    //Api.notificationSound = new Media('sounds/notificacion.mp3',function(){},function(e){alert("No se ha podido ubicar el archivo notification.mp3= "+e);});
                }else{
                    //Api.notificationSound = new Media('/android_asset/www/sounds/notificacion.mp3',function(){},function(e){alert("No se ha podido ubicar el archivo notification.mp3= "+e);});
                }
                //Api.notificationSound.play();               
            }  
            
            if(typeof(window.PushNotification)!=='undefined'){
                $scope.loading = UI.getLoadingBox($ionicLoading,'Registrando ID de app');
                var push = window.PushNotification.init({
                    android: {
                        senderID: "475772788803"
                    },
                    ios: {
                        alert: "true",
                        badge: true,
                        sound: 'false'
                    },
                    windows: {}
                });

                push.on('registration', function(data) {
                    if(localStorage.gcm!==data.registrationId){
                        $scope.data = {gcm:data.registrationId};
                        Api.update('sucursales',$scope.user,$scope,$http,function(data){
                            localStorage.gcm = $scope.data.gcm;
                        });
                    }
                });

                push.on('notification', function(data) {
                    if(typeof($scope.alertPopup)==='undefined'){
                        $scope.alertPopup = $ionicPopup.alert({
                          title: data.title,
                          template: data.message
                        });
                        $scope.alertPopup.then(function(res) {
                            $scope.alertPopup.close();
                            $scope.alertPopup = undefined;
                        });
                    }
                });

                push.on('error', function(e) {
                    alert( e.message);
                });
            }
            
        });
});
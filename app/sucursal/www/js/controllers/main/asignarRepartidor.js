controllers.controller('AsignarRepartidor', function($scope,$rootScope,$state,$stateParams,$http,$ionicLoading,$ionicModal,$ionicPopup,Api,UI,User) {
        /**
        * Created by PavelCSS on 13.01.15.
        */
       var time = 2;
       var peopleCount = 20;
       var peoples = [];

       for (i = 0; i < peopleCount; i++) {
           peoples.push({
               distance : Math.floor((Math.random() * 140) + 1),
               angle    : Math.floor((Math.random() * 360) + 1)
           });
       }

       (function radar(){

           var radius = 150;
           for (i = 0; i < peoples.length; i++) {
               var disX = 90 < peoples[i].angle + 90 < 270 ? radius - peoples[i].distance : radius,
                   disY = 180 < peoples[i].angle + 90 < 360 ? radius - peoples[i].distance : radius,
                   angleNew = (peoples[i].angle + 90) * Math.PI / 180,
                   getDegX = disX + peoples[i].distance - Math.round(peoples[i].distance * Math.cos(angleNew)),
                   getDegY = disY + peoples[i].distance - Math.round(peoples[i].distance * Math.sin(angleNew)),
                   delay = time / radius * (peoples[i].distance + 5);

               $('#guides').append($('<span>')
                   .addClass('dot')
                   .css({
                       left : getDegX,
                       top  : getDegY,
                       '-webkit-animation-delay' : delay + 's',
                       'animation-delay' : delay + 's'
                   })
                   .attr({
                       'data-atDeg' : peoples[i].angle
                   }));
             $("#sonar").addClass('animated');
           }
        })();
        
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);    
    $scope.reintentar = false;
    
    Api.respuestaSolicitudRepartidor = function(data){
        $scope.loading('hide');
        if(data.response){
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            var str = '<div> Se ha encontrado un repartidor</div>';
            str+= '<div class="card">';
            str+= '<img src="'+data.data.foto+'" style="width:100%">';
            str+= '<div><b>Nombre: </b>'+data.data.nombre+'</div>';
            str+= '<div><b>Placa: </b>'+data.data.placa+'</div>';
            $1 = parseInt(data.data.rank)>=1?'act':' ';
            $2 = parseInt(data.data.rank)>=2?'act':' ';
            $3 = parseInt(data.data.rank)>=3?'act':' ';
            $4 = parseInt(data.data.rank)>=4?'act':' ';
            $5 = parseInt(data.data.rank)>=5?'act':' ';
            str+= '<div align="center">\n\
            <ul class="rate2">\n\
                <li class="'+$1+'"></li>\n\
                <li class="'+$2+'"></li>\n\
                <li class="'+$3+'"></li>\n\
                <li class="'+$4+'"></li>\n\
                <li class="'+$5+'"></li>\n\
            </ul>\n\
            </div>';
            str+= '</div>';
            $scope.showAlert('Respuesta de asignación',str);               
            Api.lista=undefined;
            document.location.href="#/tab/main";
        }
        else{
            alert('Su pedido se atenderá a la brevedad posible…');
            $scope.reintentar = true;
            //$state.go('tab.main');
        }
    };
    $scope.asignar = function(id,repartidor){
        console.log(id);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Repartidor');
        //$scope.loading('show');
        if(Api.programar){
            $rootScope.broadcast('programar',$scope.detail.id,Api.time,function(){
                //$scope.loading('hide');
                alert('Se ha programado su envío');
                document.location.href="#/tab/main";
            });
        }else{            
            if(id==='0'){                
                //automatico
                $scope.modal.hide();
                $rootScope.$broadcast('buscarRepartidor',$scope.detail.id,function(){});
                setTimeout(function(){document.location.href="#/tab/main";},5000);
            }else{
                //Manual
                $scope.data = {'repartidores_id':id,'status':1};
                Api.update('pedidos',$scope.detail.id,$scope,$http,function(data){
                    $scope.modal.hide();
                    $scope.data.id = $scope.detail.id;
                    $scope.data.repartidor = repartidor;
                    $rootScope.$broadcast('notificarAsignacion',$scope.data,function(){});
                    document.location.href="#/tab/main";
                });
            }
        }
    };
    
    
    $scope.mostrarRepartidores = function(){
        Api.list('getRepartidoresActivos',{'status':1},$scope,$http,function(data){
            $scope.repartidores = data;
            UI.getModalBox($ionicModal,'templates/modals/asignarRepartidor.html',$scope,function(){$scope.modal.show();});
        });
    };
    
    
    $scope.consultar = function(){
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
        Api.list('pedidos',{id:$stateParams.id,'pedidos.status':1},$scope,$http,function(data){
            Api.lista = data;
            for(i in Api.lista){
                if(Api.lista[i].id===$stateParams.id){
                    $scope.detail = Api.lista[i];
                    if(Api.programar){
                        $scope.asignar('0');
                    }else{
                        $scope.mostrarRepartidores();
                    }
                }
            }                
        });        
    };
    
    $scope.consultar();
});
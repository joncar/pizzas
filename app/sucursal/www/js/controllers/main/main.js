var myPopup = [];
var controllers = angular.module('starter.controllers',['ionic']);

controllers.controller('Main', function($scope,Api,UI,User,$http) {    
    $scope.task = [];
    Api.selFavMainSendFav = function(id,datos){
        datos.direccion = datos.nombre;
        datos.tipo = 1;
        $scope.task = [datos];
    };
    
    $scope.pedirRepartidor = function(pedidos){
        for(var i = 0;i<pedidos; i++){
            $scope.task.push({lat:0,lon:0,direccion:'',ubicacion:'(0,0)',nombre:'',tipo:0});
        }
        Api.datos = {sucursales_id:$scope.user,task:JSON.stringify($scope.task),fecha_solicitud:$scope.fecha_solicitud};
        $scope.task = [];
        document.location.href="#/tab/aceptarprecio";
        console.log($scope.task);
    };
});
controllers.controller('aceptarprecio', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User){
    $scope = User.getData($scope);
    $scope.confirm = UI.getConfirmBox($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Por favor espere...');
    $scope.min = Api.ajustes.pago_minimo_sucursal; //Pago minimo aprobado por la sucursal    
    if(typeof(Api.datos)==='undefined' || Api.ajustes===undefined || Api.datos===null || typeof(Api.datos)==='undefined'){
        $state.go('tab.main');
    }    
    else{
         $scope.calcularPrecio = function(gratis){            
            var task = JSON.parse(Api.datos.task);
            task = task.length-1;            
            if(task<0){
                task = 0;
            }
            $scope.task = task;
            $scope.precio *=task;
            $scope.pagosucursal = ($scope.porcentaje_sucursal*$scope.precio)/100;
            $scope.pagocliente = ($scope.porcentaje_cliente*$scope.precio)/100;
            
        };

        $scope.gratis = 0;
        Api.list('sucursales',{id:$scope.user},$scope,$http,function(data){ 
            data = data[0]; 
            $scope.gratis = data.envios_sin_costo;
            $scope.precio = parseFloat(data.precio_por_paquete);
            $scope.calcularPrecio(data.envios_sin_costo);
        });
        //Tokenizar la tarjeta
        Conekta.setPublishableKey(Api.ajustes.public_key);
        var fecha = $scope.fecha_vencimiento.split('-');
        $form = {
            card:{
                'name':$scope.nombre_tarjeta,
                'number':$scope.tarjeta,
                'cvc':$scope.cvc,
                'exp_month':fecha[0],
                'exp_year':fecha[1]
            }
        };
        $scope.aceptar = function(pagocliente,pagosucursal){            
            if(Api.datos===null || Api.datos===undefined){
                $state.go('tab.main');
            }else{
                if(isNaN(pagosucursal) || pagosucursal<$scope.min){
                    alert(' El pago minimo aprobado para la sucursal es $'+$scope.min);
                }else{
                    $scope.toking = true;
                    $scope.pagosucursal = pagosucursal;
                    $scope.pagocliente = pagocliente;
                    if($scope.precio>$scope.gratis){
                        if($scope.nombre_tarjeta==='undefined' || $scope.tarjeta==='undefined' || $scope.cvc==='undefined'){
                                $scope.confirm('No posee tarjetas asociadas a su cuenta','¿Desea asociar una tarjeta ahora?',function(data){
                                    $state.go('tab.cuenta');
                                },function(){
                                    $state.go('tab.main');
                                });
                        }else{                                
                                Conekta.token.create($form,
                                function(data){
                                    Api.datos.card = [data.id];            
                                    $scope.insertarPedido();
                                }, 
                                function(data){
                                    $scope.showAlert = UI.getShowAlert($ionicPopup);
                                    $scope.showAlert('Disculpe pero su transacción no fué aprobada ['+data.message_to_purchaser+']');
                                    $scope.toking = false;
                                });
                        }
                    }else{
                        $scope.insertarPedido();
                    }
                }
            }
        };
        
        $scope.validarValores = function(pagocliente,pagosucursal){
            var tope = $scope.precio;
            var min = $scope.min;
            $scope.pagosucursal = pagosucursal<min?min:pagosucursal;
            $scope.pagocliente = tope-$scope.pagosucursal;
        };

        $scope.insertarPedido = function(){
            if(Api.datos===null || Api.datos===undefined){
                $state.go('tab.main');
            }
            else{
                $scope.data = Api.datos;
                $scope.data.precio = $scope.pagosucursal;
                $scope.data.precio_cliente = $scope.pagocliente;
                if($scope.data.fecha_solicitud===undefined){
                    var fecha = new Date();
                    var f = fecha.getFullYear()+'-'+(parseInt(fecha.getMonth())+1)+'-'+fecha.getDate()+' '+fecha.getHours()+':'+fecha.getMinutes()+':'+fecha.getSeconds();
                    $scope.data.fecha_solicitud = f;
                }
                $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando pedido...');
                Api.query('addPedido',$scope,$http,function(data){
                    Api.datos = null;
                    Api.lista = undefined;
                    if(Api.programar){
                        document.location.href="#/tab/main";
                    }else{
                        if(typeof(data.insert_primary_key)!=='undefined'){
                            document.location.href="#/tab/AsignarRepartidor/"+data.insert_primary_key;
                        }else{
                            alert('Su tarjeta fue rechazada por el banco, por favor verifiquela e intente nuevamente');
                            document.location.href="#/tab/cuenta/";
                        }
                    }
                });
            }
        };
        if(Api.triggerPay!==undefined){
            $scope.aceptar();
            Api.triggerPay = undefined;
        }
        $scope.toking = false;
    }
});
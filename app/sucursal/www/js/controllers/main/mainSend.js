controllers.controller('MainSend', function($scope,$state,$ionicModal,$ionicSideMenuDelegate,$ionicPopup,$ionicPlatform,Api,UI,User,SocketConnection,$http) {
    $scope = User.getData($scope);
    $scope.pedidos = 1;
    $scope.task = [
        {direccion:''},
        {direccion:''}
    ];
    $scope.tipo = '';
    $scope.divid = '';
    $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
    };

    $scope.inputForm = {
      street_number: 'ext',
      route: 'calle',
      locality: 'delegacion',
      administrative_area_level_1: 'ciudad',
      postal_code: 'cp',
      sublocality_level_1:'colonia'
    };

    $scope.selectFieldsFunction = function(place,indice,tipo){
        var datos = {};
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                datos[$scope.inputForm[addressType]] = val;
            }
        }
        datos.direccion = $scope.task[indice].direccion;
        datos.tipo = tipo;
        $scope.task[indice] = datos;
        if(indice===0){
            $scope.task[indice].tipo=$scope.tipo;
        }else{
            $scope.task[indice].tipo=$scope.tipo===1?0:1;
        }
    };
    
    $scope.initModals = function(){
            UI.getModalBox($ionicModal,'templates/modals/entrega.html',$scope,function(){
            $scope.entregaModal = $scope.modal;
            $scope.entrega = {show:function(){$scope.tipo = 1; $scope.entregaModal.show();},
                hide:function(){
                    $scope.task = [
                        {direccion:''},
                        {direccion:''}
                    ];                    
                    $scope.entregaModal.remove();
                    $scope.recolectaModal.remove();
                    $scope.initModals();
                }};           
         });
         
         UI.getModalBox($ionicModal,'templates/modals/recolecta.html',$scope,function(){
            $scope.recolectaModal = $scope.modal;
            $scope.recolecta = {show:function(){$scope.tipo = 0; $scope.recolectaModal.show();},
                hide:function(){
                    $scope.task = [
                        {direccion:''},
                        {direccion:''}
                    ];                    
                    $scope.entregaModal.remove();
                    $scope.recolectaModal.remove();
                    $scope.initModals();
                }};            
         });
    };
    $scope.initModals();
    $scope.addTask = function(){
      if($scope.task.length<5){
        $scope.task.push({direccion:''});
      }
    };
    $scope.remTask = function(i){
      $scope.task.splice(i,1);
    };
    
    UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope,function(){
        $scope.programarModal = $scope.modal;
    });
    
    $scope.programar = function(){
        $scope.programarModal.show();
    };
    
    $scope.selectFecha = function(datos,d){
        $scope.fecha_solicitud = d.fechas.getFullYear()+'-'+(parseInt(d.fechas.getMonth())+1)+'-'+d.fechas.getDate()+' '+d.hora.getHours()+':'+d.hora.getMinutes()+':'+d.hora.getSeconds();
        d.fechas.setHours(d.hora.getHours(), d.hora.getMinutes(), d.hora.getSeconds(), 0);
        var fecha = new Date();
        if(fecha.getTime()>=d.fechas.getTime()){
            alert('Debe asignar una fecha con un intervalo mayor a 2 horas como mínimo');
        }else{            
            var m = (d.fechas.getTime()-fecha.getTime())/1000;
            if(m<=7200){
                alert('Debe asignar una fecha con un intervalo mayor a 2 horas como mínimo');
            }else{
                $scope.programarModal.hide();                
                Api.time = m;
                Api.programar = true;
                $scope.sendDatos(datos,true);
            }
        }
    };
    
    $scope.sendDatos = function(datos,programar){        
        var task = [];
        for(i in $scope.task){
            if($scope.task[i].direccion!==''){
                task.push($scope.task[i]);
            }
        }
        $scope.task = task;
        if($scope.task.length>=2){
            //Ver si a alguno le falta las coordenadas
            var ok = true;
            for(var i in $scope.task){
                if(typeof($scope.task[i].lat)==='undefined' || typeof($scope.task[i].lon)==='undefined'){
                    ok = false;
                }
            }
            //VOltear arreglo en caso de recolecta
            if($scope.tipo===0){
                var task = [];
                for(i=($scope.task.length-1);i>=0;i--){
                    task.push($scope.task[i]);
                }
                $scope.task = task;
            }
            if(ok){
                //Ir al pago
                console.log($scope.task);
                $scope.recolectaModal.remove();
                $scope.entregaModal.remove();
                Api.datos = {sucursales_id:$scope.user,task:JSON.stringify($scope.task),fecha_solicitud:$scope.fecha_solicitud};
                $scope.task = [];
                console.log(Api.datos);
                document.location.href="#/tab/aceptarprecio";
            }else{
                //Ir a pedir datos faltantes
                $scope.recolectaModal.remove();
                $scope.entregaModal.remove();
                Api.datos = {sucursales_id:$scope.user,task:$scope.task,fecha_solicitud:$scope.fecha_solicitud};
                document.location.href="#/tab/task";
            }
        }else{
            alert('Por favor, llene los datos del pedido para poder ser procesado');
        }
    };
    
    $scope.gps = function(){
        $scope.options = {enableHighAccuracy: true};
        navigator.geolocation.getCurrentPosition(function(position) {            
            var c = position.coords;
            localStorage.lat = c.latitude;
            localStorage.lon = c.longitude;
            $scope.$apply();
            UI.getLocationName(c.latitude,c.longitude,function(dir){
               $scope.task[0].direccion = dir.formatted_address;
               $scope.selectFieldsFunction(dir,0,$scope.tipo);
            });                
         },function(e){
             alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');             
         },$scope.options);
    };
    
    Api.selFavMainSend = function(id,datos){
        var listo = false;
        for(var i in $scope.task){
            if(!listo && $scope.task[i].direccion===''){
                datos.direccion = datos.nombre;
                $scope.task[i] = datos;
                $scope.task[i].id = id;                
                if(i==='0'){
                    $scope.task[i].tipo=$scope.tipo;
                }else{
                    $scope.task[i].tipo=$scope.tipo===1?0:1;
                    $scope.addTask();
                }
                listo = true;
            }
        }        
    };
});